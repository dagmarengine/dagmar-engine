#include "dagmar/GameLayer.h"
#include "dagmar/EventDispatcher.h"
#include "dagmar/KeyManager.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/VoxelChunkManager.h"
#include <dagmar/PhysicsSystem.h>
#include <dagmar/PlayerControllerSystem.h>
#include <imgui.h>

dag::GameLayer::GameLayer()
{
}

void dag::GameLayer::onAttach()
{
    renderingSystem = coordinator->getSystem<RenderingSystem>();
}

void dag::GameLayer::onDetach()
{
}

void dag::GameLayer::onUpdate(float const& dt)
{
    const auto playerControllerSystem = coordinator->getSystem<PlayerControllerSystem>();
    Entity& activePlayer = playerControllerSystem->activePlayer;

    if (activePlayer != static_cast<Entity>(-1) && gameState == GameState::Play)
    {
        std::cout << "activeplayer!" << std::endl;
        auto& transform = coordinator->getComponent<Transform>(activePlayer);
        auto& playerController = coordinator->getComponent<PlayerController>(activePlayer);
        if (coordinator->hasComponent<RigidBody>(activePlayer))
        {

            // physics player
            auto& rigidBody = coordinator->getComponent<RigidBody>(activePlayer);

            /*if(rigidBody.velocity.y < 0.01f){
                coordinator->getSystem<PlayerControllerSystem>()->isInAir = false;
            }*/

            std::cout << "rigibody!" << std::endl;

            updateMovement(dt, playerController, rigidBody.velocity, transform);
            rigidBody.impulse -= airResistance * rigidBody.impulse;
        }
    }

    // if (voxelManager->currVoxelMainEntity != UINT32_MAX && voxelManager->currVoxelEntity != UINT32_MAX)
    if (voxelManager->currVoxelMainEntity != UINT32_MAX)
    {
        if (voxelManager->currVoxelEntity != UINT32_MAX)
        {
            voxelManager->startingPosition = coordinator->getComponent<VoxelComponent>(voxelManager->currVoxelMainEntity).startPosition;

            if (dag::voxelManager->update(coordinator->getComponent<Transform>(voxelManager->currVoxelEntity).position))
            {
                resourceManager->models[voxelManager->meshHash] = voxelManager->model;
            }
        }
        else
        {
            glm::vec3 pos = coordinator->getComponent<VoxelComponent>(voxelManager->currVoxelMainEntity).playerPosition;
            if (dag::voxelManager->update(pos))
            {
                resourceManager->models[voxelManager->meshHash] = voxelManager->model;
            }
        }
    }

    if (gameState == GameState::Play)
    {
        auto physicsSystem = coordinator->getSystem<PhysicsSystem>();
        for (size_t i = 0; i < scene->particles.size(); ++i)
        {
            scene->particles[i].lifespan--;
            if (scene->particles[i].lifespan < 2)
            {
                std::unique_lock<std::shared_mutex> lock(physicsSystem->physicsMutex);

                scene->destroyEntity(scene->particles[i].entity);

                scene->particles[i] = scene->particles[scene->particles.size() - 1];
                scene->particles.erase(scene->particles.end() - 1);
                --i;
            }
        }
        scene->particles.shrink_to_fit();

        /*
		auto& transform = coordinator->getComponent<Transform>(1);
        transform.rotation.x -= 0.05f;
        transform.updateModelMatrix();
        */
    }
    else if (gameState == GameState::Stop)
    {
        for (size_t i = 0; i < scene->particles.size(); ++i)
        {
            scene->destroyEntity(scene->particles[i].entity);
        }

        scene->particles.clear();
        scene->particles.shrink_to_fit();

        auto pos = keyManager->getMousePosition();
        lastPosX = pos.x;
        lastPosY = pos.y;
        hasBeenReset = false;
    }
    else if (gameState == GameState::Pause)
    {
        auto pos = keyManager->getMousePosition();
        lastPosX = pos.x;
        lastPosY = pos.y;
        hasBeenReset = false;
    }
}

void dag::GameLayer::updateMovement(float const& dt, PlayerController& playerController, glm::vec3& velocity, Transform& transform)
{
    glm::vec3 direction = glm::vec3(0.0, 0.0, 0.0);
    float speed = 0.0;

    Log::info("updateMovement");

    if (!coordinator->getSystem<PlayerControllerSystem>()->isInAir && playerController.fps_style)
    {
        // TODO Modify via transform directly, not with the the ones calculated in gamelayer
        if (keyManager->isKeyDown(GLFW_KEY_W))
        {
            direction -= forwardVector;
            speed = playerController.walkSpeed;
        }
        if (keyManager->isKeyDown(GLFW_KEY_S))
        {
            direction += forwardVector;
            speed = playerController.walkSpeed;
        }
        if (keyManager->isKeyDown(GLFW_KEY_A))
        {
            direction -= rightVector;
            speed = playerController.walkSpeed;
        }
        if (keyManager->isKeyDown(GLFW_KEY_D))
        {
            direction += rightVector;
            speed = playerController.walkSpeed;
        }

        if (keyManager->isKeyDown(GLFW_KEY_LEFT_SHIFT))
        {
            speed *= playerController.runMultiplier;
        }

        direction.y = 0.0;

        if (glm::length(direction) != 0.0)
        {
            direction = glm::normalize(direction);
        }
        else
        {
            direction = glm::vec3(1.0, 0.0, 0.0);
        }

        velocity.x = direction.x * speed * 10;
        velocity.z = direction.z * speed * 10;
    }

    if (keyManager->isKeyDown(GLFW_KEY_SPACE) && !coordinator->getSystem<PlayerControllerSystem>()->isInAir)
    {
        if (keyManager->isKeyDown(GLFW_KEY_LEFT_SHIFT))
        {
            velocity.y = playerController.jumpSpeed * playerController.runMultiplier;
        }
        else
        {
            velocity.y = playerController.jumpSpeed;
        }
        coordinator->getSystem<PlayerControllerSystem>()->isInAir = true;
    }
}

void dag::GameLayer::onEvent(Event& event)
{
    EventDispatcher dispatcher(event);

    dispatcher.dispatch<KeyTypedEvent>(BIND_EVENT_FN(onKeyTyped));
    dispatcher.dispatch<KeyPressedEvent>(BIND_EVENT_FN(onKeyPressed));
    dispatcher.dispatch<KeyReleasedEvent>(BIND_EVENT_FN(onKeyReleased));
    dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FN(onMouseScrolled));
    dispatcher.dispatch<MouseMovedEvent>(BIND_EVENT_FN(onMouseMoved));
    dispatcher.dispatch<MouseButtonPressedEvent>(BIND_EVENT_FN(onMouseButtonPressed));
    dispatcher.dispatch<MouseButtonReleasedEvent>(BIND_EVENT_FN(onMouseButtonReleased));
    dispatcher.dispatch<WindowResizeEvent>(BIND_EVENT_FN(onWindowResize));
}

void dag::GameLayer::reset()
{
    if (!hasBeenReset)
    {
        auto pos = keyManager->getMousePosition();
        lastPosX = pos.x;
        lastPosY = pos.y;
        hasBeenReset = true;
    }
}

bool dag::GameLayer::onKeyTyped(KeyTypedEvent& event)
{
    return false;
}

bool dag::GameLayer::onKeyPressed(KeyPressedEvent& event)
{
    return false;
}

bool dag::GameLayer::onKeyReleased(KeyReleasedEvent& event)
{
    return false;
}

bool dag::GameLayer::onMouseScrolled(MouseScrolledEvent& event)
{
    return false;
}

bool dag::GameLayer::onMouseMoved(MouseMovedEvent& event)
{
    reset();

    const auto cameraSystem = coordinator->getSystem<CameraSystem>();
    auto& cameraComponent = coordinator->getComponent<CameraComponent>(cameraSystem->activeCamera);
    auto& transform = coordinator->getComponent<Transform>(cameraSystem->activeCamera);

    if (cameraSystem->activeCamera == cameraSystem->editorCamera)
    {
        return false;
    }

    {
        auto mousePos = keyManager->getMousePosition();

        float deltaX = lastPosX - mousePos.x;
        float deltaY = lastPosY - mousePos.y;

        transform.rotation += glm::vec3(-deltaY * mouseDragSensitivity, -deltaX * mouseDragSensitivity, 0.0f);
        transform.updateModelMatrix();

        forwardVector.x = -cos(glm::radians(transform.rotation.x)) * sin(glm::radians(transform.rotation.y));
        forwardVector.y = sin(glm::radians(transform.rotation.x));
        forwardVector.z = cos(glm::radians(transform.rotation.x)) * cos(glm::radians(transform.rotation.y));
        forwardVector = glm::normalize(forwardVector);

        rightVector = glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), forwardVector));
        upVector = glm::normalize(glm::cross(rightVector, forwardVector));

        forwardVector = glm::normalize(glm::vec3(forwardVector.x, 0.0f, forwardVector.z));
        rightVector = glm::normalize(glm::vec3(rightVector.x, 0.0f, rightVector.z));

        lastPosX = mousePos.x;
        lastPosY = mousePos.y;
    }

    //ImGuiIO& io = ImGui::GetIO();
    //io.MousePos = ImVec2(event.getX(), event.getY());

    lastPosX = event.getX();
    lastPosY = event.getY();
    return true;
}

bool dag::GameLayer::onMouseButtonPressed(MouseButtonPressedEvent& event)
{
    // Camera update

    Transform transform;

    if (voxelManager->currVoxelMainEntity != UINT32_MAX)
    {
        // player update
        if (voxelManager->currVoxelEntity != UINT32_MAX)
        {
            const auto playerControllerSystem = coordinator->getSystem<PlayerControllerSystem>();
            transform = coordinator->getComponent<Transform>(playerControllerSystem->activePlayer);
        }
        // camera update
        else
        {
            const auto cameraSystem = coordinator->getSystem<CameraSystem>();
            transform = coordinator->getComponent<Transform>(cameraSystem->activeCamera);
        }

        //Player update

        // TODO check for camera attached to player
        // if (cameraSystem->activeCamera == cameraSystem->editorCamera)
        // {
        //     return false;
        // }

        if (keyManager->isMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT))
        {
            auto mousePos = keyManager->getMousePosition();

            for (int i = 1; i <= 6; ++i)
            {
                glm::vec checkPosition = transform.position - (float)i * transform.getForwardVector();
                auto& voxel = voxelManager->getVoxel(checkPosition);
                // std::cout << voxel.active << std::endl;
                if (voxel.active == true && voxel.blockType != BlockTypes::Default)
                {
                    voxel.active = false;

                    voxelManager->shouldUpdate = true;
                    scene->spawnParticles(glm::vec3(std::floor(checkPosition.x), std::floor(checkPosition.y), std::floor(checkPosition.z)), blockColors[voxel.blockType]);
                    voxel.blockType = BlockTypes::Unused;
                    // std::cout << checkPosition.x << ' ' << checkPosition.y << ' ' << checkPosition.z << std::endl;
                    break;
                }
            }
        }
    }
    return true;
}

bool dag::GameLayer::onMouseButtonReleased(MouseButtonReleasedEvent& event)
{
    return false;
}

bool dag::GameLayer::onWindowResize(WindowResizeEvent& event)
{
    return false;
}