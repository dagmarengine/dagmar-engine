#pragma once

#include <dagmar/Application.h>
#include <dagmar/GameLayer.h>
#include <dagmar/RendererLayer.h>

namespace dag
{
    class DagmarGame : public Application
    {
      public:
        DagmarGame();
        ~DagmarGame() override
        {
            std::cout << "~DagmarGame()" << std::endl;
        }

        std::shared_ptr<GameLayer> gameLayer;
        std::shared_ptr<RendererLayer> rendererLayer;

        void run() override;
        void onEvent(Event& e) override;
        bool onWindowClose(WindowCloseEvent& e) override;
    };
} // namespace dag