dagmar_add_game_library(game_resource_loader STATIC
	ResourceLoader.cpp
)

 target_link_libraries(game_resource_loader
	Dagmar::Engine
)

target_include_directories(game_resource_loader PUBLIC
	${CMAKE_CURRENT_SOURCE_DIR}/include
)

add_library(Dagmar::Game::ResourceLoader ALIAS game_resource_loader)