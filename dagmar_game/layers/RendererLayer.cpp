#include "dagmar/RendererLayer.h"
#include "dagmar/EventDispatcher.h"
#include "dagmar/Scene.h"
#include "dagmar/WindowManager.h"

dag::RendererLayer::RendererLayer() :
    Layer("RendererLayer")
{
}

dag::RendererLayer::~RendererLayer()
{
    std::cout << "~RendererLayer()" << std::endl;
}

void dag::RendererLayer::onAttach()
{
    renderingSystem = coordinator->getSystem<RenderingSystem>();
    renderingSystem->setRenderingMode(renderingSystem->currentRenderMode, true, false);
}

void dag::RendererLayer::onDetach()
{
}

void dag::RendererLayer::onUpdate(float const& dt)
{
    renderingSystem->update();
}

void dag::RendererLayer::onEvent(Event& event)
{
    EventDispatcher dispatcher(event);

    dispatcher.dispatch<WindowResizeEvent>(BIND_EVENT_FN(RendererLayer::onWindowResize));
}

bool dag::RendererLayer::onWindowResize(WindowResizeEvent& event)
{
    renderingSystem->setSizeDirty(event.getWidth(), event.getHeight());
    return false;
}