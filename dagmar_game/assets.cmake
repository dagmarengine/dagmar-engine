set(ASSETS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../assets)
set(ASSETS_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/assets)
set(ASSETS_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/assets)

set(ASSETS_FROM_MAIN "../assets/")

install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/application/output_scene_data.json DESTINATION ${ASSETS_INSTALL_DIR})

add_custom_target(assets_targ ALL
	COMMENT "Copying assets dir"
	COMMAND ${CMAKE_COMMAND} -E make_directory ${ASSETS_INSTALL_DIR}
	COMMAND ${CMAKE_COMMAND} -E make_directory ${ASSETS_INSTALL_DIR}/internal
	COMMAND ${CMAKE_COMMAND} -E make_directory ${ASSETS_INSTALL_DIR}/shaders
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${ASSETS_DIR}/internal ${ASSETS_INSTALL_DIR}/internal
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${ASSETS_DIR}/shaders ${ASSETS_INSTALL_DIR}/shaders
	COMMAND ${CMAKE_COMMAND} -E copy ${ASSETS_DIR}/EngineConfig.ini ${ASSETS_INSTALL_DIR}
)
