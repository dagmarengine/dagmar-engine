add_library(imfilebrowser INTERFACE)

target_include_directories(imfilebrowser INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/imgui-filebrowser
)

target_link_libraries(imfilebrowser INTERFACE
    imgui::imgui
)

file(GLOB_RECURSE public_headers
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui-filebrowser/*.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui-filebrowser/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui-filebrowser/*.inl"
)

install( FILES ${public_headers} DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${dir} )

install(TARGETS imfilebrowser)

add_library(imfilebrowser::imfilebrowser ALIAS imfilebrowser)