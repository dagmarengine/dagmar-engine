#!/bin/bash

mkdir build
pushd build
cmake .. -DBUILD_AND_RUN_TESTS=ON -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install
cmake --build .
popd
