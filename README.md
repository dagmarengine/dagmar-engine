# Dagmar Engine

### Find full website at: https://dagmarengine.gitlab.io/

### Watch our trailer at: https://www.youtube.com/watch?v=7IZ6tfjHeKU

## Clone

If this repository was not cloned before, you can simply do the following:

```
git clone --recursive https://gitlab.com/sc17mae/dagmar-engine.git
```

If the repository already exists on your machine, you will need to do the following:

```
git pull
git submodule init
git submodule update
```

## Build

To build this project, you can simply run the `build.sh` script.
Alternatively, you can do the following:

```
mkdir build
cd build
cmake ..
cmake --build .
```

You will see the `build/dagmar-engine` directory created in which you can find the executable.
This will make an in-project build.

## GIT PUSH

For documentation stuff, you don't need to create a CI pipeline, so you can push as follows:
```
git push -o ci.skip
```

## FAQ

Head over to [Initialising Project and CMake](https://gitlab.com/dagmar3/dagmar-engine/-/wikis/Initialising-Project-and-CMake) and [Development Workflow](https://gitlab.com/dagmar3/dagmar-engine/-/wikis/Development-workflow) for some FAQ's
