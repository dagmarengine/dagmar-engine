#pragma once
#include <memory>
#include <vector>

#include "dagmar/Layer.h"

namespace dag
{
    class LayerManager
    {
      public:
        std::vector<std::shared_ptr<Layer>> layers;

      public:
        LayerManager() = default;
        ~LayerManager();

        void addLayer(std::shared_ptr<Layer> const& layer);
        void removeLayer(std::shared_ptr<Layer>& layer);
        void removeLayer(size_t const& layerID);

    	void updateLayers();
    };
} // namespace dag