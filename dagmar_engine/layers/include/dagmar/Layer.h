#pragma once
#include <string>

#include "dagmar/Event.h"

namespace dag
{
    enum class GameState
    {
        Play,
        Pause,
        Stop
    };
	
    class Layer
    {
      public:
        std::string name;

      public:
        Layer(std::string const& name = "Layer");
        virtual ~Layer() = default;

        virtual void onAttach();
        virtual void onDetach();
        virtual void onUpdate(float const& dt = 0.1f);
        virtual void onEvent(Event& event);
    };
} // namespace dag