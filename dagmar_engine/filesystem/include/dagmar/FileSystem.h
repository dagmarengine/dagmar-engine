#pragma once
#include <string>
#include <filesystem>

// Utility to bind fs insted of the lengthy name
namespace fs = std::filesystem;

/**
 * @brief Engine custom namespace for paths to different
 * directories
 */
namespace dag
{
    namespace filesystem
    {
        // Gets the executable path, platform independent
        fs::path getExePath();

        // Gets the asset path
        fs::path getAssetsPath();

        // Gets the intermediate path
        fs::path getIntermediatePath();
    }
};
