#include "dagmar/RenderPass.h"
#include "dagmar/Coordinator.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/Scene.h"
#include "dagmar/UniformHandler.h"
#include <GL/glew.h>

void dag::RenderPass::begin()
{
    writeFramebuffer->bind();

    glViewport(0, 0, static_cast<int32_t>(writeFramebuffer->width), static_cast<int32_t>(writeFramebuffer->height));

    // Clear buffers
    for (size_t i = 0; i < writeFramebuffer->colorAttachments.size(); i++)
    {
        glClearNamedFramebufferfv(writeFramebuffer->id, GL_COLOR, i, clearValues[0].data());
    }

    if (writeFramebuffer->depthAttachment)
    {
        glClearNamedFramebufferfi(writeFramebuffer->id, GL_DEPTH_STENCIL, 0, clearValues[1][0], static_cast<int32_t>(clearValues[1][1]));
    }
}

void dag::RenderPass::end()
{
    writeFramebuffer->unbind();
}