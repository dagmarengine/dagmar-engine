#pragma once
#include <string>
#include <vector>

#include "dagmar/DODBlocks.h"
#include "dagmar/UniformBuffer.h"

namespace dag
{
    class UniformHandler
    {
      public:
        UniformBuffer<SceneData> ubSceneData;
        UniformBuffer<CameraData> ubCameraData;
        UniformBuffer<LightData> ubLightData;
        UniformBuffer<MaterialData> ubMaterialData;
        UniformBuffer<EntitiesData> ubEntitiesData;
        UniformBuffer<DrawData> ubDrawData;
        UniformBuffer<LightDrawData> ubLightDrawData;

    	// Post process buffers
    	UniformBuffer<PostProcessBloomSettings> ubPPBloomData;
        UniformBuffer<PostProcessFog> ubPPFogData;
        UniformBuffer<PostProcessBlur> ubPPBlurData;
        UniformBuffer<PostProcessChromaticAberration> ubPPChromaticAberrationData;
        UniformBuffer<PostProcessDilation> ubPPDilationData;
        UniformBuffer<PostProcessPixelize> ubPPPixelizeData;
        UniformBuffer<PostProcessSharpen> ubPPSharpenData;
    };

} // namespace dag