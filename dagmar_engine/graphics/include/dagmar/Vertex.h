#pragma once
#include <glm/glm.hpp>
#include <vector>

namespace dag
{
    /**
     * @brief Vertex struct
     */
    struct Vertex
    {
        glm::vec3 position = glm::vec3(0.0f);
        glm::vec3 normal = glm::vec3(0.0f);
        // glm::vec4 color = glm::vec4(0.0f);
        // glm::vec2 uv = glm::vec3(0.0f);

        std::vector<float> toFloatVector() 
        {
            return { position.x, position.y, position.z, normal.x, normal.y, normal.z };
        }
    };
} // namespace dag