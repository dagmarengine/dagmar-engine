#pragma once
#include <string>
#include "Texture.h"
#include <vector>

namespace dag
{
    /*
     * @brief TextureCube class
     */
    class TextureCube : public Texture
    {
      public:
        // Create func with texture info
        void create(TextureInfo const& textureInfo) override;

        // Create func with path
        // in the following order: px nx py ny pz nz
        virtual void create(std::vector<std::string> const& paths);

        // Binds the texture
        void bind(uint32_t const& slot) const override;
        void bind() const override;

        // Unbinds the texture
        void unbind(uint32_t const& slot) const override;
        void unbind() const override;
    };
} // namespace dag
