#include "dagmar/Framebuffer.h"

#include "dagmar/TimeManager.h"
#include <GL/glew.h>
#include <iostream>
#include <stdexcept>

/**
 * @brief Creates the framebuffer
 */
void dag::Framebuffer::create(uint32_t const& width, uint32_t const& height, bool isBackBuffer)
{
    if (width == 0 || height == 0 || width > 8192 || height > 8192)
    {
        throw std::runtime_error("Error: invalid resize of the framebuffer.");
    }

    this->width = width;
    this->height = height;

    if ((id != 0) || !isBackBuffer)
    {
        destroy();

        glCreateFramebuffers(1, &id);
    }

    if ((depthAttachment != nullptr) || (!colorAttachments.empty()))
    {
        reattachTextures();
    }
}

/**
 * @brief Destroys the framebuffer
 */
void dag::Framebuffer::destroy()
{
    if (id != 0)
    {
        glDeleteFramebuffers(1, &id);
    }
}

void dag::Framebuffer::attachTexture(Texture* const texture, TextureAttachment const& type, int32_t const& offset)
{
    auto translatedType = Texture::attachmentToGL(type);
    switch (translatedType)
    {
        case GL_COLOR_ATTACHMENT0:
        {
            if (offset < 0)
            {
                throw std::runtime_error("Offset invalid - Negative offset");
            }

            int32_t diff = static_cast<int32_t>(colorAttachments.size()) - offset;

            if (offset < colorAttachments.size())
            {
                colorAttachments[offset] = texture;
                colorAttachmentBoundLayers[offset] = 0;
            }
            else
            {
                if (diff > 1)
                {
                    throw std::runtime_error("Offset invalid - Not enough attachments in between.");
                }
                colorAttachments.emplace_back(texture);
                colorAttachmentBoundLayers.emplace_back(0);
            }
            break;
        }
        case GL_DEPTH_STENCIL_ATTACHMENT:
        {
            if (offset < 0)
            {
                throw std::runtime_error("Offset invalid - Only one depth attachment in a framebuffer is allowed.");
            }
            depthAttachment = texture;
            depthAttachmentBoundLayer = 0;
            depthAttachmentType = translatedType;

            break;
        }
        default:
            break;
    }

    glNamedFramebufferTexture(this->id, translatedType + offset, texture->id, 0);

    if (translatedType == GL_COLOR_ATTACHMENT0)
    {
        setDrawBuffers(colorAttachments.size());
    }
}

void dag::Framebuffer::attachTexture(Texture* const texture, TextureAttachment const& type, int32_t const& offset, int32_t const& layer)
{
    auto translatedType = Texture::attachmentToGL(type);

    switch (translatedType)
    {
        case GL_COLOR_ATTACHMENT0:
        {
            if (offset < 0)
            {
                throw std::runtime_error("Offset invalid - Negative offset");
            }

            int32_t diff = static_cast<int32_t>(colorAttachments.size()) - offset;

            if (offset < colorAttachments.size())
            {
                colorAttachments[offset] = texture;
                colorAttachmentBoundLayers[offset] = layer;
            }
            else
            {
                if (diff > 1)
                {
                    throw std::runtime_error("Offset invalid - Not enough attachments in between.");
                }
                colorAttachments.emplace_back(texture);
                colorAttachmentBoundLayers.emplace_back(layer);
            }
            break;
        }
        case GL_DEPTH_STENCIL_ATTACHMENT:
        {
            if (offset < 0)
            {
                throw std::runtime_error("Offset invalid - Only one depth attachment in a framebuffer is allowed.");
            }
            depthAttachment = texture;
            depthAttachmentBoundLayer = layer;
            depthAttachmentType = translatedType;

            break;
        }
        default:
            break;
    }

    glNamedFramebufferTextureLayer(this->id, translatedType + offset, texture->id, 0, layer);

    if (translatedType == GL_COLOR_ATTACHMENT0)
    {
        setDrawBuffers(colorAttachments.size());
    }
}

void dag::Framebuffer::setDrawBuffers(uint32_t const& count)
{
    std::vector<uint32_t> buf(count);

    for (int i = 0; i < count; i++)
    {
        buf[i] = GL_COLOR_ATTACHMENT0 + i;
    }

    glNamedFramebufferDrawBuffers(id, count, buf.data());
}

/**
 * @brief Gets color attachment at index
 */
dag::Texture* dag::Framebuffer::getColorAttachment(uint32_t const& id)
{
    if (id < colorAttachments.size())
    {
        return colorAttachments[id];
    }

    return nullptr;
}

/**
 * @brief Get depth attachment
 */
dag::Texture* dag::Framebuffer::getDepthAttachment()
{
    return depthAttachment;
}

/**
 * @brief Binds the framebuffer
 */
void dag::Framebuffer::bind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

/**
 * @brief Unbinds the framebuffer
 */
void dag::Framebuffer::unbind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void dag::Framebuffer::reattachTextures()
{
    for (size_t i = 0; i < colorAttachments.size(); i++)
    {
        if (colorAttachmentBoundLayers[i] != 0)
        {
            glNamedFramebufferTextureLayer(this->id, GL_COLOR_ATTACHMENT0 + i, colorAttachments[i]->id, 0, colorAttachmentBoundLayers[i]);
        }
        else
        {
            glNamedFramebufferTexture(this->id, GL_COLOR_ATTACHMENT0 + i, colorAttachments[i]->id, 0);
        }
    }

    setDrawBuffers(colorAttachments.size());

    if (depthAttachment)
    {
        if (depthAttachmentBoundLayer != 0)
        {
            glNamedFramebufferTextureLayer(this->id, depthAttachmentType, depthAttachment->id, 0, depthAttachmentBoundLayer);
        }
        else
        {
            glNamedFramebufferTexture(this->id, depthAttachmentType, depthAttachment->id, 0);
        }
    }
}

/**
 * @brief Gets pixel data
 */
std::array<unsigned char, 4> dag::Framebuffer::getPixelData(int32_t const& x, int32_t const& y) const
{
    bind();
    unsigned char data[4] = {1, 1, 1, 1};

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glReadPixels(x, height - y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    unbind();

    return std::array<unsigned char, 4>({data[0], data[1], data[2], data[3]});
}

void dag::Framebuffer::blit(Framebuffer& src, Framebuffer& dst, int32_t const& bitfield)
{
    glBlitNamedFramebuffer(src.id, dst.id, 0, 0, src.width, src.height, 0, 0, dst.width, dst.height, bitfield, GL_NEAREST);
}
