#include "dagmar/Texture2DArray.h"

#include <GL/glew.h>
#include <stdexcept>

#include <iostream>


/**
 * @brief Creates a 2D texture using the information gathered
 * param textureInfo 
 */
void dag::Texture2DArray::create(dag::TextureInfo const& textureInfo)
{
    if (id != 0)
    {
        destroy();
    }

    this->info = textureInfo;

    glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &id);

	float borderColor[] = {1.0, 1.0, 1.0, 1.0};
	glTextureParameterfv(id, GL_TEXTURE_BORDER_COLOR, borderColor);  

    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTextureStorage3D(id, 1, formatToGL(textureInfo.format), textureInfo.width, textureInfo.height, textureInfo.arrayLayers);
}

/**
 * @brief Binds the texture
 */
void dag::Texture2DArray::bind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, id);
    
}

/**
 * @brief Binds the texture
 */
void dag::Texture2DArray::bind() const
{
    glBindTexture(info.samples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D_ARRAY, id);
}

void dag::Texture2DArray::unbind() const
{
    glBindTexture(info.samples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D_ARRAY, 0);
}

void dag::Texture2DArray::unbind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, 0);
}

void dag::Texture2DArray::resize(uint32_t const& width, uint32_t const& height, uint32_t const& arrayLayers)
{
    if (width > 32768 || height > 32768)
    {
        throw std::runtime_error("Error: Maximum width/height is 32768 px.");
    }

    info.width = width;
    info.height = height;
    info.arrayLayers = arrayLayers;

    create(info);
}
