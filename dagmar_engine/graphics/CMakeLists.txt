dagmar_add_library(graphics Dagmar::Graphics STATIC
    Descriptor.cpp
    Mesh.cpp
    Framebuffer.cpp
    Texture.cpp
    Texture2D.cpp
    Texture2DArray.cpp
    TextureCube.cpp
    RenderPass.cpp
    UniformHandler.cpp
)

target_include_directories(graphics PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(graphics PUBLIC
    glew::glew
    Dagmar::Log
    Dagmar::Exceptions
    Dagmar::Renderer
    glm::glm
    Dagmar::ECS
    stb::stb
)
