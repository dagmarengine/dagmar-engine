#include "dagmar/KeyManager.h"
#include "dagmar/WindowManager.h"
#include <algorithm>
#include <vector>

/**
 * @brief Call to check whether a key is pressed.
 * Keys are of the format GLFW_KEY_X
 */
bool dag::KeyManager::isKeyDown(int key)
{
    const auto state = glfwGetKey(windowManager->window.handle, static_cast<int32_t>(key));

    return state == GLFW_PRESS || state == GLFW_REPEAT;

    // return dag::KeyManager::keys[key];
}

bool dag::KeyManager::isMouseButtonDown(int mouseButton)
{
    const auto state = glfwGetMouseButton(windowManager->window.handle, static_cast<int32_t>(mouseButton));

    return state == GLFW_PRESS;
}

glm::vec2 dag::KeyManager::getMousePosition()
{
    double x;
    double y;
    glfwGetCursorPos(windowManager->window.handle, &x, &y);

    return {(float)x, (float)y};
}

/**
 * @brief Private function to set key state
 */
void dag::KeyManager::setKeyDown(int key, bool isDown)
{
    keys[key] = isDown;
}

/**
 * @brief Register our callback with GLFW
 */
void dag::KeyManager::startup()
{
    //glfwSetKeyCallback(dag::windowManager->window.handle, keyCallback);
    //glfwSetCharCallback(dag::windowManager->window.handle, charCallback);
    //glfwSetScrollCallback(dag::windowManager->window.handle, scrollCallback);
}

/**
 * @brief Does nothing
 */
void dag::KeyManager::shutdown()
{
}

/**
 * @brief Our callback for GLFW to call whenever a key is pressed.
 * It updates an internal mapping of key -> bool
 */
void dag::KeyManager::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);

    // Ours here ---
    // dag::keyManager->setKeyDown(key, action != GLFW_RELEASE);
}

/**
 * @brief Our callback for GLFW to call whenever a char is pressed
 */
void dag::KeyManager::charCallback(GLFWwindow* window, unsigned c)
{
    // ImGui_ImplGlfw_CharCallback(window, c);

    // Ours here ---
}

/**
 * @brief Our callback for GLFW to call whenever scroll is used
 */
void dag::KeyManager::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    // ImGui_ImplGlfw_ScrollCallback(window, xoffset, yoffset);

    // Ours here ---
}