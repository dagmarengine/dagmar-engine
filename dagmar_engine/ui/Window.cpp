#include "dagmar/Window.h"

#include "dagmar/KeyPressedEvent.h"
#include "dagmar/KeyReleasedEvent.h"
#include "dagmar/KeyTypedEvent.h"
#include "dagmar/Log.h"
#include "dagmar/MouseButtonPressedEvent.h"
#include "dagmar/MouseButtonReleasedEvent.h"
#include "dagmar/MouseMovedEvent.h"
#include "dagmar/MouseScrolledEvent.h"
#include "dagmar/WindowCloseEvent.h"
#include "dagmar/WindowResizeEvent.h"

#include "dagmar/FileSystem.h"
#include "stb_image.h"

dag::Window::~Window()
{
}

void dag::Window::create(WindowInfo const& windowInfo)
{
    this->info = windowInfo;

    glfwWindowHint(GLFW_DECORATED, windowInfo.decorated);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, info.vsync);
    glfwWindowHint(GLFW_VISIBLE, info.visible);

    glfwSwapInterval(1);

    // Open a window and create its OpenGL context
    handle = glfwCreateWindow(info.width, info.height, info.title.c_str(), nullptr, nullptr);

    if (handle == nullptr)
    {
        glfwTerminate();
        Log::error("Failed to open GLFW window.");
        throw std::runtime_error("Failed to open GLFW window.");
    }

    glfwMakeContextCurrent(handle);

    if (windowInfo.install)
    {
        // Ensure we can capture the escape key being pressed below
        glfwSetInputMode(handle, GLFW_STICKY_KEYS, GL_TRUE);
        glfwSetInputMode(handle, GLFW_LOCK_KEY_MODS, GLFW_TRUE);

        glfwSetWindowUserPointer(handle, &info);

        glfwSetWindowSizeCallback(handle, [](GLFWwindow* window, int width, int height) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            eventWindowInfo.width = width;
            eventWindowInfo.height = height;

            WindowResizeEvent event(width, height);
            eventWindowInfo.eventCallbackFn(event);
        });

        glfwSetWindowCloseCallback(handle, [](GLFWwindow* window) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));
            WindowCloseEvent event;
            eventWindowInfo.eventCallbackFn(event);
        });

        glfwSetCharCallback(handle, [](GLFWwindow* window, unsigned int character) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            KeyTypedEvent event(character, 0, 0, 0);
            eventWindowInfo.eventCallbackFn(event);
        });

        glfwSetKeyCallback(handle, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            switch (action)
            {
                case GLFW_PRESS:
                {
                    KeyPressedEvent event(key, scancode, action, mods, 0);
                    eventWindowInfo.eventCallbackFn(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    KeyReleasedEvent event(key, scancode, action, mods);
                    eventWindowInfo.eventCallbackFn(event);
                    break;
                }
                case GLFW_REPEAT:
                {
                    KeyPressedEvent event(key, scancode, action, mods, 1);
                    eventWindowInfo.eventCallbackFn(event);
                    break;
                }
                default:
                {
                    break;
                }
            }
        });

        glfwSetScrollCallback(handle, [](GLFWwindow* window, double xOffset, double yOffset) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            MouseScrolledEvent event(static_cast<float>(xOffset), static_cast<float>(yOffset));
            eventWindowInfo.eventCallbackFn(event);
        });

        glfwSetCursorPosCallback(handle, [](GLFWwindow* window, double xPos, double yPos) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            MouseMovedEvent event(static_cast<float>(xPos), static_cast<float>(yPos));
            eventWindowInfo.eventCallbackFn(event);
        });

        glfwSetMouseButtonCallback(handle, [](GLFWwindow* window, int button, int action, int mods) {
            WindowInfo& eventWindowInfo = *static_cast<WindowInfo*>(glfwGetWindowUserPointer(window));

            switch (action)
            {
                case GLFW_PRESS:
                {
                    MouseButtonPressedEvent event(button, mods);
                    eventWindowInfo.eventCallbackFn(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    MouseButtonReleasedEvent event(button, mods);
                    eventWindowInfo.eventCallbackFn(event);
                    break;
                }
                default:
                {
                    break;
                }
            }
        });
    }

    GLFWimage* icon_image = new GLFWimage{};
    int width, height, channal;
    stbi_uc* img = stbi_load((dag::filesystem::getAssetsPath() / "internal/icon.png").generic_string().c_str(), &width, &height, &channal, 0); //rgba channels
    if (stbi_failure_reason())
        std::cout << stbi_failure_reason() << std::endl;
    icon_image->height = height;
    icon_image->width = width;
    icon_image->pixels = img;
    glfwSetWindowIcon(handle, 1, icon_image);
    stbi_image_free(icon_image->pixels);

    if (!glewInitialized)
    {
        // Initialize GLEW
        if (glewInit() != GLEW_OK)
        {
            //glfwTerminate();
            Log::error("Failed to initialize GLEW");
            throw std::runtime_error("Failed to initialize GLEW");
        }
        glewInitialized = true;
    }
}

void dag::Window::onUpdate()
{
    glfwPollEvents();
    glfwSwapBuffers(handle);
}

uint32_t dag::Window::getWidth() const
{
    return info.width;
}

uint32_t dag::Window::getHeight() const
{
    return info.height;
}

void dag::Window::focus() const
{
    glfwMakeContextCurrent(handle);
}

void dag::Window::setEventCallbackFn(EventCallbackFn const& callback)
{
    info.eventCallbackFn = callback;
}

void dag::Window::setVSync(bool const& enabled)
{
    info.vsync = enabled;
}

bool dag::Window::getVSync() const
{
    return info.vsync;
}

void dag::Window::maximize()
{
    glfwMakeContextCurrent(handle);
    glfwMaximizeWindow(handle);
}

void dag::Window::minimize()
{
    glfwMakeContextCurrent(handle);
    glfwIconifyWindow(handle);
}

void dag::Window::hide()
{
    glfwMakeContextCurrent(handle);
    glfwHideWindow(handle);
    info.visible = false;
}

void dag::Window::show()
{
    glfwShowWindow(handle);
    info.visible = true;
}

void dag::Window::center()
{
    auto* monitor = glfwGetPrimaryMonitor();
    if (!monitor)
    {
        return;
    }

    auto* mode = glfwGetVideoMode(monitor);
    if (!mode)
    {
        return;
    }

    int monitorX, monitorY;
    glfwGetMonitorPos(monitor, &monitorX, &monitorY);

    int windowWidth, windowHeight;
    glfwGetWindowSize(handle, &windowWidth, &windowHeight);
    glfwSetWindowPos(handle, monitorX + (mode->width - windowWidth) / 2, monitorY + (mode->height - windowHeight) / 2);
}

void dag::Window::destroy()
{
    if (handle)
    {
        glfwMakeContextCurrent(handle);
        glfwSetWindowShouldClose(handle, 1);
        glfwDestroyWindow(handle);
    }
}
