#include "dagmar/WindowManager.h"
#include "dagmar/FileSystem.h"
#include "dagmar/IconsFontAwesome5.h"
#include "dagmar/Log.h"
#include "dagmar/Module.h"

#include <filesystem>
#include <memory>

// Include GLEW

// Include GLFW
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <random>

#include "dagmar/WindowResizeEvent.h"

#include <thread>

#include "dagmar/Framebuffer.h"
#include "stb_image.h"

void dag::WindowManager::startup()
{
    if (!initialized)
    {
        initialized = glfwInit();
        // Initialise GLFW
        if (!initialized)
        {
            Log::error("Failed to initialize GLFW");
            throw std::runtime_error("Failed to initialize GLFW");
        }
    }

    createSplashWindow();
    createMainWindow();
}

void dag::WindowManager::shutdown()
{
    window.destroy();
}

void dag::WindowManager::createMainWindow()
{
    window.create({.title = "Dagmar Engine", .width = 1280, .height = 720, .vsync = true, .decorated = true, .install = true, .visible = false});
}

void dag::WindowManager::createSplashWindow(bool const& randomized)
{
    int width = 0;
    int height = 0;
    int channels = 0;
    int randomizedNum = 0;

    std::filesystem::path splashPath = dag::filesystem::getAssetsPath() / "internal" / "splash" / "splash_1024_512.png";

    if (randomized)
    {
        std::vector<std::filesystem::path> paths = {
            dag::filesystem::getAssetsPath() / "internal" / "splash" / "splash_1024_512.png",
            dag::filesystem::getAssetsPath() / "internal" / "splash" / "splash1_512_512.png",
            dag::filesystem::getAssetsPath() / "internal" / "splash" / "splash2_915_512.png"};

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> distr(0, paths.size() - 1);

        randomizedNum = distr(gen);
        splashPath = paths[randomizedNum];
    }
    
    stbi_load(splashPath.string().c_str(), &width, &height, &channels, 0);

    loadingWindow.create({.title = "Dagmar Engine - Loading", .width = static_cast<uint32_t>(width), .height = static_cast<uint32_t>(height), .vsync = true, .decorated = false, .install = false});
    loadingWindow.center();

    stbi_set_flip_vertically_on_load(1);
    Texture2D tex;
    tex.create(splashPath);
    Framebuffer fb;
    fb.create(width, height, false);
    fb.attachTexture(&tex, TextureAttachment::COLOR_ATTACHMENT);
    Framebuffer bb;
    bb.create(width, height, true);
    Framebuffer::blit(fb, bb, GL_COLOR_BUFFER_BIT);
    fb.destroy();
    tex.destroy();
    loadingWindow.onUpdate();
    stbi_set_flip_vertically_on_load(0);
}

void dag::WindowManager::swapContext()
{
    loadingWindow.destroy();
    window.show();
    window.focus();
}

std::string dag::WindowManager::getName() const
{
    return "WindowManager";
}

void dag::WindowManager::disableCursor() const
{
    if (glfwGetInputMode(window.handle, GLFW_CURSOR) != GLFW_CURSOR_DISABLED)
    {
        glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}

void dag::WindowManager::enableCursor() const
{
    if (glfwGetInputMode(window.handle, GLFW_CURSOR) != GLFW_CURSOR_NORMAL)
    {
        glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

void dag::WindowManager::hideCursor() const
{
    if (glfwGetInputMode(window.handle, GLFW_CURSOR) != GLFW_CURSOR_HIDDEN)
    {
        glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    }
}
