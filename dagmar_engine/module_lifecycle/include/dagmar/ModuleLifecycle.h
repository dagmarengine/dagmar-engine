#pragma once
#include "dagmar/Module.h"
#include "dagmar/KeyManager.h"
#include "dagmar/PlatformDetector.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/Log.h"
#include "dagmar/Coordinator.h"
#include "dagmar/WindowManager.h"
#include "dagmar/TimeManager.h"
#include "dagmar/EngineConfig.h"
#include "dagmar/Profiler.h"
#include <vector>
#include <memory>
#include <atomic>

#include "dagmar/SystemsManager.h"

namespace dag
{
	/**
	 * @brief Class to manage the dependancies between modules
	 */
	class ModuleLifecycle
    {

	public:
		// ALL Modules should be listed here IN ORDER of initialisation
		std::vector<std::shared_ptr<Module>> modules =
		{
			engineConfig,
			profiler,
			timeManager,
			platformDetector,
			resourceManager,
            coordinator,
			windowManager,
            keyManager,
			systemsManager
		};

        // This class is a singleton, this is how it should be retrieved.
        static ModuleLifecycle& get();

        // function to start up all modules in the desired order
        void startup();

        // function to shut down all modules in the desired order (roughly the reverse of the start up)
        void shutdown();

        bool isShuttingDown();

      private:
        // Default [con/de]structors
        ModuleLifecycle() = default;
        ~ModuleLifecycle() = default;
    };
} // namespace dag
