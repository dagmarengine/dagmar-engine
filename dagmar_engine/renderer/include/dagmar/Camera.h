#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace dag
{
    /**
     * @brief Camera class
     */
    class Camera
    {
      public:
        bool isPossessed;
        glm::vec3 position;
        glm::vec3 upVector;
        glm::vec3 forwardVector;
        glm::vec3 rightVector;
        // in radians
        float fov;
        float aspect;
        float zNear;
        float zFar;

      public:
        // default constructor with default values
        Camera();

        void setPosition(glm::vec3 const& pos);
        // set the up/forward/right vector, does not need to be normalised
        void setUpVector(glm::vec3 const& up);
        void setForwardVector(glm::vec3 const& forward);
        void setRightVector(glm::vec3 const& right);

        // set perspective attributes, expects fov in angles
        void setFOV(float const& fovInAngles);
        void setAspect(float const& aspect);
        void setNear(float const& zNear);
        void setFar(float const& zFar);

        // set the view/projection parameters
        void setView(glm::vec3 const& pos, glm::vec3 const& up, glm::vec3 const& forward);
        void setProjection(float const& fovInAngles, float const& aspect, float const& zNear, float const& zFar);

        // data getters for shaders
        glm::mat4 getView() const;
        glm::mat4 getProjection() const;

        glm::vec3 getPosition() const;

        // assumed CCW for positive angles
        void pitch(float const& angle);
        void roll(float const& angle);
        void yaw(float const& angle);

        // moving in one of the axes
        void moveForward(float const& value);
        void moveUp(float const& value);
        void moveRight(float const& value);
    };
} // namespace dag
