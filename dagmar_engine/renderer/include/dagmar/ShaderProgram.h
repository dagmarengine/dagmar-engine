#pragma once

#include <GL/glew.h>
#include <filesystem>
#include <string>
#include <vector>
#include <memory>


#include "dagmar/ShaderModule.h"

namespace dag
{
    /**
     * @class Shader program 
     */
    class ShaderProgram
    {
      public:
        unsigned int id;

      public:
        ShaderProgram();
        ShaderProgram(std::vector<std::shared_ptr<dag::ShaderModule>>& shaders);

        // Destroy the shader program
        void destroy();

        // Tell GL to bind this program
        void bind();

        // Tell GL to unbind this shader program
        void unbind();
    };
} // namespace dag
