#pragma once

#include <GL/glew.h>
#include <vector>

namespace dag
{
    /**
     * @brief Buffer Templated Class
     */
    template <class T>
    class Buffer
    {
      public:
        uint32_t id = 0;
        std::vector<T> elements;
        size_t size = 0;

        void* mapping = nullptr;

      public:
        Buffer() = default;

        // Default constructor
        Buffer(void* data, uint32_t const& size)
        {
            for (uint32_t i = 0; i < size; i++)
            {
                elements.push_back(static_cast<T*>(data)[i]);
                this->size += 1;
            }
        }

        // Constructor with vector
        Buffer(std::vector<T> const& data)
        {
            elements = data;
            size = data.size();
        }

        // Copy Constructor
        Buffer(Buffer const& other)
        {
            id = other.id;
            elements = other.elements;
            size = other.size;
        }

        virtual void destroy()
        {
            if (id != 0)
            {
                glDeleteBuffers(1, &id);
            }
        }

        // Virtual buffer generate function
        virtual void generate()
        {
            if (id == 0)
            {
                glGenBuffers(1, &id);
            }
        }

        // Pure virtual update function
        virtual void update(void* data, uint32_t const& offset, uint32_t const& size, bool const& flushToGPU) = 0;

        // Pure virtual map function
        virtual void* map(int32_t const& access) = 0;

        // Pure virtual map range function
        virtual void* mapRange(uint32_t const& offset, uint32_t const& length, int32_t const& access) = 0;

        // Pure Virtual function to unmap the buffer
        virtual void unmap() = 0;

        // Pure virtual function to load the buffer
        virtual void load(int32_t const& usage) = 0;

        // Pure virtual fucntion to bind the buffer
        virtual void bind() = 0;

        // Pure virtual function to unbind the buffer
        virtual void unbind() = 0;
    };
} // namespace dag
