#pragma once

#include "Buffer.h"

#include <GL/glew.h>
#include <cstdint>

namespace dag
{
    /**
     * @brief Vertex Buffer Class
     */
    template <class T>
    class UniformBuffer : public Buffer<T>
    {
    public:
        void* data = nullptr;
      int32_t size;
    	
      public:
        UniformBuffer() = default;

        // Default old-style constructor
        UniformBuffer(void* data, uint32_t const& size) :
            Buffer<T>(data, size)
        {
        }

        // Constructor with a vector
        UniformBuffer(std::vector<T> const& data) :
            Buffer<T>(data)
        {
        }

        // Copy constructor
        UniformBuffer(UniformBuffer const& other) :
            Buffer<T>(other)
        {
        }

    	~UniformBuffer()
        {
            if (data)
            {
                free(data);
            }
        }

        // Pure virtual update function
        void update(void* data, uint32_t const& offset, uint32_t const& size, bool const& flushToGPU) override
        {
            if (flushToGPU)
            {
                bind();
                glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data);
                unbind();
            }

        	

            this->elements[offset / sizeof(T)] = *(reinterpret_cast<T*>(data));
        }

    	void update()
        {
            bind();
            if (data)
            {
                glBufferData(GL_UNIFORM_BUFFER, size, data, GL_DYNAMIC_DRAW);
            }
            else
            {
				glBufferData(GL_UNIFORM_BUFFER, sizeof(T) * this->elements.size(), this->elements.data(), GL_DYNAMIC_DRAW);
            }
        	unbind();
        }

        // Virtual function to map the buffer
        void* map(int32_t const& access) override
        {
            this->mapping = glMapBuffer(GL_UNIFORM_BUFFER, access);
            return this->mapping;
        }

        // Virtual function to map the buffer
        void* mapRange(uint32_t const& offset, uint32_t const& length, int32_t const& access) override
        {
            this->mapping = glMapBufferRange(GL_UNIFORM_BUFFER, offset, length, access);
            return this->mapping;
        }

        // Virtual function to unmap the buffer
        void unmap() override
        {
            glUnmapBuffer(GL_UNIFORM_BUFFER);
        }

        // Virtual function to load the buffer
        void load(int32_t const& usage) override
        {
            if (data)
            {
				glBufferData(GL_UNIFORM_BUFFER, size, data, usage);
            }
            else
            {
				glBufferData(GL_UNIFORM_BUFFER, this->elements.size() * sizeof(T), this->elements.data(), usage);
            }
        }

        // Virtual function to bind the buffer
        void bind() override
        {
            glBindBuffer(GL_UNIFORM_BUFFER, this->id);
        }

        // Virtual function to unbind the buffer
        void unbind() override
        {
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }

    	virtual void bindBufferBase(uint32_t const& bindingPoint)
        {
            glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, this->id);
        }

    	virtual void bindBufferRange(uint32_t const& bindingPoint, uint32_t const& offset, uint32_t const& size)
        {
            glBindBufferRange(GL_UNIFORM_BUFFER, bindingPoint, this->id, offset, size);
        }
    };
} // namespace dag
