#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/IconsFontAwesome5.h"
#include "dagmar/RenderingSystem.h"
#include <cstring>
#include <nlohmann/json.hpp>
#include <random>
#include <vector>

using json = nlohmann::json;

namespace dag
{
    class Scene : public Module
    {
        struct Particle
        {
            Entity entity;
            size_t lifespan;
        };

      public:
        std::shared_ptr<dag::Coordinator> coordinator;
        std::vector<dag::Entity> entities;
        std::vector<Particle> particles;
        // to be used in IMGUI as sceneGraph.data() for pure pointer
        std::vector<std::string> sceneGraph;
        int currentEntity;
        // Current entity relative to the RenderingSystem entities set start
        int currentRenderableEntity;
        int currentSceneCamera;
        std::vector<std::string> defaultEntities;
        std::vector<int> entitiesCounter;
        std::string focussedEntity;

      public:
        Scene();
        void checkName(std::string& name);
        void spawnParticles(glm::vec3 position, glm::vec3 const& color);
        void createEntity(std::string name);
        void createDefaultEntity(std::string name);
        void destroyEntity(dag::Entity const& entity);

        template <typename T>
        void addComponent(dag::Entity const& entity, T const& component)
        {
            coordinator->addComponent<T>(entity, component);
        }
        template <typename T>
        void removeComponent(dag::Entity const& entity)
        {
            coordinator->removeComponent<T>(entity);
        }

        void recomputeSceneGraph();
        std::string getName() const override;

        // Starts up the module
        void startup() override;
        void initialize();

        // Shuts down the module
        void shutdown() override;

        // serialize entire scene
        void serialize(std::filesystem::path const& output_path);
        void serialize(json& big_j);

        // deserialize scene from json file
        void deserialize(std::filesystem::path const& input_path);
        // deserialize scene from json object and file location
        void deserialize(json const& input_json, std::filesystem::path const& input_path, bool load_mesh);

        int getEntityIndex(Entity const& entity);
    };

    inline std::shared_ptr<Scene> scene(new Scene());
} // namespace dag