#pragma once

#include <memory>
#include <typeindex>
#include <unordered_map>

#include "Component.h"
#include "ComponentArray.h"
#include "dagmar/Exceptions.h"

namespace dag
{
    /**
     * @brief Component Manager class
     * Manages the component types and the arrays
     */
    class ComponentManager
    {
      private:
        std::unordered_map<std::type_index, ComponentType> componentTypes;
        std::unordered_map<std::type_index, std::shared_ptr<InterfaceComponentArray>> componentArrays;
        ComponentType nextComponentType;

      public:
        ComponentManager();

        /**
         * @brief Tries to register a component
         */
        template <typename T>
        void registerComponent()
        {
            std::type_index type = std::type_index(typeid(T));

            if (componentTypes.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::RegisterComponentAlreadyExistent);
            }

            componentTypes.insert({type, nextComponentType});
            componentArrays.insert({type, std::make_shared<ComponentArray<T>>()});

            nextComponentType++;
        }

        /**
         * @brief Tries to get a component type
         */
        template <typename T>
        ComponentType getComponentType()
        {
            std::type_index type = std::type_index(typeid(T));

            if (!componentTypes.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::GetComponentTypeNotExistent);
            }

            return componentTypes[type];
        }

        /**
         * @brief Tries to add a component by insterting into the corresponding array
         */
        template <typename T>
        void addComponent(Entity const& entity, T const& component)
        {
            getComponentArray<T>()->insertData(entity, component);
        }

        /**
         * @brief Tries to remove a component from an entity
         */
        template <typename T>
        void removeComponent(Entity const& entity)
        {
            getComponentArray<T>()->removeData(entity);
        }

        /**
         * @brief Tries to get a component of an entity
         */
        template <typename T>
        T& getComponent(Entity const& entity)
        {
            return getComponentArray<T>()->getData(entity);
        }

        // Marks the component as being destroyed in the arrays
        void entityDestroyed(Entity const& entity);

        template <typename T>
        bool hasComponent(Entity const& entity){
            std::type_index type = std::type_index(typeid(T));

            if (!componentTypes.contains(type))
            {
                return false;
            }


            return getComponentArray<T>()->hasData(entity);
        }

    	/**
         * @brief Gets a ptr to a component array
         */
        template <typename T>
        std::shared_ptr<ComponentArray<T>> getComponentArray()
        {
            std::type_index type = std::type_index(typeid(T));

            if (!componentTypes.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::GetComponentArrayNotExistent);
            }

            return std::static_pointer_cast<ComponentArray<T>>(componentArrays[type]);
        }
    };
} // namespace dag
