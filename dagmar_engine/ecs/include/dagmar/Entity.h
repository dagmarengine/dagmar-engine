#pragma once
#include <cstdint>

namespace dag
{
    using Entity = std::uint32_t;
    const Entity MAX_ENTITIES = 512;
} // namespace dag
