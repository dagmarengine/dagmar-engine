#pragma once

#include <atomic>
#include <memory>

#include "dagmar/ComponentManager.h"
#include "dagmar/EntityManager.h"
#include "dagmar/SystemManager.h"

#include "dagmar/Module.h"

namespace dag
{
    class Coordinator : public Module
    {
      private:
        std::unique_ptr<EntityManager> entityManager;
        std::unique_ptr<ComponentManager> componentManager;
        std::unique_ptr<SystemManager> systemManager;

      public:
        // Creating an entity
        Entity createEntity();

        // Destroying an entity
        void destroyEntity(Entity const& entity);

        /**
         * @brief Checks if system exists
         */
        template <typename T>
        bool containsSystem()
        {
            return systemManager->containsSystem<T>();
        }

        /**
         * @brief Register a component
         */
        template <typename T>
        void registerComponent()
        {
            componentManager->registerComponent<T>();
        }

        /**
         * @brief Adds a component to an entity
         */
        template <typename T>
        void addComponent(Entity const& entity, T const& component)
        {
            componentManager->addComponent<T>(entity, component);

            auto signature = entityManager->getSignature(entity);
            signature.set(componentManager->getComponentType<T>(), true);
            entityManager->setSignature(entity, signature);

            systemManager->entitySignatureChanged(entity, signature);
        }

        /**
         * @brief Gets a component array
         */
    	template<typename T>
        std::shared_ptr<ComponentArray<T>> getComponentArray()
        {
            return componentManager->getComponentArray<T>();
        }
    	
        /**
         * @brief Removes a component from an entity
         */
        template <typename T>
        void removeComponent(Entity const& entity)
        {
            componentManager->removeComponent<T>(entity);

            auto signature = entityManager->getSignature(entity);
            signature.set(componentManager->getComponentType<T>(), false);
            entityManager->setSignature(entity, signature);

            systemManager->entitySignatureChanged(entity, signature);
        }

        /**
         * @brief Gets a system
         */
        template <typename T>
        std::shared_ptr<T> getSystem()
        {
            return systemManager->getSystem<T>();
        }

        template <typename T>
        bool hasComponent(Entity const& entity)
        {
            return componentManager->hasComponent<T>(entity);
        }

        /**
         * @brief Gets a reference to the component
         */
        template <typename T>
        T& getComponent(Entity const& entity)
        {
            return componentManager->getComponent<T>(entity);
        }

        /**
         * @brief Gets the component type
         */
        template <typename T>
        ComponentType getComponentType()
        {
            return componentManager->getComponentType<T>();
        }

        /**
         * @brief Registers a system
         */
        template <typename T>
        std::shared_ptr<T> registerSystem()
        {
            return systemManager->registerSystem<T>();
        }

        /**
         * @brief Sets the system signature
         */
        template <typename T>
        void setSystemSignature(Signature const& signature)
        {
            systemManager->setSignature<T>(signature);
        }

        /**
         * @brief Gets the system signature
         */
        template <typename T>
        Signature const getSystemSignature()
        {
            return systemManager->getSignature<T>();
        }

        // Gets the name of the module
        std::string getName() const override;

        // Starts up the module
        void startup() override;

        // Shuts down the module
        void shutdown() override;
    };

    inline std::shared_ptr<Coordinator> coordinator(new Coordinator());
} // namespace dag
