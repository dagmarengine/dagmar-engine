#pragma once

#include <cassert>
#include <memory>
#include <typeindex>
#include <unordered_map>

#include <iostream>

#include "dagmar/Exceptions.h"

#include "Component.h"
#include "Entity.h"
#include "System.h"

namespace dag
{
    /**
     * @brief System Manager class
     * Manages the systems and their signatures
     */
    class SystemManager
    {
      private:
        std::unordered_map<std::type_index, Signature> signatures;
        std::unordered_map<std::type_index, std::shared_ptr<System>> systems;

      public:
        ~SystemManager() = default;

        /**
         * @brief Checks if system exists in system
         */
        template <typename T>
        bool containsSystem()
        {
            std::type_index type = std::type_index(typeid(T));

            if (!systems.contains(type))
            {
                return false;
            }

            return true;
        }

        /**
         * @brief Tries to get a system
         */
        template <typename T>
        std::shared_ptr<T> getSystem()
        {
            std::type_index type = std::type_index(typeid(T));

            if (!systems.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::GetSystemSignatureNotExistent);
            }

            return std::static_pointer_cast<T>(systems[type]);
        }

        /**
         * @brief Tries to register a system
         */
        template <typename T>
        std::shared_ptr<T> registerSystem()
        {
            std::type_index type = std::type_index(typeid(T));

            if (systems.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::RegisterSystemAlreadyExistent);
            }

            auto system = std::make_shared<T>();
            systems.insert({type, system});
            return system;
        }

        /**
         * @brief Sets the signature of a system
         */
        template <typename T>
        void setSignature(Signature const& signature)
        {
            std::type_index type = std::type_index(typeid(T));

            if (!systems.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::SetSystemSignatureNotExistent);
            }

            signatures[type] = signature;
        }

        /**
         * @brief Gets the signature of a system
         */
        template <typename T>
        Signature const getSignature()
        {
            std::type_index type = std::type_index(typeid(T));

            if (!systems.contains(type))
            {
                throw dag::ECSException(dag::ECSException::ErrorType::GetSystemSignatureNotExistent);
            }

            return signatures[type];
        }

        // Erases a destroyed entity from all the systems
        void entityDestroyed(Entity const& entity);

        // Signals the systems that an entity's signature has changed
        void entitySignatureChanged(Entity const& entity, Signature const& signature);
    };
} // namespace dag
