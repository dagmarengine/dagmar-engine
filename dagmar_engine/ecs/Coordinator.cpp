#include "dagmar/Coordinator.h"
#include "dagmar/PhysicsSystem.h"

#include <iostream>

/**
 * @brief Startup function overriden for the module class
 */
void dag::Coordinator::startup()
{
    entityManager = std::make_unique<EntityManager>();
    componentManager = std::make_unique<ComponentManager>();
    systemManager = std::make_unique<SystemManager>();
}

/**
 * @brief Shutdown function overriden for the module class
 */
void dag::Coordinator::shutdown()
{
    entityManager.reset();
    componentManager.reset();
    systemManager.reset();
}

/**
 * @brief Gets the name of the module
 */
std::string dag::Coordinator::getName() const
{
    return "Coordinator";
}

/**
 * @brief Creates an entity
 */
dag::Entity dag::Coordinator::createEntity()
{
    return entityManager->createEntity();
}

/**
 * @brief Destroys an entity then signals the managers
 * that the entity has been destroyed
 */
void dag::Coordinator::destroyEntity(dag::Entity const& entity)
{
    entityManager->destroyEntity(entity);
    componentManager->entityDestroyed(entity);
    systemManager->entityDestroyed(entity);
}
