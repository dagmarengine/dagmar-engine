#include "dagmar/SystemManager.h"

/**
 * @brief Erase a destroyed entity from all the systems
 */
void dag::SystemManager::entityDestroyed(dag::Entity const& entity)
{
    for (auto const& pair : systems)
    {
        auto const& system = pair.second;
        system->entities.erase(entity);
    }
}

/**
 * @brief Signals all the systems that the signature of an entity has changed
 */
void dag::SystemManager::entitySignatureChanged(dag::Entity const& entity, dag::Signature const& signature)
{
    for (auto const& pair : systems)
    {
        auto const& type = pair.first;
        auto const& system = pair.second;
        auto const& systemSignature = signatures[type];

        if ((signature & systemSignature) == systemSignature)
        {
            system->entities.insert(entity);
        }
        else
        {
            // Doesn't match, then erase
            system->entities.erase(entity);
        }
    }
}
