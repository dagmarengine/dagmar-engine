#pragma once
#include "KeyEvent.h"
#include "dagmar/KeyEvent.h"

namespace dag
{
    /**
     * @brief KeyPressedEvent class
     *
     * Is a KeyEvent extension for pressed keys
     */
    class KeyPressedEvent : public KeyEvent
    {
      private:
        int repeatCount;

      public:
    	// Constructor
        KeyPressedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods, int const& repeatCount);

        int getRepeatCount() const;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag