#pragma once
#include "MouseButtonEvent.h"

namespace dag
{
    /**
     * @brief MouseButtonReleasedEvent class
     *
     * Is an extension of MouseButtonEvent, used on released
     */
    class MouseButtonReleasedEvent : public MouseButtonEvent
    {
      public:
        MouseButtonReleasedEvent(int const& button, int const& mods);

    	static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag