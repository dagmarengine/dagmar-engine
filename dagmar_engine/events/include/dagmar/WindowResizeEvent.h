#pragma once
#include "dagmar/WindowEvent.h"
#include <cstdint>

namespace dag
{
    /**
     * @brief WindowResizeEvent class
     *
     * Is an extension of the WindowEvent, called when
     * the window is resized
     */
    class WindowResizeEvent : public WindowEvent
    {
      private:
        uint32_t width;
        uint32_t height;

      public:
        WindowResizeEvent(uint32_t const& width, uint32_t const& height);

        uint32_t getWidth() const;
        uint32_t getHeight() const;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag