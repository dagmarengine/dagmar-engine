#pragma once
#include "dagmar/KeyEvent.h"

namespace dag
{
    /**
     * @brief KeyReleasedEvent class
     *
     * Is a KeyEvent extension, mainly called when a key is released
     */
    class KeyReleasedEvent : public KeyEvent
    {
      public:
    	// Constructor
        KeyReleasedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods);

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag