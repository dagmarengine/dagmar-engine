#pragma once
#include "dagmar/WindowEvent.h"

namespace dag
{
    /**
     * @brief WindowMovedEvent class
     *
     * Is an extension of WindowEvent, called when the
     * window was moved
     */
    class WindowMovedEvent : public WindowEvent
    {
      private:
        uint32_t x;
        uint32_t y;

      public:
        WindowMovedEvent(uint32_t const& x, uint32_t const& y);

        uint32_t getX() const;
        uint32_t getY() const;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag
