#include "dagmar/Event.h"

/**
 * @brief Overridable function that should print
 * debug info about the event
 */
std::string dag::Event::toString()
{
	return getName();
}

/**
 * @brief Checks if the event belongs to a category
 */
bool dag::Event::isInCategory(CategoryBitFlags const& flags) const
{
	return getCategoryBitFlags() & flags;
}