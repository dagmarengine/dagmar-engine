#include "dagmar/KeyEvent.h"

/**
 * @brief Key Event constructor
 */
dag::KeyEvent::KeyEvent(int const& keyCode, int const& scanCode, int const& action, int const& mods) :
    keyCode(keyCode), scanCode(scanCode), action(action), mods(mods)
{
}

/**
 * @brief Gets the keycode from a key event
 */
int dag::KeyEvent::getKeyCode() const
{
    return keyCode;
}

int dag::KeyEvent::getScanCode() const
{
    return scanCode;
}

int dag::KeyEvent::getAction() const
{
    return action;
}

int dag::KeyEvent::getMods() const
{
    return mods;
}

/**
 * @brief Gets the category flags of the key event
 */
int dag::KeyEvent::getCategoryBitFlags() const
{
    return CategoryBitFlags::Input | CategoryBitFlags::Keyboard;
}