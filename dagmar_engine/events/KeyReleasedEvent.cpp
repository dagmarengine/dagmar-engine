#include "dagmar/KeyReleasedEvent.h"

/**
 * @brief Constructor for key released event
 */
dag::KeyReleasedEvent::KeyReleasedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods) : KeyEvent(keycode, scanCode, action, mods)
{
}

/**
 * @brief Gets the static type of the event
 */
dag::Event::Type dag::KeyReleasedEvent::getStaticType()
{
    return Type::KeyReleased;
}

/**
 * @brief Gets the type of the event
 */
dag::Event::Type dag::KeyReleasedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::KeyReleasedEvent::getName() const
{
    return "KeyReleasedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::KeyReleasedEvent::toString()
{
    return getName() + ":" + std::to_string(keyCode);
}