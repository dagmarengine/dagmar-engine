#include "dagmar/MouseButtonReleasedEvent.h"

/**
 * @brief Constructor for mouse button released event
 */
dag::MouseButtonReleasedEvent::MouseButtonReleasedEvent(int const& button, int const& mods) :
    MouseButtonEvent(button, mods)
{
}

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::MouseButtonReleasedEvent::getStaticType()
{
    return Type::MouseButtonReleased;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::MouseButtonReleasedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::MouseButtonReleasedEvent::getName() const
{
    return "MouseButtonReleasedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::MouseButtonReleasedEvent::toString()
{
    return getName() + ": " + std::to_string(button);
}
