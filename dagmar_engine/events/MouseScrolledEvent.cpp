#include "dagmar/MouseScrolledEvent.h"

/**
 * @brief Constructor for mouse scroll event
 */
dag::MouseScrolledEvent::MouseScrolledEvent(float const& xOffset, float const& yOffset) :
    xOffset(xOffset), yOffset(yOffset)
{
}

/**
 * @brief Gets the x offset
 */
float dag::MouseScrolledEvent::getXOffset() const
{
    return xOffset;
}

/**
 * @brief Gets the y offset
 */
float dag::MouseScrolledEvent::getYOffset() const
{
    return yOffset;
}

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::MouseScrolledEvent::getStaticType()
{
    return Type::MouseScrolled;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::MouseScrolledEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of th event
 */
std::string dag::MouseScrolledEvent::getName() const
{
    return "MouseScrolledEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::MouseScrolledEvent::toString()
{
    return getName() + ": xoffset=" + std::to_string(xOffset) + " yoffset=" + std::to_string(yOffset);
}

/**
 * @brief Gets the category bit flags
 */
int dag::MouseScrolledEvent::getCategoryBitFlags() const
{
    return CategoryBitFlags::Mouse | CategoryBitFlags::Input;
}