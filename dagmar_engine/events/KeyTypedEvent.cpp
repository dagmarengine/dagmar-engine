#include "dagmar/KeyTypedEvent.h"

/**
 * @brief Constructor for key typed event
 */
dag::KeyTypedEvent::KeyTypedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods) :
    KeyEvent(keycode, scanCode, action, mods)
{
}

/**
 * @brief Gets the static type of the event
 */
dag::Event::Type dag::KeyTypedEvent::getStaticType()
{
    return Type::KeyTyped;
}

/**
 * @brief Gets the type of the event
 */
dag::Event::Type dag::KeyTypedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::KeyTypedEvent::getName() const
{
    return "KeyTypedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::KeyTypedEvent::toString()
{
    return getName() + ":" + std::to_string(keyCode);
}