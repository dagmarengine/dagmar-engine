#pragma once

#include "dagmar/Module.h"

#include <memory>
#include <vector>
#include <fstream>
#include <atomic>
#include <thread>
#include <mutex>
#include <vector>

#ifdef _WIN32
#include "windows.h"
#endif

namespace dag
{
#ifdef _WIN32
	typedef DWORDLONG TotalMemoryType;
	typedef SIZE_T MemoryType;
#else
	typedef long long TotalMemoryType;
	typedef int MemoryType;
#endif

	class Profiler : public Module
	{

	private:
		std::thread profilerThread;
		std::atomic<bool> shuttingDown;
		std::mutex requestMutex;
		std::vector<float> previous100;
		std::mutex prevVecMutex;
		float maxMemThusFar = 0.0f;

#ifdef _WIN32
		MEMORYSTATUSEX memInfo;
		ULARGE_INTEGER lastCPU, lastSysCPU, lastUserCPU;
		int numProcessors;
		HANDLE self;
#endif
	public:
		std::vector<MemoryType> memoryLog;
		std::vector<float> cpuLog;
		std::ofstream outputCpuLog;
		std::ofstream outputMemoryLog;

		void startup() override;
		void shutdown() override;
		std::string getName() const override
		{
			return "Profiler";
		}

		dag::TotalMemoryType getTotalVirtualMemory();
		dag::MemoryType getCurrentVirtualMemoryUsed();

		dag::TotalMemoryType getTotalPhysicalMemory();
		dag::MemoryType getCurrentPhysicalMemoryUsed();

		double getCurrentCPUUsed();

		void imguiPlotMem();
	};

	inline std::shared_ptr<Profiler> profiler(new Profiler());
}