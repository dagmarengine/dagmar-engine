dagmar_add_library(systems Dagmar::Systems STATIC
    PhysicsSystem.cpp
    RenderingSystem.cpp
    CameraSystem.cpp
    PlayerControllerSystem.cpp
    LightSystem.cpp
    ScriptSystem.cpp
    SystemsManager.cpp)

target_include_directories(systems PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(systems PUBLIC
    Threads::Threads
    glew::glew
    glm::glm
    Dagmar::Components
    Dagmar::ECS
    Dagmar::Renderer
    Dagmar::Graphics
    Dagmar::ModuleLifecycle
    Dagmar::Layers
    Dagmar::Voxel
)
