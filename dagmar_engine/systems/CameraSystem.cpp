#include "dagmar/CameraSystem.h"
#include "dagmar/Log.h"

/**
 * @brief Default cosntructor to initialise the global variables & coefficients
  */
dag::CameraSystem::CameraSystem()
{
    editorCamera = UINT32_MAX;
    activeCamera = UINT32_MAX;
}

void dag::CameraSystem::setEditorCamera(Entity const& entity)
{
    editorCamera = entity;
    activeCamera = entity;
}

dag::CameraSystem::~CameraSystem()
{
}

/**
 * @brief Register the needed components and the system
  */

void dag::CameraSystem::startup()
{
    coordinator->registerComponent<dag::CameraComponent>();

    dag::Signature CameraSystemSignature;
    CameraSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    CameraSystemSignature.set(coordinator->getComponentType<dag::CameraComponent>());

    coordinator->setSystemSignature<dag::CameraSystem>(CameraSystemSignature);
}

/**
 * @brief 
  */
std::vector<dag::Descriptor> const& dag::CameraSystem::getCurrentCamera()
{
    // assumes view, projection and cameraPosition in this order
    auto& cameraComponent = coordinator->getComponent<CameraComponent>(activeCamera);
    auto& transform = coordinator->getComponent<Transform>(activeCamera);

    descriptors[0].data = cameraComponent.getView(transform.position, transform.rotation);
    descriptors[1].data = cameraComponent.getProjection();
    descriptors[2].data = transform.position;

    return descriptors;
}

/**
 * @brief deactivate all the other cameras
  */
void dag::CameraSystem::setPossessed(dag::Entity const& activeEntity, bool active)
{
    // we possesed a camera
    if (active)
    {
        activeCamera = activeEntity;
        for (auto& entity : entities)
        {
            if (entity != activeEntity)
            {
                auto& cameraComponent = coordinator->getComponent<CameraComponent>(entity);
                cameraComponent.isPossessed = false;
            }
        }
    }
    else
    {
        // go back to default editor camera
        activeCamera = editorCamera;
        auto& cameraComponent = coordinator->getComponent<CameraComponent>(editorCamera);
        cameraComponent.isPossessed = true;
    }
}