#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"

/**
 * @brief Light system
  */

namespace dag
{
    class LightSystem : public System
    {
      public:
        std::shared_ptr<Coordinator> coordinator;
        std::vector<Descriptor> descriptors;

    	// Used for faster lookup once we add a light
        std::vector<size_t> directionalLightsIDs;
        std::vector<Entity> directionalLightsEntities;
        std::vector<size_t> spotLightsIDs;
        std::vector<Entity> spotLightsEntities;
        std::vector<size_t> pointLightsIDs;
        std::vector<Entity> pointLightsEntities;
    
      public:
        LightSystem();
        ~LightSystem() override;

        void startup();

        // returns the first light for the moment TODO adapt to multiple lights
        std::vector<Descriptor> const& getLights();
        std::vector<Descriptor> getLightCameraDescriptors(dag::Entity const& entity);

    	void updateLookupVectors();
    };
} // namespace dag