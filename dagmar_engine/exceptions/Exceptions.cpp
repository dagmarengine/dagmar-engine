#include <GL/glew.h>
#include "dagmar/Exceptions.h"

/**
 * @brief Returns error message for compilation
 */
const char* dag::ShaderCompilationException::what() const throw ()
{
    switch (shader_type)
    {
        case GL_FRAGMENT_SHADER:
        {
            return "Fragment shader compilation failed.";
            break;
        }
        case GL_VERTEX_SHADER:
        {
            return "Vertex shader compilation failed.";
            break;
        }
        case GL_TESS_CONTROL_SHADER:
        {
            return "Tesselation control shader compilation failed.";
            break;
        }
        case GL_TESS_EVALUATION_SHADER:
        {
            return "Tesselation evaluation shader compilation failed.";
            break;
        }
        case GL_GEOMETRY_SHADER:
        {
            return "Geometry shader compilation failed.";
            break;
        }
        case GL_COMPUTE_SHADER:
        {
            return "Compute shader compilation failed.";
            break;
        }
        default:
        {
            return "Unknown shader compilation failed.";
            break;
        }
    }
}

/**
 * @brief Returns error message for linking
 */
const char* dag::ShaderProgramLinkingException::what() const throw ()
{
    return "Shader program linking failed.";
}

/**
 * @brief Returns the error message for ecs
 */
const char* dag::ECSException::what() const throw ()
{
	switch (errorType)
	{
        case ErrorType::InsertComponentAlreadyExistent:
        {
            return "Insert component failed: Component already added to the entity.";
            break;
        }
        case ErrorType::RemoveComponentNotExistent:
        {
            return "Remove component failed: Component does not exist.";
            break;
        }
        case ErrorType::GetComponentNotExistent:
        {
            return "Get component failed: Component does not exist.";
            break;
        }
        case ErrorType::RegisterComponentAlreadyExistent:
        {
            return "Register component failed: Component already registered.";
            break;
        }
        case ErrorType::GetComponentTypeNotExistent:
        {
            return "Get component type failed: Component does not exist.";
            break;
        }
        case ErrorType::GetComponentArrayNotExistent:
        {
            return "Get component array failed: Component array does not exist.";
            break;
        }
        case ErrorType::RegisterSystemAlreadyExistent:
        {
            return "Register system failed: System already existent.";
            break;
        }
        case ErrorType::SetSystemSignatureNotExistent:
        {
            return "Set system signature failed: System does not exist.";
            break;
        }
        case ErrorType::GetSystemSignatureNotExistent:
        {
            return "Get system signature failed: System does not exist.";
            break;
        }
        case ErrorType::CreateEntityTooManyEntities:
        {
            return "Create entity failed: MAX_ENTITIES limit has been reached.";
            break;
        }
        case ErrorType::DestroyEntityOutOfRange:
        {
            return "Destroy entity failed: Entity out of range.";
            break;
        }
        case ErrorType::SetEntitySignatureOutOfRange:
        {
            return "Set entity signature failed: Entity out of range.";
            break;
        }
        case ErrorType::GetEntitySignatureOutOfRange:
        {
            return "Get entity signature failed: Entity out of range.";
            break;
        }
        default:
        {
            return "Unknown ECS error.";
            break;
        }
	}
}