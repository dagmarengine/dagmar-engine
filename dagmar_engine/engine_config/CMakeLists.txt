dagmar_add_library(engine_config Dagmar::EngineConfig STATIC
    EngineConfig.cpp
)

target_include_directories(engine_config PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(engine_config
    glm::glm
    Dagmar::ModuleLifecycle
    Dagmar::Filesystem
)
