#include "dagmar/EngineConfig.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Module.h"

#include <algorithm>
#include <cctype>
#include <string>

void dag::EngineConfig::startup()
{
	parse(dag::filesystem::getAssetsPath() / "EngineConfig.ini");
}

void dag::EngineConfig::shutdown()
{
}

/**
 * @brief Get name of the module
 */
std::string dag::EngineConfig::getName() const
{

    return "EngineConfig";
}

/**
 * @brief Parse the EngineConfig.ini file
 */
void dag::EngineConfig::parse(std::filesystem::path const& path)
{
	if ((!std::filesystem::is_regular_file(path)) || (!std::filesystem::exists(path)))
	{
		Log::error("ini file not existant at: ", path.generic_string());
		exit(EXIT_FAILURE);
	}

	dag::Sections tempSection;
	std::string tempKey, tempValue;
	std::ifstream configFile(path.string());
	std::string stream;

	while(std::getline(configFile, stream))
	{
        stream.erase(std::remove_if(stream.begin(), stream.end(), [](unsigned char x){return std::isspace(x);}),stream.end());
		if(stream[0] == '[')
		{
			// Code for section parsing
			if(stream.find(']') == std::string::npos)
			{
				Log::error("Missing ']'");
				exit(EXIT_FAILURE);
			}

			// Push the first section if existent
			if(tempSection.sectionName.length() != 0)
			{
				arrSections.push_back(tempSection);
				tempSection.properties.clear();
			}

			// Create a new section
			tempSection.sectionName = stream.substr(1, stream.find(']') - 1);
		}
		else
		{
			// Code for inner parsing
			if(tempSection.sectionName.length() == 0)
			{
				Log::error("Missing section.");
				exit(EXIT_FAILURE);
			}

            if(stream.size() == 0)
            {
                continue;
            }

			if(stream.find('=') == std::string::npos)
			{
				Log::error("Missing '='.");
				exit(EXIT_FAILURE);
			}


			tempKey = stream.substr(0, stream.find('='));
			tempValue = stream.substr(stream.find('=') + 1, stream.size());
			tempSection.properties.insert(std::pair<std::string, std::string>(tempKey, tempValue));
		}
	}

    arrSections.push_back(tempSection);
	configFile.close();
}

/**
 * @brief Access a value according to the key and the scetion it belongs to
 */
std::string dag::EngineConfig::getValue(const std::string key, const std::string sectionName)
{
    std::map<std::string, std::string>::iterator itr;
    for (int i = 0; i < arrSections.size(); i++)
    {
        if (sectionName == arrSections[i].sectionName)
        {
            return arrSections[i].properties.find(key)->second;
        }
    }
    Log::error("Key: " + key + " not found");
    exit(EXIT_FAILURE);
}
