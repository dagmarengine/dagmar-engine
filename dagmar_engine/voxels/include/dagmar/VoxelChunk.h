#pragma once

#include "Voxel.h"
#include "dagmar/Mesh.h"
#include "dagmar/RenderingSystem.h"

namespace dag
{
    class VoxelChunk
    {
      public:
        std::vector<Voxel> chunk;
        size_t blockSize;
        size_t blockHeight;
        Voxel outside;
        glm::vec3 startingPoint;
        int index;
        float sqr3 = 1.7321f;

        float scale2D;
        float scale3D;
        int iterations3D;
        int iterations2D;
        float persistence2D;
        float persistence3D;
        bool hasWater;
        int waterHeight;
        bool hasTrees;
        bool hasFlowers;
        float treeProbability;
        float flowerProbability;
        int minTreeHeight;
        int maxTreeHeight;
        int minTreeWidth;
        int maxTreeWidth;
        int terrainType;         

        static float randomInRange(float min, float max);

        VoxelChunk(size_t size, glm::vec3 position, int terrainType, float scale2D, float scale3D, int iterations2D, int iterations3D, float persistence2D, float persistence3D, bool hasWater, int waterHeight, bool hasTrees, float treeProbability, int minTreeHeight, int maxTreeHeight, int minTreeWidth, int maxTreeWidth, float flowerProbability, bool hasFlowers);

        inline size_t getIndex(int x, int y, int z)
        {
            if (x < 0 || x >= blockSize || y < 0 || y >= blockHeight || z < 0 || z >= blockSize)
            {
                return -1;
            }
            return y * blockSize * blockSize + x * blockSize + z;
        };
        Voxel& getVoxel(size_t x, size_t y, size_t z);
        Voxel& getVoxel(glm::vec3 const& position);
        glm::vec3 getVoxelPosition(glm::vec3 const& position);
        dag::Mesh generateMesh();
        void generateTerrainFrom3DNoise();
        void generateTerrainFrom2DNoise();
        void generate3DTerrainFrom2DNoise();
        void generateTerrain();
        void fillTerrain(size_t height);
        bool spawnTree(size_t i, size_t j, size_t k);
        bool spawnFlower(size_t i, size_t j, size_t k);
        
        BlockTypes getColor(size_t const& height);
        void generateTris(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec4>& colors, std::vector<uint32_t>& indices);
    };
} // namespace dag