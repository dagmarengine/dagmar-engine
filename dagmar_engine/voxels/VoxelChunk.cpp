#include "dagmar/VoxelChunk.h"
#include <glm/gtc/noise.hpp>

float dag::VoxelChunk::randomInRange(float min, float max)
{
    // get range
    float range = max - min;
    // get random number
    float randomNumber = rand();
    // scale to given range
    randomNumber /= RAND_MAX;

    randomNumber *= range;
    // translate to min
    randomNumber += min;

    return randomNumber;
}

dag::VoxelChunk::VoxelChunk(size_t size, glm::vec3 position, int terrainType, float scale2D, float scale3D, int iterations2D, int iterations3D, float persistence2D, float persistence3D, bool hasWater, int waterHeight, bool hasTrees, float treeProbability, int minTreeHeight, int maxTreeHeight, int minTreeWidth, int maxTreeWidth, float flowerProbability, bool hasFlowers)
{
    this->blockHeight = 8 * size;
    this->blockSize = size;
    // std::cout << "SIZE: " << size * size * size * 8 << std::endl;
    chunk.resize(blockSize * blockSize * blockHeight, Voxel());
    outside = Voxel();
    outside.active = false;
    outside.blockType = BlockTypes::Unused;
    this->startingPoint = position;
    this->terrainType = terrainType;
    this->scale2D = scale2D;
    this->scale3D = scale3D;
    this->iterations2D = iterations2D;
    this->iterations3D = iterations3D;
    this->persistence2D = persistence2D;
    this->persistence3D = persistence3D;
    this->hasWater = hasWater;
    this->waterHeight = waterHeight;
    this->hasTrees = hasTrees;
    this->hasFlowers = hasFlowers;
    this->treeProbability = treeProbability;
    this->minTreeHeight = minTreeHeight;
    this->maxTreeHeight = maxTreeHeight;
    this->minTreeWidth = minTreeWidth;
    this->maxTreeWidth = maxTreeWidth;
    this->flowerProbability = flowerProbability;
}

dag::Voxel& dag::VoxelChunk::getVoxel(size_t x, size_t y, size_t z)
{
    int index = getIndex(x, y, z);
    if (index != -1)
    {
        return chunk[index];
    }
    else
    {
        return outside;
    }
}

dag::Voxel& dag::VoxelChunk::getVoxel(glm::vec3 const& position)
{
    glm::vec3 newPosition = position - startingPoint;
    // newPosition /= (float)size;
    // std::cout << "Position " << position.x << ' ' << position.y << ' ' << position.z << std::endl;
    // std::cout << "starting position " << startingPoint.x << ' ' << startingPoint.y << ' ' << startingPoint.z << std::endl;
    // std::cout << "Index " << (size_t)newPosition.x << ' ' << (size_t)newPosition.y << ' ' << (size_t)newPosition.z << std::endl;
    size_t index = getIndex((int)newPosition.x, (int)newPosition.y, (int)newPosition.z);
    if (index != -1)
    {
        return chunk[index];
    }
    else
    {
        return outside;
    }
}

glm::vec3 dag::VoxelChunk::getVoxelPosition(glm::vec3 const& position)
{
    return glm::vec3((size_t)position.x + 0.5f, (size_t)position.y + 0.5f, (size_t)position.z + 0.5f);
}

float noiseSample(int iterations, float x, float y, int low, int high, float persistence, float scale)
{
    float amplitude = 1.0f;
    float maxAmplitude = 0.0f;
    float frequency = scale;
    float noise = 0.0f;

    for (int i = 0; i < iterations; ++i)
    {
        noise += glm::simplex(glm::vec2(x, y) * frequency) * amplitude;
        frequency *= 2.0f;
        maxAmplitude += amplitude;
        amplitude *= persistence;
    }

    noise /= maxAmplitude;

    noise = low + (noise + 1.0f) * 0.5f * (high - low);

    // noise = noise * (high - low) * 0.5f + (high - low) * 0.5f;

    return noise;
}

float noiseSample(int iterations, float x, float y, float z, float persistence, float scale)
{
    float amplitude = 1.0f;
    float maxAmplitude = 0.0f;
    float frequency = scale;
    float noise = 0.0f;

    for (int i = 0; i < iterations; ++i)
    {
        noise += glm::simplex(glm::vec3(x, y, z) * frequency) * amplitude;
        frequency *= 2.0f;
        maxAmplitude += amplitude;
        amplitude *= persistence;
    }

    noise /= maxAmplitude;

    return noise;
}

dag::BlockTypes dag::VoxelChunk::getColor(size_t const& height)
{
    for (size_t i = 0; i < blockColorSize; ++i)
    {
        if (height > blockColorLimits[i] * blockHeight)
        {
            return static_cast<BlockTypes>(i);
        }
    }
    return BlockTypes::Default;
}

void dag::VoxelChunk::fillTerrain(size_t height)
{
    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockSize; ++j)
        {
            for (size_t k = 0; k <= height; ++k)
            {
                auto& voxel = getVoxel(i, k, j);

                if (!voxel.active)
                {
                    voxel.active = true;
                    voxel.blockType = BlockTypes::Water;
                }
            }
        }
    }
}

void dag::VoxelChunk::generateTerrain()
{
    switch (terrainType)
    {
        case 0:
            generateTerrainFrom2DNoise();
            break;
        case 1:
            generateTerrainFrom3DNoise();
            break;
        case 2:
            generate3DTerrainFrom2DNoise();
            break;

        default:
            generate3DTerrainFrom2DNoise();
            break;
    }
    if (hasWater)
    {
        fillTerrain(waterHeight);
    }
}

void dag::VoxelChunk::generateTerrainFrom2DNoise()
{
    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockSize; ++j)
        {
            getVoxel(i, 0, j).active = true;

            size_t height = noiseSample(iterations2D, i + startingPoint.x, j + startingPoint.z, 0, blockHeight - 1, persistence2D, scale2D);

            for (size_t k = height; k >= 1; --k)
            {
                auto& voxel = getVoxel(i, k, j);
                voxel.active = true;

                if (voxel.active)
                {
                    voxel.blockType = getColor(k);

                    if (voxel.blockType == BlockTypes::Grass)
                    {

                        if (hasTrees)
                        {
                            if (spawnTree(i, k, j))
                            {
                                continue;
                            }
                        }
                        if (hasFlowers)
                        {
                            spawnFlower(i, k, j);
                        }
                    }
                }
            }
        }
    }
}

void dag::VoxelChunk::generate3DTerrainFrom2DNoise()
{

    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockSize; ++j)
        {
            getVoxel(i, 0, j).active = true;

            size_t height = noiseSample(iterations2D, i + startingPoint.x, j + startingPoint.z, waterHeight / 2.0f, blockHeight - 1, persistence2D, scale2D);

            for (size_t k = height; k >= 1; --k)
            {
                auto& voxel = getVoxel(i, k, j);

                auto val = noiseSample(iterations3D, i + startingPoint.x, j + startingPoint.z, k + startingPoint.y, persistence3D, scale3D);

                voxel.active = val > 0 ? true : false;
                if (voxel.active)
                {
                    voxel.blockType = getColor(k);

                    if (voxel.blockType == BlockTypes::Grass)
                    {

                        if (hasTrees)
                        {
                            if (spawnTree(i, k, j))
                            {
                                continue;
                            }
                        }
                        if (hasFlowers)
                        {
                            spawnFlower(i, k, j);
                        }
                    }
                }
            }
        }
    }
}

bool dag::VoxelChunk::spawnTree(size_t x, size_t y, size_t z)
{
    if (getVoxel(x, y + 1, z).active)
    {
        return false;
    }
    float result = noiseSample(1, x + startingPoint.x, z + startingPoint.z, 0, 9, 0.5f, 1.0f);
    // std::cout << result << std::endl;
    if (result < treeProbability)
    {
        int height = randomInRange(minTreeHeight, maxTreeHeight);
        int width = randomInRange(minTreeWidth, maxTreeWidth);

        // woody part
        for (size_t i = 1; i <= height; ++i)
        {
            auto& voxel = getVoxel(x, y + i, z);
            voxel.active = true;
            voxel.blockType = BlockTypes::Wood;
        }
        // std::cout << "Original: " << x << ' ' << y << ' ' << z << std::endl;
        // std::cout << "Width: " << width << std::endl;
        // std::cout << "Height: " << height << std::endl;

        for (int i = -width; i <= width; ++i)
        {
            for (int j = -width; j <= width; ++j)
            {
                for (int k = -width; k <= width; ++k)
                {
                    // std::cout << "Generated: " << x + i << ' ' << y + j + height + 1 << ' ' << z + k << std::endl;

                    auto& voxel = getVoxel(x + i, y + j + height + 1, z + k);

                    if (voxel.active || voxel.blockType == BlockTypes::Unused)
                    {
                        continue;
                    }

                    voxel.active = true;
                    voxel.blockType = BlockTypes::Leaves;
                }
            }
        }

        return true;
    }
    return false;
}

bool dag::VoxelChunk::spawnFlower(size_t x, size_t y, size_t z)
{
    if (getVoxel(x, y + 1, z).active)
    {
        return false;
    }
    float result = noiseSample(1, x + startingPoint.x, z + startingPoint.z, 0, 9, 0.6f, 1.1f);
    // std::cout << result << std::endl;

    if (result < flowerProbability)
    {

        auto& voxel = getVoxel(x, y + 1, z);
        voxel.active = true;
        voxel.blockType = BlockTypes::Flower;
    }
    return true;
}

void dag::VoxelChunk::generateTerrainFrom3DNoise()
{

    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockSize; ++j)
        {
            getVoxel(i, 0, j).active = true;

            for (size_t k = 1; k < blockHeight; ++k)
            {

                auto val = noiseSample(iterations3D, i + startingPoint.x, j + startingPoint.z, k + startingPoint.y, persistence3D, scale3D);

                auto& voxel = getVoxel(i, k, j);

                voxel.active = val > 0 ? true : false;
                if (voxel.active)
                {
                    voxel.blockType = getColor(k);

                    if (voxel.blockType == BlockTypes::Grass)
                    {

                        if (hasTrees)
                        {
                            if (spawnTree(i, k, j))
                            {
                                continue;
                            }
                        }
                        if (hasFlowers)
                        {
                            spawnFlower(i, k, j);
                        }
                    }
                }
            }
        }
    }
}

void dag::VoxelChunk::generateTris(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec4>& colors, std::vector<uint32_t>& indices)
{
    std::vector<int> corners(4);

    std::array<glm::vec3, 6> faceNormals = {
        glm::vec3(1, 0, 0),
        glm::vec3(-1, 0, 0),
        glm::vec3(0, 1, 0),
        glm::vec3(0, -1, 0),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, -1)};

    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockHeight; ++j)
        {
            for (size_t k = 0; k < blockSize; ++k)
            {
                auto& voxel = getVoxel(i, j, k);
                if (!voxel.active)
                {
                    continue;
                }

                if (!getVoxel(i, j, k - 1).active)
                {
                    // front face
                    vertices.push_back(startingPoint + glm::vec3(i, j, k));
                    normals.push_back(faceNormals[5]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k));
                    normals.push_back(faceNormals[5]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k));
                    normals.push_back(faceNormals[5]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k));
                    normals.push_back(faceNormals[5]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }

                if (!getVoxel(i - 1, j, k).active)
                {
                    // left face
                    vertices.push_back(startingPoint + glm::vec3(i, j, k + 1));
                    normals.push_back(faceNormals[1]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j, k));
                    normals.push_back(faceNormals[1]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k));
                    normals.push_back(faceNormals[1]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k + 1));
                    normals.push_back(faceNormals[1]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }

                if (!getVoxel(i, j, k + 1).active)
                {
                    // back face
                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k + 1));
                    normals.push_back(faceNormals[4]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j, k + 1));
                    normals.push_back(faceNormals[4]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k + 1));
                    normals.push_back(faceNormals[4]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k + 1));
                    normals.push_back(faceNormals[4]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }

                if (!getVoxel(i + 1, j, k).active)
                {
                    // right face
                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k));
                    normals.push_back(faceNormals[0]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k + 1));
                    normals.push_back(faceNormals[0]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k + 1));
                    normals.push_back(faceNormals[0]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k));
                    normals.push_back(faceNormals[0]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }

                if (!getVoxel(i, j + 1, k).active)
                {
                    // top face
                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k));
                    normals.push_back(faceNormals[2]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k));
                    normals.push_back(faceNormals[2]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k + 1));
                    normals.push_back(faceNormals[2]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j + 1, k + 1));
                    normals.push_back(faceNormals[2]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }

                if (!getVoxel(i, j - 1, k).active)
                {
                    // bottom face
                    vertices.push_back(startingPoint + glm::vec3(i, j, k + 1));
                    normals.push_back(faceNormals[3]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[0] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k + 1));
                    normals.push_back(faceNormals[3]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[1] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i + 1, j, k));
                    normals.push_back(faceNormals[3]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[2] = vertices.size() - 1;

                    vertices.push_back(startingPoint + glm::vec3(i, j, k));
                    normals.push_back(faceNormals[3]);
                    colors.push_back(blockColors[voxel.blockType]);
                    corners[3] = vertices.size() - 1;

                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }
            }
        }
    }
}

dag::Mesh dag::VoxelChunk::generateMesh()
{
    std::vector<glm::vec3> vertices, normals;
    std::vector<uint32_t> indices;
    int counter = 0;

    for (size_t i = 0; i < blockSize; ++i)
    {
        for (size_t j = 0; j < blockHeight; ++j)
        {
            for (size_t k = 0; k < blockSize; ++k)
            {
                if (!getVoxel(i, j, k).active)
                {
                    continue;
                }
                std::array<int, 8> corners;
                vertices.push_back(startingPoint + glm::vec3(i, j, k));
                normals.push_back(glm::vec3(-sqr3, -sqr3, -sqr3));
                corners[0] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i + 1, j, k));
                normals.push_back(glm::vec3(sqr3, -sqr3, -sqr3));
                corners[1] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k));
                normals.push_back(glm::vec3(sqr3, -sqr3, sqr3));
                corners[2] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i, j + 1, k));
                normals.push_back(glm::vec3(-sqr3, -sqr3, sqr3));
                corners[3] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i, j, k + 1));
                normals.push_back(glm::vec3(-sqr3, sqr3, -sqr3));
                corners[4] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i + 1, j, k + 1));
                normals.push_back(glm::vec3(sqr3, sqr3, -sqr3));
                corners[5] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i + 1, j + 1, k + 1));
                normals.push_back(glm::vec3(sqr3, sqr3, sqr3));
                corners[6] = vertices.size() - 1;

                vertices.push_back(startingPoint + glm::vec3(i, j + 1, k + 1));
                normals.push_back(glm::vec3(-sqr3, sqr3, sqr3));
                corners[7] = vertices.size() - 1;

                // front face
                if (!getVoxel(i, j - 1, k).active)
                {
                    indices.push_back(corners[0]);
                    indices.push_back(corners[1]);
                    indices.push_back(corners[5]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[5]);
                    indices.push_back(corners[4]);
                }

                // left face
                if (!getVoxel(i - 1, j, k).active)
                {
                    indices.push_back(corners[3]);
                    indices.push_back(corners[4]);
                    indices.push_back(corners[7]);

                    indices.push_back(corners[3]);
                    indices.push_back(corners[0]);
                    indices.push_back(corners[4]);
                }

                // right face
                if (!getVoxel(i + 1, j, k).active)
                {
                    indices.push_back(corners[1]);
                    indices.push_back(corners[6]);
                    indices.push_back(corners[5]);

                    indices.push_back(corners[1]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[6]);
                }

                // back face
                if (!getVoxel(i, j + 1, k).active)
                {
                    indices.push_back(corners[2]);
                    indices.push_back(corners[7]);
                    indices.push_back(corners[6]);

                    indices.push_back(corners[2]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[7]);
                }

                // top face
                if (!getVoxel(i, j, k + 1).active)
                {
                    indices.push_back(corners[4]);
                    indices.push_back(corners[6]);
                    indices.push_back(corners[7]);

                    indices.push_back(corners[4]);
                    indices.push_back(corners[5]);
                    indices.push_back(corners[6]);
                }

                // bottom face
                if (!getVoxel(i, j, k - 1).active)
                {
                    indices.push_back(corners[0]);
                    indices.push_back(corners[2]);
                    indices.push_back(corners[1]);

                    indices.push_back(corners[0]);
                    indices.push_back(corners[3]);
                    indices.push_back(corners[2]);
                }
            }
        }
    }

    std::vector<float> verticesFloat(vertices.size() * 6);

    for (int v = 0; v < vertices.size(); ++v)
    {
        verticesFloat[6 * v] = vertices[v].x;
        verticesFloat[6 * v + 1] = vertices[v].y;
        verticesFloat[6 * v + 2] = vertices[v].z;

        // if (v < models[index].vertices.normals.size())
        // {
        verticesFloat[6 * v + 3] = normals[v].x;
        verticesFloat[6 * v + 4] = normals[v].y;
        verticesFloat[6 * v + 5] = normals[v].z;
        // }
        // else
        // {
        //     verticesFloat[6 * v + 3] = 0;
        //     verticesFloat[6 * v + 4] = 1;
        //     verticesFloat[6 * v + 5] = 0;
        // }
    }

    // std::vector<uint32_t> indices;
    // for (int s = 0; s < models[index].shapes.size(); ++s)
    // {
    //     for (int f = 0; f < models[index].shapes[s].faces.size(); ++f)
    //     {
    //         for (int i = 0; i < models[index].shapes[s].faces[f].vertexIndex.size(); ++i)
    //         {
    //             indices.emplace_back(models[index].shapes[s].faces[f].vertexIndex[i]);
    //         }
    //     }
    // }

    return dag::Mesh(verticesFloat, indices);
}