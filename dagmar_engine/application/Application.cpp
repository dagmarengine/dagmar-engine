#include "dagmar/Application.h"
#include "dagmar/EventDispatcher.h"
#include "dagmar/RenderingSystem.h"

dag::Application::Application()
{
#ifdef _WIN32
    #ifdef NDEBUG
    FreeConsole();
    #endif
#endif

    ModuleLifecycle::get().startup();

    //windowManager->createMainWindow();
    windowManager->window.setEventCallbackFn(std::bind(&Application::onEvent, this, std::placeholders::_1));

    running = true;
    minimized = false;
    lastFrameTime = 0;
}

dag::Application::~Application()
{
    ModuleLifecycle::get().shutdown();
}

void dag::Application::addLayer(std::shared_ptr<Layer>& layer)
{
    layerManager.addLayer(layer);
}

void dag::Application::removeLayer(std::shared_ptr<Layer>& layer)
{
    layerManager.removeLayer(layer);
}

void dag::Application::run()
{
}

void dag::Application::onEvent(Event& e)
{
    EventDispatcher dispatcher(e);
    dispatcher.dispatch<WindowCloseEvent>(std::bind(&dag::Application::onWindowClose, this, std::placeholders::_1));
    dispatcher.dispatch<WindowResizeEvent>(std::bind(&dag::Application::onWindowResize, this, std::placeholders::_1));

    if (running)
    {
        for (auto& layer : layerManager.layers)
        {
            if (e.consumed)
            {
                break;
            }

            layer->onEvent(e);
        }
    }
}

bool dag::Application::onWindowClose(WindowCloseEvent& e)
{
    running = false;
    return true;
}

bool dag::Application::onWindowResize(WindowResizeEvent& e)
{
    if (e.getWidth() == 0 || e.getHeight() == 0)
    {
        minimized = true;
        return false;
    }

    minimized = false;

    windowManager->window.info.width = e.getWidth();
    windowManager->window.info.height = e.getHeight();

    return false;
}
