#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "dagmar/PlatformDetector.h"

TEST_CASE("Plaform Detector Tests")
{
    dag::platformDetector->startup();

    SECTION("TEST 1")
    {
#ifdef __linux__
        REQUIRE(dag::platformDetector->platform == dag::PlatformDetector::Platform::Linux);
#elif _WIN32
        REQUIRE(dag::platformDetector->platform == dag::PlatformDetector::Platform::Windows);
#endif
    }

    dag::platformDetector->shutdown();
}
