add_executable(platform_test
    platform_test.cpp
)

target_link_libraries(platform_test PUBLIC 
	Catch2::Catch2
    Dagmar::PlatformDetector
    Dagmar::ModuleLifecycle
)

catch_discover_tests(platform_test)

add_custom_command(TARGET platform_test POST_BUILD
    COMMAND ctest -C $<CONFIGURATION> --output-on-failure
)
