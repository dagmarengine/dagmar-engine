#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <string>
#include <iostream>
#include "dagmar/ModuleLifecycle.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/FileSystem.h"
#include <glm/gtx/string_cast.hpp>


TEST_CASE("Resources Tests")
{
    //dag::ModuleLifecycle::get().startup();

    SECTION("TEST 1")
    {
        dag::resourceManager->startup();
    	
        auto test1 = dag::resourceManager->parseOBJ("../test_assets/duck.obj");
        std::cout << "Read Files" << std::endl;
        dag::resourceManager->writeBin(test1, "../test_assets/duck.dat");
        std::cout << "Read Files" << std::endl;
        auto binTest = dag::resourceManager->readBin("../test_assets/duck.dat");

        for (int s = 0; s < dag::resourceManager->models[test1].shapes.size(); ++s)
        {
            REQUIRE(dag::resourceManager->models[test1].shapes[s].vertices.size() == dag::resourceManager->models[binTest].shapes[s].vertices.size());
            REQUIRE(dag::resourceManager->models[test1].shapes[s].indices.size() == dag::resourceManager->models[binTest].shapes[s].indices.size());
        }

       
        for (int s = 0; s < dag::resourceManager->models[test1].shapes.size(); ++s)
        {
            //Vertex Position check
            for (int j = 0; j < dag::resourceManager->models[test1].shapes[s].vertices.size(); j++)
            {
                REQUIRE(dag::resourceManager->models[test1].shapes[s].vertices[j].pos == dag::resourceManager->models[binTest].shapes[s].vertices[j].pos);

                REQUIRE(dag::resourceManager->models[test1].shapes[s].vertices[j].normal == dag::resourceManager->models[binTest].shapes[s].vertices[j].normal);

                REQUIRE(dag::resourceManager->models[test1].shapes[s].vertices[j].texCoord == dag::resourceManager->models[binTest].shapes[s].vertices[j].texCoord);

                REQUIRE(dag::resourceManager->models[test1].shapes[s].vertices[j].color == dag::resourceManager->models[binTest].shapes[s].vertices[j].color);
            }

            std::cout << "Vertices Done" << std::endl;

            //Shapes
            for (int j = 0; j < dag::resourceManager->models[test1].shapes[s].indices.size(); j++)
            {
                std::cout << "j: " << j << std::endl;

                //ShapeName
                REQUIRE(dag::resourceManager->models[test1].shapes[s].indices[j] == dag::resourceManager->models[binTest].shapes[s].indices[j]);

            }

            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.name == dag::resourceManager->models[binTest].shapes[s].material.name);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.ambience == dag::resourceManager->models[binTest].shapes[s].material.ambience);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.diffuse == dag::resourceManager->models[binTest].shapes[s].material.diffuse);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.specular == dag::resourceManager->models[binTest].shapes[s].material.specular);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.emission == dag::resourceManager->models[binTest].shapes[s].material.emission);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.dissolve == dag::resourceManager->models[binTest].shapes[s].material.dissolve);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.textureAmbientHash == dag::resourceManager->models[binTest].shapes[s].material.textureAmbientHash);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.textureDiffuseHash == dag::resourceManager->models[binTest].shapes[s].material.textureDiffuseHash);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.textureSpecularHash == dag::resourceManager->models[binTest].shapes[s].material.textureSpecularHash);
            REQUIRE(dag::resourceManager->models[test1].shapes[s].material.textureAlphaHash == dag::resourceManager->models[binTest].shapes[s].material.textureAlphaHash);
        }
    }    
}
