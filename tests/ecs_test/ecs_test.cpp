#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <cstdint>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "dagmar/Coordinator.h"

struct A
{
    float x = 0;
};

struct B
{
    float y = 0;
};

TEST_CASE("Coordinator Component Registration", "[component_registration]") {
    
    dag::coordinator->startup();

    dag::coordinator->registerComponent<A>();

    dag::ComponentType compABits = dag::coordinator->getComponentType<A>();

    REQUIRE(compABits == 0);

    dag::coordinator->registerComponent<B>();

    dag::ComponentType compBBits = dag::coordinator->getComponentType<B>();

    REQUIRE(compBBits == 1);

    dag::coordinator->shutdown();
}

class EmptySys : public dag::System
{
    public:
        void foo() {};
};

class FilledSys : public dag::System
{
    public:
        void bar() {};
};

TEST_CASE("Coordinator System Registration", "[system_registration]") {
    
    dag::coordinator->startup();

    dag::coordinator->registerComponent<A>();
    dag::coordinator->registerComponent<B>();

    // First empty system
    dag::coordinator->registerSystem<EmptySys>();
    dag::Signature emptySysSig;

    dag::coordinator->setSystemSignature<EmptySys>(emptySysSig);
    REQUIRE(dag::coordinator->getSystemSignature<EmptySys>() == 0);

    // Second system
    auto fs = dag::coordinator->registerSystem<FilledSys>();
    dag::Signature filledSysSig;

    filledSysSig.set(dag::coordinator->getComponentType<A>());
    dag::coordinator->setSystemSignature<FilledSys>(filledSysSig);
    REQUIRE(dag::coordinator->getSystemSignature<FilledSys>() == 1);
    filledSysSig.set(dag::coordinator->getComponentType<B>());
    dag::coordinator->setSystemSignature<FilledSys>(filledSysSig);
    REQUIRE(dag::coordinator->getSystemSignature<FilledSys>() == 3);

    // second system iteration to check if they are correctly registered
    
    std::vector<dag::Entity> entities(3);
    entities[0] = dag::coordinator->createEntity();
    dag::coordinator->addComponent(entities[0], A{});

    entities[1] = dag::coordinator->createEntity();
    dag::coordinator->addComponent(entities[1], B{});

    entities[2] = dag::coordinator->createEntity();
    dag::coordinator->addComponent(entities[2], A{});
    dag::coordinator->addComponent(entities[2], B{});

    // Only the entities[2] should be added to this system
    REQUIRE(fs->entities.size() == 1);
    REQUIRE(*(fs->entities.begin()) == entities[2]);

    // Removing test
    dag::coordinator->removeComponent<A>(entities[2]);
    REQUIRE(fs->entities.size() == 0);

    // Adding the component to another entity
    dag::coordinator->addComponent(entities[0], B{});
    REQUIRE(fs->entities.size() == 1);
    REQUIRE(*(fs->entities.begin()) == entities[0]);

    // Remove entity
    dag::coordinator->destroyEntity(entities[0]);
    REQUIRE(fs->entities.size() == 0);

    dag::coordinator->shutdown();
}

using Catch::Matchers::Contains;

TEST_CASE("ECS Exceptions: Insert Existing Component", "[insert_component_already_existent]") {
    
    dag::coordinator->startup();
    dag::coordinator->registerComponent<A>();

    auto entity = dag::coordinator->createEntity();
    dag::coordinator->addComponent(entity, A{});
    
    REQUIRE_THROWS_WITH(dag::coordinator->addComponent(entity, A{}), Contains("Insert component failed: Component already added to the entity."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Remove Component Not Existent", "[remove_component_not_existent]") {
    
    dag::coordinator->startup();
    dag::coordinator->registerComponent<A>();

    auto entity = dag::coordinator->createEntity();
    REQUIRE_THROWS_WITH(dag::coordinator->removeComponent<A>(entity), Contains("Remove component failed: Component does not exist."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Not Existent", "[get_component_not_existent]") {
    
    dag::coordinator->startup();
    dag::coordinator->registerComponent<A>();

    auto entity = dag::coordinator->createEntity();

    REQUIRE_THROWS_WITH(dag::coordinator->getComponent<A>(entity), Contains("Get component failed: Component does not exist"));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Register Existing Component", "[register_component_already_existing]") {
    
    dag::coordinator->startup();

    dag::coordinator->registerComponent<A>();
    REQUIRE_THROWS_WITH(dag::coordinator->registerComponent<A>(), Contains("Register component failed: Component already registered."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Type Not Existent", "[get_component_type_not_existent]") {    
    dag::coordinator->startup();

    dag::coordinator->registerComponent<A>();
    auto entity = dag::coordinator->createEntity();
    REQUIRE_THROWS_WITH(dag::coordinator->getComponentType<B>(), Contains("Get component type failed: Component does not exist."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Array Not Existent", "[get_component_array_not_existent]") {
    
    dag::coordinator->startup();

    auto entity = dag::coordinator->createEntity();
    REQUIRE_THROWS_WITH(dag::coordinator->addComponent(entity, A{}), Contains("Get component array failed: Component array does not exist."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Register System Already Existent", "[register_system_already_existent]") {
    
    dag::coordinator->startup();
    dag::coordinator->registerSystem<EmptySys>();
    REQUIRE_THROWS_WITH(dag::coordinator->registerSystem<EmptySys>(), Contains("Register system failed: System already existent."));
    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Set System Signature Not Existent", "[set_system_signature_not_existent]") {
    
    dag::coordinator->startup();
    dag::coordinator->registerComponent<A>();

    dag::Signature sysSig;
    sysSig.set(dag::coordinator->getComponentType<A>());

    REQUIRE_THROWS_WITH(dag::coordinator->setSystemSignature<EmptySys>(sysSig), Contains("Set system signature failed: System does not exist."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Get System Signature Not Existent", "[get_system_signature_not_existent]") {
    dag::coordinator->startup();

    REQUIRE_THROWS_WITH(dag::coordinator->getSystemSignature<EmptySys>(), Contains("Get system signature failed: System does not exist."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Create Too Many Entities", "[create_too_many_entities]") {
    
    dag::coordinator->startup();

    for(size_t i = 0; i < dag::MAX_ENTITIES; i++)
    {
        dag::coordinator->createEntity();
    }

    REQUIRE_THROWS_WITH(dag::coordinator->createEntity(), Contains("Create entity failed: MAX_ENTITIES limit has been reached."));

    dag::coordinator->shutdown();
}

TEST_CASE("ECS Exceptions: Destroy entity out of range", "[destroy_entity_out_of_range]") {
    
    dag::coordinator->startup();

    REQUIRE_THROWS_WITH(dag::coordinator->destroyEntity(dag::MAX_ENTITIES), Contains("Destroy entity failed: Entity out of range."));

    dag::coordinator->shutdown();
}