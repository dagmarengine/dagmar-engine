add_executable(log_test
    log_test.cpp
)

target_link_libraries(log_test PRIVATE 
	Catch2::Catch2
    Dagmar::Log
)

catch_discover_tests(log_test)

add_custom_command(TARGET log_test POST_BUILD
    COMMAND ctest -C $<CONFIGURATION> --output-on-failure
    )
