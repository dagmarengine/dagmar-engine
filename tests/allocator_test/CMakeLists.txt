add_executable(allocator_test
    allocator_test.cpp
)

target_link_libraries(allocator_test PRIVATE 
	Catch2::Catch2
    Dagmar::Allocator
    Dagmar::ModuleLifecycle
)

catch_discover_tests(allocator_test)

add_custom_command(TARGET allocator_test POST_BUILD
    COMMAND ctest -C $<CONFIGURATION> --output-on-failure
    )
