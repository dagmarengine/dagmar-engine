set(DAGMAR_EDITOR_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(DAGMAR_EDITOR_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR})

add_subdirectory(layers)
add_subdirectory(resources)
add_subdirectory(application)

add_executable(dagmar_editor
	main.cpp
)

target_link_libraries(dagmar_editor
	Dagmar::Engine
	Dagmar::Editor::ResourceLoader
	Dagmar::Editor::Layers
	Dagmar::Editor::Application
)

install(TARGETS dagmar_editor)
