#include "dagmar/EditorUILayer.h"

#include <future>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <thread>

#include "ImGuizmo.h"
#include "dagmar/EventDispatcher.h"
#include "dagmar/FileSystem.h"
#include "dagmar/IconsFontAwesome5.h"
#include "dagmar/KeyManager.h"
#include "dagmar/Scene.h"
#include "dagmar/TimeManager.h"
#include "dagmar/UI/Console.h"
#include "dagmar/VoxelChunkManager.h"
#include "dagmar/WindowManager.h"
#include "glm/gtx/matrix_decompose.hpp"
#include "imgui_internal.h"
#include <dagmar/PhysicsSystem.h>
#include <dagmar/PlayerControllerSystem.h>
#include <dagmar/ResourceManager.h>
#include <imfilebrowser.h>

#include "dagmar/Profiler.h"

#include <random>
#include <thread>

#include "dagmar/RendererLayer.h"

dag::EditorUILayer::EditorUILayer() :
    Layer("EditorUILayer")
{
    prePlayState = nlohmann::json{};
    gameState = GameState::Stop;
}

void dag::EditorUILayer::onAttach()
{
    // Initialise imgui helper functions
    IMGUI_CHECKVERSION();
    ImGuiContext* ctx = ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;     // Enable Docking
#ifdef _WIN32
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable; // Enable Multi-Viewport / Platform Windows
#endif

    setEditorColors();
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    ImGui_ImplGlfw_InitForOpenGL(dag::windowManager->window.handle, false);
    ImGui_ImplOpenGL3_Init("#version 450");

    // io.Fonts->AddFontDefault();
    io.Fonts->AddFontFromFileTTF((dag::filesystem::getAssetsPath() / "internal" / "fonts" / "SFPro-Regular.ttf").string().c_str(), 16.0f);

    static const ImWchar icons_ranges[] = {ICON_MIN_FA, ICON_MAX_FA, 0};
    ImFontConfig icons_config;
    icons_config.GlyphOffset.y += 1;
    icons_config.MergeMode = true;
    icons_config.PixelSnapH = true;

    io.Fonts->AddFontFromFileTTF((dag::filesystem::getAssetsPath() / "internal" / "fonts" / FONT_ICON_FILE_NAME_FAS).string().c_str(), 13.0f, &icons_config, icons_ranges);

    renderingSystem = dag::coordinator->getSystem<RenderingSystem>();
    // Setting up the ui manager

    snapGridSizesCheckboxes[currentSnapGridSizeID] = true;
    snapRotationSizesCheckboxes[currentSnapRotationSizeID] = true;
    snapScaleSizesCheckboxes[currentSnapScaleSizeID] = true;

    if (std::filesystem::exists("default_imgui.ini"))
    {
        if (!std::filesystem::exists(ImGui::GetIO().IniFilename))
        {
            std::filesystem::copy("default_imgui.ini", "imgui.ini");
        }
    }

    currentFileManagerPath = dag::filesystem::getAssetsPath();
    currentFileManagerSelectedPath = "";
    refreshCachedDirs();

    renderingSystem->enqueueLateOverlays(RenderingSystem::OverlaysFuncID::Grid);

    auto const& cameraSystem = coordinator->getSystem<CameraSystem>();
    auto const& lightSystem = coordinator->getSystem<LightSystem>();
    cameraSystem->setEditorCamera(coordinator->createEntity());

    scene->createEntity("Light");
    auto& lightTransform = coordinator->getComponent<Transform>(scene->entities.back());
    lightTransform.setPosition(glm::vec3(0, 0, 100));

    coordinator->addComponent(cameraSystem->editorCamera, dag::Transform{});
    coordinator->addComponent(cameraSystem->editorCamera, dag::CameraComponent{});

    auto& cameraComponent = coordinator->getComponent<CameraComponent>(cameraSystem->editorCamera);
    cameraComponent.isPossessed = true;
    cameraComponent.isScreenDependent = true;
    auto& transform = coordinator->getComponent<Transform>(cameraSystem->editorCamera);

    transform.setPosition(glm::vec3(0.0f, 30.0f, 30.0f));
    transform.setRotation(glm::vec3(45.0f, transform.rotation.y, transform.rotation.z));
    cameraComponent.zNear = 1.0f;
    cameraComponent.zFar = 10000.0f;
}

void dag::EditorUILayer::onDetach()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void dag::EditorUILayer::onUpdate(float const& dt)
{
    renderUI();

    ImGuiIO& io = ImGui::GetIO();

    const auto cameraSystem = coordinator->getSystem<CameraSystem>();
    auto& cameraComponent = coordinator->getComponent<CameraComponent>(cameraSystem->activeCamera);
    auto& transform = coordinator->getComponent<Transform>(cameraSystem->activeCamera);

    if (renderWindowFocused && gameState != GameState::Play)
    {
        auto dt = 0.02;

        if (keyManager->isMouseButtonDown(GLFW_MOUSE_BUTTON_RIGHT))
        {
            auto vald = ImGui::GetMouseDragDelta(ImGuiMouseButton_Right);

            auto delta = ImVec2(prevDelta.x - vald.x, prevDelta.y - vald.y);

            float deltaX = delta.x;
            float deltaY = delta.y;

            if (deltaX != 0 || deltaY != 0)
            {
                transform.rotation += glm::vec3(-deltaY * mouseDragSensitivity, -deltaX * mouseDragSensitivity, 0.0f);
                transform.updateModelMatrix();
            }

            prevDelta = vald;

            glm::vec3 forwardVector;
            forwardVector.x = -cos(glm::radians(transform.rotation.x)) * sin(glm::radians(transform.rotation.y));
            forwardVector.y = sin(glm::radians(transform.rotation.x));
            forwardVector.z = cos(glm::radians(transform.rotation.x)) * cos(glm::radians(transform.rotation.y));
            forwardVector = glm::normalize(forwardVector);

            glm::vec3 rightVector = glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), forwardVector));
            glm::vec3 upVector = glm::normalize(glm::cross(rightVector, forwardVector));

            if (keyManager->isKeyDown(GLFW_KEY_W))
            {
                transform.moveForward(-1.0f * dt * cameraComponent.cameraSpeed, forwardVector);
                transform.updateModelMatrix();
            }
            if (keyManager->isKeyDown(GLFW_KEY_S))
            {
                transform.moveForward(1.0f * dt * cameraComponent.cameraSpeed, forwardVector);
                transform.updateModelMatrix();
            }
            if (keyManager->isKeyDown(GLFW_KEY_A))
            {
                transform.moveRight(-1.0f * dt * cameraComponent.cameraSpeed, rightVector);
                transform.updateModelMatrix();
            }
            if (keyManager->isKeyDown(GLFW_KEY_D))
            {
                transform.moveRight(1.0f * dt * cameraComponent.cameraSpeed, rightVector);
                transform.updateModelMatrix();
            }
            if (keyManager->isKeyDown(GLFW_KEY_E))
            {
                transform.moveUp(-1.0f * dt * cameraComponent.cameraSpeed, upVector);
                transform.updateModelMatrix();
            }
            if (keyManager->isKeyDown(GLFW_KEY_Q))
            {
                transform.moveUp(1.0f * dt * cameraComponent.cameraSpeed, upVector);
                transform.updateModelMatrix();
            }
        }
        else
        {
            if (keyManager->isKeyDown(GLFW_KEY_W))
            {
                gizmoType = ImGuizmo::TRANSLATE;
            }
            if (keyManager->isKeyDown(GLFW_KEY_E))
            {
                gizmoType = ImGuizmo::ROTATE;
            }
            if (keyManager->isKeyDown(GLFW_KEY_R))
            {
                gizmoType = ImGuizmo::SCALE;
            }
        }
    }

    if (dag::scene->currentEntity != -1)
    {
        auto const& currentEntity = dag::scene->entities[dag::scene->currentEntity];
        auto& currentTransform = coordinator->getComponent<Transform>(currentEntity);
        ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(currentTransform.modelMatrix), glm::value_ptr(updatedTranslation), glm::value_ptr(updatedRotation), glm::value_ptr(updatedScale));
    }
}

void dag::EditorUILayer::onEvent(Event& event)
{
    EventDispatcher dispatcher(event);

    dispatcher.dispatch<KeyTypedEvent>(BIND_EVENT_FN(EditorUILayer::onKeyTyped));
    dispatcher.dispatch<KeyPressedEvent>(BIND_EVENT_FN(EditorUILayer::onKeyPressed));
    dispatcher.dispatch<KeyReleasedEvent>(BIND_EVENT_FN(EditorUILayer::onKeyReleased));
    dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FN(EditorUILayer::onMouseScrolled));
    dispatcher.dispatch<MouseMovedEvent>(BIND_EVENT_FN(EditorUILayer::onMouseMoved));
    dispatcher.dispatch<MouseButtonPressedEvent>(BIND_EVENT_FN(EditorUILayer::onMouseButtonPressed));
    dispatcher.dispatch<MouseButtonReleasedEvent>(BIND_EVENT_FN(EditorUILayer::onMouseButtonReleased));
    dispatcher.dispatch<WindowResizeEvent>(BIND_EVENT_FN(EditorUILayer::onWindowResize));
}

void dag::EditorUILayer::setEditorColors()
{
    ImVec4* colors = ImGui::GetStyle().Colors;
    colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 1.00f);
    colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
    colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
    colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_FrameBg] = ImVec4(0.20f, 0.20f, 0.20f, 0.54f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.59f, 0.59f, 0.59f, 0.40f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.78f, 0.78f, 0.78f, 0.67f);
    colors[ImGuiCol_TitleBg] = ImVec4(0.28f, 0.28f, 0.28f, 1.00f);
    colors[ImGuiCol_TitleBgActive] = ImVec4(0.12f, 0.12f, 0.12f, 1.00f);
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
    colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
    colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
    colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_CheckMark] = ImVec4(0.75f, 0.75f, 0.75f, 1.00f);
    colors[ImGuiCol_SliderGrab] = ImVec4(0.37f, 0.37f, 0.37f, 1.00f);
    colors[ImGuiCol_SliderGrabActive] = ImVec4(0.43f, 0.43f, 0.43f, 1.00f);
    colors[ImGuiCol_Button] = ImVec4(0.73f, 0.73f, 0.73f, 0.40f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.67f, 0.67f, 0.67f, 1.00f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.77f, 0.77f, 0.77f, 1.00f);
    colors[ImGuiCol_Header] = ImVec4(0.75f, 0.75f, 0.75f, 0.31f);
    colors[ImGuiCol_HeaderHovered] = ImVec4(0.49f, 0.49f, 0.49f, 0.80f);
    colors[ImGuiCol_HeaderActive] = ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
    colors[ImGuiCol_Separator] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
    colors[ImGuiCol_SeparatorHovered] = ImVec4(0.75f, 0.75f, 0.75f, 0.78f);
    colors[ImGuiCol_SeparatorActive] = ImVec4(0.73f, 0.74f, 0.75f, 1.00f);
    colors[ImGuiCol_ResizeGrip] = ImVec4(0.31f, 0.31f, 0.31f, 0.20f);
    colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.39f, 0.39f, 0.39f, 0.67f);
    colors[ImGuiCol_ResizeGripActive] = ImVec4(0.47f, 0.47f, 0.47f, 0.95f);
    colors[ImGuiCol_Tab] = ImVec4(0.20f, 0.20f, 0.20f, 0.86f);
    colors[ImGuiCol_TabHovered] = ImVec4(0.29f, 0.29f, 0.29f, 0.80f);
    colors[ImGuiCol_TabActive] = ImVec4(0.49f, 0.49f, 0.49f, 1.00f);
    colors[ImGuiCol_TabUnfocused] = ImVec4(0.07f, 0.10f, 0.15f, 0.97f);
    colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
    colors[ImGuiCol_DockingPreview] = ImVec4(0.65f, 0.65f, 0.65f, 0.70f);
    colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
    colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
    colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
    colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
    colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
    colors[ImGuiCol_TableHeaderBg] = ImVec4(0.19f, 0.19f, 0.20f, 1.00f);
    colors[ImGuiCol_TableBorderStrong] = ImVec4(0.31f, 0.31f, 0.35f, 1.00f);
    colors[ImGuiCol_TableBorderLight] = ImVec4(0.23f, 0.23f, 0.25f, 1.00f);
    colors[ImGuiCol_TableRowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_TableRowBgAlt] = ImVec4(1.00f, 1.00f, 1.00f, 0.06f);
    colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
    colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
    colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
    colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
    colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
}

bool dag::EditorUILayer::onKeyTyped(KeyTypedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();

    ImGui_ImplGlfw_CharCallback(windowManager->window.handle, event.getKeyCode());

    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onKeyPressed(KeyPressedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();

    ImGui_ImplGlfw_KeyCallback(windowManager->window.handle, event.getKeyCode(), event.getScanCode(), event.getAction(), event.getMods());

    if (gameState != GameState::Play)
    {
        switch (event.getKeyCode())
        {
            case GLFW_KEY_DELETE:
            {
                if (scene->currentEntity >= 0)
                {
                    if (scene->entities[scene->currentEntity] == voxelManager->currVoxelMainEntity)
                    {
                        voxelManager->currVoxelEntity = UINT32_MAX;
                        voxelManager->currVoxelMainEntity = UINT32_MAX;
                    }

                    scene->destroyEntity(dag::scene->entities[scene->currentEntity]);

                    coordinator->getSystem<LightSystem>()->updateLookupVectors();
                }
                break;
            }
        }
    }

    if (event.getKeyCode() == GLFW_KEY_ESCAPE)
    {
        if (gameState == GameState::Play)
        {
            gameState = GameState::Pause;
            coordinator->getSystem<dag::PhysicsSystem>()->gameState = GameState::Pause;
            ImGui::FocusWindow(nullptr);
            ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
            windowManager->enableCursor();

            renderingSystem->enqueueLateOverlaysEntity();
        }
    }

    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onKeyReleased(KeyReleasedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();
    io.MousePos = ImVec2(keyManager->getMousePosition().x, keyManager->getMousePosition().y);

    ImGui_ImplGlfw_KeyCallback(windowManager->window.handle, event.getKeyCode(), event.getScanCode(), event.getAction(), event.getMods());

    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onMouseScrolled(MouseScrolledEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();
    ImGui_ImplGlfw_ScrollCallback(windowManager->window.handle, event.getXOffset(), event.getYOffset());

    if (gameState != GameState::Play)
    {
        auto camera = coordinator->getSystem<CameraSystem>()->activeCamera;
        auto& cameraComponent = coordinator->getComponent<CameraComponent>(camera);
        auto& cameraTransform = coordinator->getComponent<Transform>(camera);

        if (renderWindowHovered)
        {
            if (renderWindowFocused)
            {
                if (keyManager->isMouseButtonDown(GLFW_MOUSE_BUTTON_RIGHT))
                {
                    cameraComponent.cameraSpeed += event.getYOffset() * 0.3f;

                    if (cameraComponent.cameraSpeed < 1.0f)
                    {
                        cameraComponent.cameraSpeed = 1.0f;
                    }
                    else if (cameraComponent.cameraSpeed > 300.0f)
                    {
                        cameraComponent.cameraSpeed = 300.0f;
                    }
                }
                else
                {
                    cameraTransform.moveForward(-event.getYOffset(), cameraTransform.forwardVector);
                }
            }
            else
            {
                cameraTransform.moveForward(-event.getYOffset(), cameraTransform.forwardVector);
            }
        }
    }
    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onMouseMoved(MouseMovedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();
    io.MousePos = ImVec2(event.getX(), event.getY());
    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onMouseButtonPressed(MouseButtonPressedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();

    ImGui_ImplGlfw_MouseButtonCallback(windowManager->window.handle, event.getButton(), GLFW_PRESS, event.getMods());

    if (gameState != GameState::Play)
    {
        if (event.getButton() == ImGuiMouseButton_Right)
        {
            prevDelta = ImVec2(0, 0);

            if (!renderWindowHovered)
            {
                ImGui::FocusWindow(nullptr);
            }
            else
            {
                if (!renderWindowFocused)
                {
                    ImGui::FocusWindow(gameRenderWindow);
                    renderWindowFocused = true;
                }

                windowManager->disableCursor();
            }
        }

        if (event.getButton() == ImGuiMouseButton_Left && gameState != GameState::Play && !isOverGizmo)
        {
            auto mousePos = ImGui::GetMousePos();

            mousePos.x -= viewportPos.x;
            mousePos.y -= viewportPos.y;

            if ((mousePos.x >= 0 && mousePos.x <= viewportSize.x) && (mousePos.y >= 0 && mousePos.y <= viewportSize.y))
            {
                int32_t id = renderingSystem->decodePixel(renderingSystem->encoderFramebuffer.getPixelData(mousePos.x, mousePos.y));

                renderingSystem->dequeueLateOverlaysEntity();

                if (id == 0x00FFFFFF)
                {
                    id = -1;
                    scene->currentEntity = id;
                    scene->currentRenderableEntity = id;
                }
                else
                {
                    scene->currentEntity = dag::scene->getEntityIndex(id);
                    scene->currentRenderableEntity = renderingSystem->getEntityOffset(scene->entities[scene->currentEntity]);

                    renderingSystem->enqueueLateOverlaysEntity();
                }
            }
        }
    }
    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onMouseButtonReleased(MouseButtonReleasedEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();
    io.MousePos = ImVec2(dag::keyManager->getMousePosition().x, dag::keyManager->getMousePosition().y);

    ImGui_ImplGlfw_MouseButtonCallback(windowManager->window.handle, event.getButton(), GLFW_RELEASE, event.getMods());

    if (gameState != GameState::Play)
    {
        windowManager->enableCursor();
    }

    prevDelta = ImVec2(0, 0);

    return gameState != GameState::Play;
}

bool dag::EditorUILayer::onWindowResize(WindowResizeEvent& event)
{
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize = ImVec2(event.getWidth(), event.getHeight());
    io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);

    renderingSystem->setSizeDirty(viewportSize.x, viewportSize.y);
    return false;
}

void dag::EditorUILayer::drawTopBar()
{
    // Rendering the menu bar
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("New Scene", "CTRL+N"))
            {
            }

            if (ImGui::MenuItem("Open", "CTRL+O"))
            {
                fileDialog = ImGui::FileBrowser(ImGuiFileBrowserFlags_SelectDirectory);
                fileDialog.SetTitle("Open scenegraph");
                fileDialog.SetTypeFilters({""});
                fileDialog.Open();
                currDialogState = dialogState::LOADING_SCENE;
            }

            if (ImGui::MenuItem("Export Game", "CTRL+E") && !compilerReturnCode.valid())
            {
                Log::info("Starting resource serialization...");
                dag::resourceManager->serialize(dag::filesystem::getExePath().parent_path() / "../game_output/assets");
                Log::info("Starting scene serialization...");
                dag::scene->serialize("../build_site/application/output_scene_data.h");
                Log::info("Starting compilation...");
#ifdef _WIN32
                compilerReturnCode = std::async(std::launch::async, std::system, "..\\build_site\\build\\dagmar_compiler.exe");
#else
                compilerReturnCode = std::async(std::launch::async, std::system, "../build_site/build/dagmar_compiler");
#endif
            }

            if (ImGui::MenuItem("Save", "CTRL+S"))
            {
                if (currOpenedSceneFile == "")
                {
                    fileDialog = ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
                    fileDialog.SetTitle("Save scenegraph");
                    fileDialog.SetTypeFilters({""});
                    fileDialog.Open();
                    currDialogState = dialogState::SAVING_SCENE;
                }
                else
                {
                    scene->serialize(currOpenedSceneFile);
                }
            }

            if (ImGui::MenuItem("Save As", "CTRL+SHIFT+S"))
            {
                fileDialog = ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
                fileDialog.SetTitle("Save scenegraph");
                fileDialog.SetTypeFilters({""});
                fileDialog.Open();
                currDialogState = dialogState::SAVING_SCENE;
            }

            ImGui::EndMenu();
        }

        if (fileDialog.HasSelected())
        {
            if (currDialogState == dialogState::SAVING_SCENE)
            {
                auto save_path = fileDialog.GetSelected().replace_extension("");

                if (!std::filesystem::exists(save_path))
                {
                    std::filesystem::create_directories(save_path);
                }

                Log::debug("Saving Scene to: ", save_path);

                dag::resourceManager->serialize(save_path);
                scene->serialize(save_path / "scene_data.json");
                currOpenedSceneFile = save_path;

                fileDialog.ClearSelected();
                currDialogState = dialogState::INACTIVE;
            }
            else if (currDialogState == dialogState::LOADING_SCENE)
            {
                Log::debug("Loading Scene from: ", fileDialog.GetSelected());

                for (auto& e : scene->entities)
                {
                    scene->coordinator->destroyEntity(e);
                }
                scene->entities.clear();

                scene->deserialize(fileDialog.GetSelected() / "scene_data.json");
                currOpenedSceneFile = fileDialog.GetSelected();

                fileDialog.ClearSelected();
                currDialogState = dialogState::INACTIVE;
            }
        }

        if (ImGui::BeginMenu("Edit"))
        {
            if (ImGui::MenuItem("Undo", "CTRL+Z", false, false))
            {
            }

            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false))
            {
            }

            ImGui::Separator();

            if (ImGui::MenuItem("Cut", "CTRL+X"))
            {
            }

            if (ImGui::MenuItem("Copy", "CTRL+C"))
            {
            }

            if (ImGui::MenuItem("Paste", "CTRL+V"))
            {
            }

            ImGui::Separator();

            if (ImGui::MenuItem("Duplicate", "CTRL+D"))
            {
            }

            if (ImGui::MenuItem("Rename", "F2"))
            {
            }

            if (ImGui::MenuItem("Delete", "DEL"))
            {
            }

            ImGui::Separator();

            if (ImGui::MenuItem("Play", "CTRL+P"))
            {
            }

            if (ImGui::MenuItem("Pause", "F2", false, false))
            {
            }

            if (ImGui::MenuItem("Reset"))
            {
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Debug"))
        {

            drawDebugMenu();

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Help"))
        {
            ImGui::Text("This is Dagmar Engine.");

            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    if (compilerReturnCode.valid())
    {
        if (compilerReturnCode.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
        {
            if (compilerReturnCode.get() == 0)
            {
                Log::success("**************");
                Log::success("*  SUCCESS!  *");
                Log::success("**************");
            }
            else
            {
                Log::error("*************");
                Log::error("*  FAILED!  *");
                Log::error("*************");
                Log::info("Please view external log.");
            }
            compilerReturnCode = std::future<int>();
        }
    }
}

void dag::EditorUILayer::drawDebugMenu()
{
    ImGui::Text("Debug menu");
    ImGui::Separator();

    if (ImGui::Checkbox("Show Grid " ICON_FA_BORDER_ALL, &shouldRenderGrid))
    {
        if (shouldRenderGrid)
        {
            renderingSystem->enqueueLateOverlays(RenderingSystem::OverlaysFuncID::Grid);
        }
        else
        {
            renderingSystem->dequeueLateOverlays(RenderingSystem::OverlaysFuncID::Grid);
        }
    }
    ImGui::Separator();

    ImGui::Checkbox("Show FPS " ICON_FA_HOURGLASS, &shouldRenderFPS);
    ImGui::Separator();

    ImGui::DragFloat("Mouse Drag Sensitivity", &mouseDragSensitivity, 0.005, 0.05, 1.0f);
    ImGui::Separator();

    static std::vector<std::string> debugModes = {"None", "Wireframe", "Unlit", "Depth", "Normals"};
    static int32_t currentDebugMode = 0;

    if (ImGui::BeginCombo("Debug View: ", debugModes[currentDebugMode].c_str()))
    {
        for (int32_t i = 0; i < debugModes.size(); i++)
        {
            if (ImGui::Selectable(debugModes[i].c_str(), i == currentDebugMode))
            {
                currentDebugMode = i;

                renderingSystem->setDebugMode(static_cast<RenderingSystem::DebugRenderMode>(currentDebugMode));
            }
        }

        ImGui::EndCombo();
    }

    ImGui::Separator();

    if (ImGui::Checkbox("Show Colliders " ICON_FA_CIRCLE, &shouldRenderColliderDebugs))
    {
        if (shouldRenderColliderDebugs)
        {
            renderingSystem->enqueueLateOverlays(RenderingSystem::OverlaysFuncID::DebugColliders);
        }
        else
        {
            renderingSystem->dequeueLateOverlays(RenderingSystem::OverlaysFuncID::DebugColliders);
        }
    }
}

/**
 * @brief Draws the file manager
 *
 */
void dag::EditorUILayer::drawFileManager()
{
    ImGui::Begin("File Manager");

    static int item_current_idx_l = 0;
    static int item_current_idx_r = 0;

    ImGui::Columns(2, "FileManager");

    if (ImGui::BeginListBox("##Left", ImVec2(-FLT_MIN, -1)))
    {
        int n = 0;

        if (!std::filesystem::equivalent(currentFileManagerPath, dag::filesystem::getAssetsPath()))
        {
            const bool is_selected = (item_current_idx_l == n);

            if (ImGui::Selectable(std::filesystem::path("..").string().c_str(), is_selected))
                item_current_idx_l = n;

            if (is_selected)
                ImGui::SetItemDefaultFocus();
            ++n;

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
            {
                currentFileManagerPath = currentFileManagerPath.parent_path();
                Log::debug(currentFileManagerPath.string());
                refreshCachedDirs();
            }
            ++n;
        }

        for (auto& p : cached_left_dirs)
        {
            const bool is_selected = (item_current_idx_l == n);

            std::filesystem::path path = std::filesystem::relative(p, currentFileManagerPath).make_preferred();
            std::string path_string = path.generic_string();

            std::string title = "";

            if (p.is_directory())
            {
                path_string += "/";

                title += std::string(ICON_FA_FOLDER) + " ";
            }

            title += path_string;

            if (ImGui::Selectable(title.c_str(), is_selected))
                item_current_idx_l = n;

            if (is_selected)
                ImGui::SetItemDefaultFocus();

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
            {
                currentFileManagerPath /= path;
                Log::debug(currentFileManagerPath.string());
                refreshCachedDirs();
            }

            ++n;
        }

        ImGui::EndListBox();
    }

    ImGui::NextColumn();
    if (ImGui::BeginListBox("##Right", ImVec2(-FLT_MIN, -1)))
    {
        int n = 0;

        if (!std::filesystem::equivalent(currentFileManagerPath, dag::filesystem::getAssetsPath()))
        {
            const bool is_selected = (item_current_idx_r == n);

            /*if (ImGui::Selectable(std::filesystem::path("..").string().c_str(), is_selected))
                item_current_idx = n;*/

            if (is_selected)
                ImGui::SetItemDefaultFocus();
            ++n;

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
            {
                currentFileManagerPath = currentFileManagerPath.parent_path();
                Log::debug(currentFileManagerPath.string());
                refreshCachedDirs();
            }
            ++n;
        }

        for (auto& p : cached_right_dirs)
        {
            const bool is_selected = (item_current_idx_r == n);

            std::filesystem::path path = std::filesystem::relative(p, currentFileManagerPath).make_preferred();
            std::string path_string = path.generic_string();

            if (ImGui::Selectable(path_string.c_str(), is_selected))
                item_current_idx_r = n;

            if (is_selected)
                ImGui::SetItemDefaultFocus();

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
            {
                std::string name = std::filesystem::relative(p.path(), dag::filesystem::getAssetsPath()).string();
                dag::scene->createEntity(name);

                auto lastEntity = dag::scene->entities.back();

                auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();

                size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(p.path(), dag::filesystem::getAssetsPath()).string());
                if (!resourceManager->models.contains(inputHash))
                {

                    size_t meshHash = dag::resourceManager->parseOBJ(p.path());

                    std::cout << name << std::endl;

                    for (auto& shape : dag::resourceManager->models[meshHash].shapes)
                    {
                        std::cout << "shape: " << std::endl;
                        std::cout << shape.indices.size() << std::endl;
                        std::cout << shape.vertices.size() << std::endl;
                        std::cout << shape.material.name << std::endl;
                        rs->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
                        shape.VAOIndex = rs->vaos.size() - 1;
                    }

                    MeshRenderer meshRenderer;
                    meshRenderer.name = name;
                    meshRenderer.vaoID = rs->vaos.size() - 1;
                    meshRenderer.hashIndex = meshHash;

                    resourceManager->meshRenderers.insert({meshHash, meshRenderer});
                }

                coordinator->addComponent(lastEntity, resourceManager->meshRenderers[inputHash]);
                coordinator->addComponent(lastEntity, MaterialComponent{});

                renderingSystem->categorizeGeometry();
                refreshCachedDirs();
            }

            // Drag Source to Game Renderer and Scene Graph
            if (ImGui::BeginDragDropSource())
            {
                dragPayload = p.path();
                ImGui::SetDragDropPayload("DRAG_OBJECTS", &dragPayload, dragPayload.string().size());
                ImGui::Text("Drag");
                ImGui::EndDragDropSource();
            }

            ++n;
        }

        ImGui::EndListBox();
    }

    ImGui::End();
}

void dag::EditorUILayer::drawCameraController()
{
    bool button = ImGui::Button(ICON_FA_VIDEO);

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Editor camera settings");
    }

    if (button)
    {
        ImGui::OpenPopup("cameraControllerPopup");
    }

    if (ImGui::BeginPopup("cameraControllerPopup"))
    {
        auto camera = coordinator->getSystem<CameraSystem>()->activeCamera;
        auto& cameraComponent = coordinator->getComponent<CameraComponent>(camera);
        ImGui::DragFloat("Speed", &cameraComponent.cameraSpeed, 0.1, 1.0f, 300.0f);
        ImGui::EndPopup();
    }
}

/**
 * @brief Draws the window containing our Scene
 *
 */
void dag::EditorUILayer::drawOpenGLRenderWindow()
{
    bool test = false;

    ImGui::Begin("Game Window", &test, ImGuiWindowFlags_MenuBar);

    drawGameStateButtons();

    ImGui::BeginChild("GameRender");

    gameRenderWindow = ImGui::GetCurrentWindow();

    auto wsize = ImGui::GetWindowSize();

    renderWindowFocused = ImGui::IsWindowFocused();
    renderWindowHovered = ImGui::IsWindowHovered();

    ImGui::Image(reinterpret_cast<ImTextureID>(renderingSystem->compositeFramebuffer.getColorAttachment(0)->id), wsize, ImVec2(0, 1), ImVec2(1, 0));

    viewportPos = ImGui::GetWindowPos();

    if (wsize.x != viewportSize.x || wsize.y != viewportSize.y)
    {
        viewportSize = wsize;
        renderingSystem->setSizeDirty(viewportSize.x, viewportSize.y);
    }

    // Drop Target from File Manager
    if (ImGui::BeginDragDropTarget())
    {
        if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DRAG_OBJECTS"))
        {
            std::string path_string = dragPayload.generic_string();

            std::string name = std::filesystem::relative(dragPayload, dag::filesystem::getAssetsPath()).string();

            dag::scene->createEntity(name);

            auto lastEntity = dag::scene->entities.back();

            auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();

            size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(dragPayload, dag::filesystem::getAssetsPath()).string());

            if (!resourceManager->models.contains(inputHash))
            {

                size_t meshHash = dag::resourceManager->parseOBJ(dragPayload);

                std::cout << name << std::endl;

                for (auto& shape : dag::resourceManager->models[meshHash].shapes)
                {
                    std::cout << "shape: " << std::endl;
                    std::cout << shape.indices.size() << std::endl;
                    std::cout << shape.vertices.size() << std::endl;
                    std::cout << shape.material.name << std::endl;
                    rs->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
                    shape.VAOIndex = rs->vaos.size() - 1;
                }

                MeshRenderer meshRenderer;
                meshRenderer.name = name;
                meshRenderer.vaoID = rs->vaos.size() - 1;
                meshRenderer.hashIndex = meshHash;

                resourceManager->meshRenderers.insert({meshHash, meshRenderer});
            }
            coordinator->addComponent(lastEntity, resourceManager->meshRenderers[inputHash]);
            coordinator->addComponent(lastEntity, MaterialComponent{});

            renderingSystem->categorizeGeometry();
        }
        ImGui::EndDragDropTarget();
    }

    drawWindowOverlay();

    ImGuizmo::SetOrthographic(false);
    ImGuizmo::SetDrawlist();
    ImGuizmo::SetRect(viewportPos.x, viewportPos.y, viewportSize.x, viewportSize.y);

    auto& camera = coordinator->getComponent<CameraComponent>(coordinator->getSystem<CameraSystem>()->activeCamera);
    auto& transform = coordinator->getComponent<Transform>(coordinator->getSystem<CameraSystem>()->activeCamera);

    auto cameraProjection = camera.getProjection();
    auto cameraView = camera.getView(transform.position, transform.rotation);
    auto identity = glm::mat4(1.0f);

    glm::vec3 snapValue;

    switch (gizmoType)
    {
        case ImGuizmo::TRANSLATE:
        {
            snapValue = shouldSnapGrid ? glm::vec3(snapGridSizes[currentSnapGridSizeID]) : glm::vec3(0.0f);
            break;
        }
        case ImGuizmo::ROTATE:
        {
            snapValue = shouldSnapRotation ? glm::vec3(snapRotationSizes[currentSnapRotationSizeID]) : glm::vec3(0.0f);
            break;
        }
        case ImGuizmo::SCALE:
        {
            snapValue = shouldSnapScale ? glm::vec3(snapScaleSizes[currentSnapScaleSizeID]) : glm::vec3(0.0f);
            break;
        }
        default:
        {
            break;
        }
    }

    if (gameState != GameState::Play)
    {
        int currentEntityID = dag::scene->currentEntity;

        if (currentEntityID >= 0 && gizmoType != -1)
        {
            Entity selectedEntity = dag::scene->entities[dag::scene->currentEntity];
            {
                auto& entityTransformComponent = coordinator->getComponent<dag::Transform>(selectedEntity);

                auto entityTransform = entityTransformComponent.modelMatrix;

                ImGuizmo::Manipulate(glm::value_ptr(cameraView), glm::value_ptr(cameraProjection), static_cast<ImGuizmo::OPERATION>(gizmoType), ImGuizmo::MODE::LOCAL, glm::value_ptr(entityTransform), nullptr, glm::value_ptr(snapValue));

                isOverGizmo = ImGuizmo::IsOver();

                if (ImGuizmo::IsUsing())
                {
                    entityTransformComponent.modelMatrix = entityTransform;
                    ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(entityTransform), glm::value_ptr(entityTransformComponent.position), glm::value_ptr(entityTransformComponent.rotation), glm::value_ptr(entityTransformComponent.scale));
                }
            }
        }
    }

    ImGui::EndChild();
    ImGui::End();
}
/**
 * @brief Draws the inspector for selected entity
 *
 */
void dag::EditorUILayer::drawInspector()
{
    ImGui::Begin("Inspector");
    if (dag::scene->currentEntity >= dag::scene->entities.size())
    {
        ImGui::End();
        return;
    }

    auto entity = scene->entities[scene->currentEntity];
    auto& nameComponent = coordinator->getComponent<Name>(entity);

    std::vector<std::string> availableComponents;

    char field[35];
    std::strcpy(field, nameComponent.name.c_str());

    if (ImGui::InputText("Name", field, 35))
    {
        nameComponent.name = std::string(field);
        dag::scene->checkName(nameComponent.name);
        dag::scene->recomputeSceneGraph();
    }

    if (ImGui::Button("Copy"))
    {
        auto cameraSystem = coordinator->getSystem<CameraSystem>();

        auto previousEntity = dag::scene->currentEntity;

        auto nameComponent = coordinator->getComponent<Name>(scene->entities[previousEntity]);

        std::string name = nameComponent.name;
        auto tabIndex = name.find("_");
        if (tabIndex != std::string::npos)
        {
            name = name.erase(tabIndex + 1);
        }
    	
        scene->createEntity(name);
        auto& new_entity = scene->entities[dag::scene->currentEntity];

        if (coordinator->hasComponent<Transform>(entity))
        {
            if (coordinator->hasComponent<Transform>(new_entity))
            {
                coordinator->getComponent<Transform>(new_entity) = coordinator->getComponent<Transform>(entity);
            }
            else
            {
                coordinator->addComponent<Transform>(new_entity, coordinator->getComponent<Transform>(entity));
            }
        }
        if (coordinator->hasComponent<RigidBody>(entity))
        {
            if (coordinator->hasComponent<RigidBody>(new_entity))
            {
                coordinator->getComponent<RigidBody>(new_entity) = coordinator->getComponent<RigidBody>(entity);
            }
            else
            {
                dag::scene->addComponent<RigidBody>(new_entity, coordinator->getComponent<RigidBody>(entity));
            }
        }
        if (coordinator->hasComponent<CollisionSphere>(entity))
        {
            if (coordinator->hasComponent<CollisionSphere>(new_entity))
            {
                coordinator->getComponent<CollisionSphere>(new_entity) = coordinator->getComponent<CollisionSphere>(entity);
            }
            else
            {
                coordinator->addComponent<CollisionSphere>(new_entity, coordinator->getComponent<CollisionSphere>(entity));
            }
        }
        if (coordinator->hasComponent<CollisionAABB>(entity))
        {
            if (coordinator->hasComponent<CollisionAABB>(new_entity))
            {
                coordinator->getComponent<CollisionAABB>(new_entity) = coordinator->getComponent<CollisionAABB>(entity);
            }
            else
            {
                coordinator->addComponent<CollisionAABB>(new_entity, coordinator->getComponent<CollisionAABB>(entity));
            }
        }
        if (coordinator->hasComponent<MeshRenderer>(entity))
        {
            if (coordinator->hasComponent<MeshRenderer>(new_entity))
            {
                coordinator->getComponent<MeshRenderer>(new_entity) = coordinator->getComponent<MeshRenderer>(entity);
            }
            else
            {
                coordinator->addComponent<MeshRenderer>(new_entity, coordinator->getComponent<MeshRenderer>(entity));
            }
        }
        if (coordinator->hasComponent<CameraComponent>(entity))
        {
            if (coordinator->hasComponent<CameraComponent>(new_entity))
            {
                coordinator->getComponent<CameraComponent>(new_entity) = coordinator->getComponent<CameraComponent>(entity);
            }
            else
            {
                coordinator->addComponent<CameraComponent>(new_entity, coordinator->getComponent<CameraComponent>(entity));
            }
        }
        if (coordinator->hasComponent<LightComponent>(entity))
        {
            if (coordinator->hasComponent<LightComponent>(new_entity))
            {
                coordinator->getComponent<LightComponent>(new_entity) = coordinator->getComponent<LightComponent>(entity);
            }
            else
            {
                coordinator->addComponent<LightComponent>(new_entity, coordinator->getComponent<LightComponent>(entity));
            }
        }
        if (coordinator->hasComponent<ScriptComponent>(entity))
        {
            if (coordinator->hasComponent<ScriptComponent>(new_entity))
            {
                coordinator->getComponent<ScriptComponent>(new_entity) = coordinator->getComponent<ScriptComponent>(entity);
            }
            else
            {
                coordinator->addComponent<ScriptComponent>(new_entity, coordinator->getComponent<ScriptComponent>(entity));
            }
        }
        if (coordinator->hasComponent<MaterialComponent>(entity))
        {
            if (coordinator->hasComponent<MaterialComponent>(new_entity))
            {
                coordinator->getComponent<MaterialComponent>(new_entity) = coordinator->getComponent<MaterialComponent>(entity);
            }
            else
            {
                coordinator->addComponent<MaterialComponent>(new_entity, coordinator->getComponent<MaterialComponent>(entity));
            }
        }
        if (coordinator->hasComponent<PlayerController>(entity))
        {
            if (coordinator->hasComponent<PlayerController>(new_entity))
            {
                coordinator->getComponent<PlayerController>(new_entity) = coordinator->getComponent<PlayerController>(entity);
            }
            else
            {
                coordinator->addComponent<PlayerController>(new_entity, coordinator->getComponent<PlayerController>(entity));
            }
        }

        //scene->currentEntity = previousEntity;

        dag::scene->recomputeSceneGraph();
    }

    if (coordinator->hasComponent<Transform>(entity))
    {
        auto& transform = coordinator->getComponent<Transform>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_ARROWS_ALT "\tTransform"))
        {
            ImGuiIO& io = ImGui::GetIO();
            bool shouldUpdate = false;

            updatedTranslation = transform.position;
            updatedRotation = transform.rotation;
            updatedScale = transform.scale;

            if (ImGui::DragFloat3("Position", glm::value_ptr(updatedTranslation), 0.1f))
            {
                shouldUpdate = true;
                transform.position = updatedTranslation;
            }

            if (ImGui::DragFloat3("Rotation", glm::value_ptr(updatedRotation), 0.1f))
            {
                shouldUpdate = true;
                transform.rotation = updatedRotation;
            }

            if (ImGui::DragFloat3("Scale", glm::value_ptr(updatedScale), 0.1f))
            {

                shouldUpdate = true;
                transform.scale = updatedScale;
            }

            if (shouldUpdate)
            {
                ImGuizmo::RecomposeMatrixFromComponents(glm::value_ptr(updatedTranslation), glm::value_ptr(updatedRotation), glm::value_ptr(updatedScale), glm::value_ptr(transform.modelMatrix));
            }
            ImGui::Separator();
        }
    }

    if (coordinator->hasComponent<RigidBody>(entity))
    {
        bool isOpened = true;
        auto& rigidBody = coordinator->getComponent<RigidBody>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_COMPRESS_ARROWS_ALT "\tRigid body", &isOpened))
        {
            if (ImGui::Checkbox("Movable", &rigidBody.movable))
            {
                rigidBody.acceleration = glm::vec3(0.0);
                rigidBody.netForce = glm::vec3(0.0);
                rigidBody.externalForce = glm::vec3(0.0);
                rigidBody.velocity = glm::vec3(0.0);
            }

            ImGui::DragFloat("Mass", &rigidBody.mass, 0.01f);
            ImGui::DragFloat("Plasticity", &rigidBody.plasticCoefficient, 0.01f);
            ImGui::DragFloat3("Impulse", glm::value_ptr(rigidBody.impulse), 0.01f);

            if (ImGui::Button("Random Impulse xz"))
            {
                std::random_device rd;
                std::mt19937::result_type seed = rd() ^ ((std::mt19937::result_type)
                                                             std::chrono::duration_cast<std::chrono::seconds>(
                                                                 std::chrono::system_clock::now().time_since_epoch())
                                                                 .count() +
                                                         (std::mt19937::result_type)
                                                             std::chrono::duration_cast<std::chrono::microseconds>(
                                                                 std::chrono::high_resolution_clock::now().time_since_epoch())
                                                                 .count());
                std::mt19937 gen(seed);
                std::uniform_real_distribution<float> distrib(-5.0, 5.0);
                rigidBody.impulse = glm::vec3(distrib(gen), 0.0, distrib(gen));
            }

            if (ImGui::Button("Clear Impulse"))
            {
                rigidBody.impulse = glm::vec3(0.0, 0.0, 0.0);
            }
            ImGui::Separator();
        }

        if (!isOpened)
        {
            dag::scene->removeComponent<RigidBody>(entity);
            if (coordinator->hasComponent<CollisionSphere>(entity))
            {
                dag::scene->removeComponent<CollisionSphere>(entity);
            }
            if (coordinator->hasComponent<CollisionAABB>(entity))
            {
                dag::scene->removeComponent<CollisionAABB>(entity);
            }
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_COMPRESS_ARROWS_ALT "\tRigid body");
    }

    if (coordinator->hasComponent<CollisionSphere>(entity))
    {
        bool isOpened = true;
        auto& collision = coordinator->getComponent<CollisionSphere>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_GLOBE "\tCollision Sphere", &isOpened))
        {
            ImGui::DragFloat("Radius of collision sphere", &collision.radius, 0.01f);
            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<CollisionSphere>(entity);
        }
    }
    else if (coordinator->hasComponent<RigidBody>(entity))
    {
        availableComponents.push_back(ICON_FA_GLOBE "\tCollision Sphere");
    }

    if (coordinator->hasComponent<CollisionAABB>(entity))
    {
        bool isOpened = true;
        auto& collision = coordinator->getComponent<CollisionAABB>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_CUBE "\tCollision AABB", &isOpened))
        {
            ImGui::DragFloat("X size of collision AABB", &collision.width, 0.01f);
            ImGui::DragFloat("Y size of collision AABB", &collision.height, 0.01f);
            ImGui::DragFloat("Z size of collision AABB", &collision.depth, 0.01f);
            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<CollisionAABB>(entity);
        }
    }
    else if (coordinator->hasComponent<RigidBody>(entity))
    {
        availableComponents.push_back(ICON_FA_CUBE "\tCollision AABB");
    }

    if (coordinator->hasComponent<dag::MeshRenderer>(entity))
    {
        bool isOpened = true;
        auto& meshRenderer = coordinator->getComponent<MeshRenderer>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_SHAPES "\tMesh renderer", &isOpened))
        {
            auto renderingSystem = coordinator->getSystem<dag::RenderingSystem>();

            const char* currentMeshLabel = meshRenderer.name.c_str(); // Label to preview before opening the combo (technically it could be anything)
            if (ImGui::BeginCombo("Mesh", currentMeshLabel))
            {
                for (auto it = resourceManager->meshRenderers.begin(); it != resourceManager->meshRenderers.end(); ++it)
                {
                    if (ImGui::Selectable(it->second.name.c_str()))
                    {
                        meshRenderer.vaoID = it->second.vaoID;
                        meshRenderer.name = it->second.name;
                        meshRenderer.hashIndex = it->second.hashIndex;
                        currentMeshLabel = it->second.name.c_str();
                    }
                }
                ImGui::EndCombo();
            }
            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<dag::MeshRenderer>(entity);
            dag::scene->removeComponent<dag::MaterialComponent>(entity);
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_SHAPES "\tMesh renderer");
    }

    if (coordinator->hasComponent<CameraComponent>(entity))
    {
        bool isOpened = true;
        auto& cameraComponent = coordinator->getComponent<CameraComponent>(entity);
        auto& transform = coordinator->getComponent<Transform>(entity);

        if (ImGui::CollapsingHeader(ICON_FA_VIDEO "\tCamera component", &isOpened))
        {
            if (ImGui::Checkbox("Possess", &cameraComponent.isPossessed))
            {
                renderingSystem->cameraSystem->setPossessed(entity, cameraComponent.isPossessed);

                renderingSystem->dequeueLateOverlaysEntity();
                renderingSystem->enqueueLateOverlaysEntity();
            }
            if (ImGui::Checkbox("Screen dependent", &cameraComponent.isScreenDependent))
            {
                renderingSystem->sizeDirty = true;
            }

            ImGui::Checkbox("Third Person", &cameraComponent.thirdPerson);

            if (cameraComponent.thirdPerson)
            {
                ImGui::DragFloat3("Offset", glm::value_ptr(cameraComponent.offset));

                renderingSystem->dequeueLateOverlaysEntity();
                renderingSystem->enqueueLateOverlaysEntity();
            }

            if (!cameraComponent.isScreenDependent)
            {
                ImGui::DragFloat("Field of view", &cameraComponent.fov, 0.01f);
                ImGui::DragFloat("Aspect", &cameraComponent.aspect, 0.01f);
            }
            ImGui::DragFloat("Near plane", &cameraComponent.zNear, 0.01f);
            ImGui::DragFloat("Far plane", &cameraComponent.zFar, 0.01f);
            ImGui::Separator();
        }
        if (!isOpened)
        {
            if (cameraComponent.isPossessed)
            {
                cameraComponent.isPossessed = false;
                renderingSystem->cameraSystem->setPossessed(entity, cameraComponent.isPossessed);

                renderingSystem->dequeueLateOverlaysEntity();
                renderingSystem->enqueueLateOverlaysEntity();
            }
            scene->removeComponent<CameraComponent>(entity);
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_VIDEO "\tCamera component");
    }

    if (coordinator->hasComponent<LightComponent>(entity))
    {
        bool isOpened = true;
        auto& lightComponent = coordinator->getComponent<LightComponent>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_LIGHTBULB "\tLight component", &isOpened))
        {
            auto lightSystem = coordinator->getSystem<LightSystem>();

            ImGui::ColorEdit3("Color", glm::value_ptr(lightComponent.color));

            std::array<const char*, 3> lightTypes = {"Directional", "Pointlight", "Spot"};

            if (ImGui::BeginCombo("Light Type", lightTypes[lightComponent.type]))
            {
                for (size_t i = 0; i < lightTypes.size(); i++)
                {
                    if (ImGui::Selectable(lightTypes[i], lightComponent.type == i))
                    {
                        lightComponent.type = static_cast<LightComponent::Type>(i);
                        lightSystem->updateLookupVectors();

                        renderingSystem->dequeueLateOverlaysEntity();
                        renderingSystem->enqueueLateOverlaysEntity();
                    }
                }

                ImGui::EndCombo();
            }

            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<LightComponent>(entity);
            coordinator->getSystem<LightSystem>()->updateLookupVectors();
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_LIGHTBULB "\tLight component");
    }

    if (coordinator->hasComponent<ScriptComponent>(entity))
    {
        bool isOpened = true;
        auto& scriptComponent = coordinator->getComponent<ScriptComponent>(entity);
        auto& transform = coordinator->getComponent<Transform>(entity);

        if (ImGui::CollapsingHeader(ICON_FA_FILE_CODE "\tScript component", &isOpened))
        {
            ImGui::Checkbox("Create Script", &scriptComponent.checkBox);
            if (scriptComponent.scriptCreated)
            {
                ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.5f, 0.6f), "Script Created.");

                ImGui::PushID(3);
                ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(3 / 7.0f, 0.6f, 0.6f));
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(3 / 7.0f, 0.7f, 0.7f));
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(3 / 7.0f, 0.8f, 0.8f));

                //Open External Application: Keeping Notepad as default for now.
                if (ImGui::Button("Open Script"))
                {
#ifdef _WIN32
                    std::system((scriptPath.string()).c_str());
#else
                    std::system(("xdg-open " + scriptPath.string()).c_str());
#endif
                }
                ImGui::PopStyleColor(3);
                ImGui::PopID();
            }

            else if (scriptComponent.checkBox)
            {
                ImGui::InputText(": Enter file name", scriptFileName, IM_ARRAYSIZE(scriptFileName));
                if (ImGui::Button("Create"))
                {
                    std::string tempScriptFileName = scriptFileName;
                    for (int i = 0; i < allScriptNames.size(); i++)
                    {
                        if (tempScriptFileName == allScriptNames[i])
                        {
                            tempScriptFileName = tempScriptFileName.append("1");
                            break;
                        }
                    }
                    allScriptNames.push_back(tempScriptFileName);

                    //Run in (Install) No-Tests config
                    scriptPath = dag::filesystem::getAssetsPath();
                    scriptPath = scriptPath.append(tempScriptFileName);
                    scriptPath = scriptPath.replace_extension(".cpp");

                    std::filesystem::path tempfilename = tempScriptFileName;
                    scriptComponent.scriptName = tempfilename.replace_extension().string();

                    //File Stencil
                    std::string line;
                    std::ifstream stencilFile;
                    stencilFile.open(stencilPath);
                    std::ofstream scriptFile(scriptPath);

                    //According to stencil format
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << " ";
                    scriptFile << scriptComponent.scriptName;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;
                    std::getline(stencilFile, line);
                    scriptFile << line << std::endl;

                    scriptFile.close();
                    stencilFile.close();

                    scriptFileName[0] = '\0';
                    scriptComponent.scriptCreated = true;
                    Log::info(tempfilename.replace_extension().string() + " script created.");
                }
            }
            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<ScriptComponent>(entity);
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_FILE_CODE "\tScript component");
    }

    if (coordinator->hasComponent<MaterialComponent>(entity))
    {
        auto& materialComponent = coordinator->getComponent<MaterialComponent>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_PALETTE "\tMaterial component"))
        {
            // ImGui::ColorEdit4("Base color", glm::value_ptr(materialComponent.color));
            ImGui::ColorEdit4("Ambient", glm::value_ptr(materialComponent.ambient));
            ImGui::ColorEdit4("Diffuse", glm::value_ptr(materialComponent.diffuse));
            ImGui::ColorEdit4("Specular", glm::value_ptr(materialComponent.specular));
            ImGui::ColorEdit4("Emissive", glm::value_ptr(materialComponent.emissive));
            ImGui::DragFloat("Specular exponent", &materialComponent.specularExponent, 0.1f, 0.0f, 128.0f);
            ImGui::Separator();
        }
    }

    if (coordinator->hasComponent<VoxelComponent>(entity))
    {
        bool isOpened = true;
        auto& voxelComponent = coordinator->getComponent<VoxelComponent>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_CUBES "\tVoxel component", &isOpened))
        {
            ImGui::Indent(10.0f);
            ImGui::PushItemWidth(ImGui::GetFontSize() * -8);

            if (voxelComponent.hotReload)
            {
                shouldUpdate = false;
            }

            if (ImGui::Checkbox("Hot reload", &voxelComponent.hotReload))
            {
                if (voxelComponent.hotReload)
                {
                    shouldUpdate = true;
                }
            }

            if (voxelManager->currVoxelEntity == UINT32_MAX)
            {
                if (ImGui::BeginCombo("Current Entity Focus", "None"))
                {
                    for (int i = 0; i < scene->entities.size(); ++i)
                    {
                        if (ImGui::Selectable(coordinator->getComponent<Name>(scene->entities[i]).name.c_str()))
                        {
                            voxelManager->currVoxelEntity = scene->entities[i];
                            voxelComponent.focussedEntity = scene->entities[i];
                        }
                    }
                    ImGui::EndCombo();
                }
            }
            else
            {
                if (ImGui::BeginCombo("Current Entity Focus", coordinator->getComponent<Name>(voxelManager->currVoxelEntity).name.c_str()))
                {
                    for (int i = 0; i < scene->entities.size(); ++i)
                    {
                        if (ImGui::Selectable(coordinator->getComponent<Name>(scene->entities[i]).name.c_str()))
                        {
                            voxelManager->currVoxelEntity = scene->entities[i];
                            voxelComponent.focussedEntity = scene->entities[i];
                        }
                    }
                    if (ImGui::Selectable("None"))
                    {
                        voxelManager->currVoxelEntity = UINT32_MAX;
                        voxelComponent.focussedEntity = UINT32_MAX;
                        shouldUpdate = true;
                    }
                    ImGui::EndCombo();
                }
            }

            if (voxelManager->currVoxelEntity == UINT32_MAX)
            {
                if (ImGui::DragFloat3("Player position", glm::value_ptr(voxelComponent.playerPosition), 0.1f))
                {
                    voxelManager->startingPosition = voxelComponent.startPosition;

                    dag::voxelManager->update(voxelComponent.playerPosition);
                    resourceManager->models[voxelManager->meshHash] = voxelManager->model;

                    // for (auto& shape : voxelManager->model.shapes)
                    // {
                    //     renderingSystem->vaos[shape.VAOIndex].destroy();
                    //     renderingSystem->vaos[shape.VAOIndex] = dag::VertexArrayObject<float, uint32_t>{};
                    //     renderingSystem->vaos[shape.VAOIndex] = (resourceManager->generateVAO(shape, resourceManager->atts));
                    //     renderingSystem->vaos[shape.VAOIndex].indexBuffer.elements.shrink_to_fit();
                    //     renderingSystem->vaos[shape.VAOIndex].vertexBuffer.elements.shrink_to_fit();
                    // }
                }
            }

            if (ImGui::TreeNode("General##generalParams"))
            {

                shouldUpdate |= ImGui::DragFloat3("Start position", glm::value_ptr(voxelComponent.startPosition), 0.1f);
                shouldUpdate |= ImGui::SliderInt("Max size", &voxelComponent.maxSize, 1, 1000);
                shouldUpdate |= ImGui::SliderInt("Load size", &voxelComponent.loadSize, 1, voxelComponent.maxSize);
                shouldUpdate |= ImGui::SliderInt("Chunk size", &voxelComponent.chunkSize, 1, 100);

                ImGui::TreePop();
            }

            if (ImGui::TreeNode("Terrain##terrainGeneration"))
            {

                if (ImGui::BeginCombo("Terrain type", voxelComponent.terrainTypeName.c_str()))
                {
                    if (ImGui::Selectable("2D Noise"))
                    {
                        voxelComponent.terrainType = 0;
                        voxelComponent.terrainTypeName = "2D Noise";
                        shouldUpdate = true;
                    }
                    if (ImGui::Selectable("3D Noise"))
                    {
                        voxelComponent.terrainType = 1;
                        voxelComponent.terrainTypeName = "3D Noise";
                        shouldUpdate = true;
                    }
                    if (ImGui::Selectable("2D + 3D Noise"))
                    {
                        voxelComponent.terrainType = 2;
                        voxelComponent.terrainTypeName = "2D + 3D Noise";
                        shouldUpdate = true;
                    }
                    ImGui::EndCombo();
                }

                if (voxelComponent.terrainType % 2 == 0)
                {
                    shouldUpdate |= ImGui::DragFloat("2D Scale", &voxelComponent.scale2D, 0.001f, 0.0001f, 3.0f);
                }
                if (voxelComponent.terrainType > 0)
                {
                    shouldUpdate |= ImGui::DragFloat("3D Scale", &voxelComponent.scale3D, 0.001f, 0.0001f, 3.0f);
                }
                if (voxelComponent.terrainType % 2 == 0)
                {
                    shouldUpdate |= ImGui::DragInt("2D Iterations", &voxelComponent.iterations2D, 1.0f, 1, 10);
                }
                if (voxelComponent.terrainType > 0)
                {
                    shouldUpdate |= ImGui::DragInt("3D Iterations", &voxelComponent.iterations3D, 1.0f, 1, 10);
                }

                if (voxelComponent.terrainType % 2 == 0)
                {
                    shouldUpdate |= ImGui::DragFloat("2D Persistence", &voxelComponent.persistence2D, 0.01f, 0.01f, 1.0f);
                }
                if (voxelComponent.terrainType > 0)
                {
                    shouldUpdate |= ImGui::DragFloat("3D Persistence", &voxelComponent.persistence3D, 0.01f, 0.01f, 1.0f);
                }

                ImGui::TreePop();
            }

            if (ImGui::TreeNode("Other elements##other"))
            {
                shouldUpdate |= ImGui::Checkbox(ICON_FA_WATER "\tWater", &voxelComponent.hasWater);
                if (voxelComponent.hasWater)
                {
                    shouldUpdate |= ImGui::DragInt("Water level", &voxelComponent.waterHeight, 1.0f, 1, 8 * voxelComponent.chunkSize - 1);
                }
                shouldUpdate |= ImGui::Checkbox(ICON_FA_TREE "\tTrees", &voxelComponent.hasTrees);
                if (voxelComponent.hasTrees)
                {

                    shouldUpdate |= ImGui::DragFloat("Tree probability", &voxelComponent.treeProbability, 0.001f, 0.001f, 9);

                    shouldUpdate |= ImGui::DragInt("Min tree height", &voxelComponent.minTreeHeight, 1.0f, 1, voxelComponent.maxTreeHeight);
                    shouldUpdate |= ImGui::DragInt("Max tree height", &voxelComponent.maxTreeHeight, 1.0f, voxelComponent.minTreeHeight, 10);

                    shouldUpdate |= ImGui::DragInt("Min tree width", &voxelComponent.minTreeWidth, 1.0f, 1, voxelComponent.maxTreeWidth);
                    shouldUpdate |= ImGui::DragInt("Max tree width", &voxelComponent.maxTreeWidth, 1.0f, voxelComponent.minTreeWidth, 10);
                }
                shouldUpdate |= ImGui::Checkbox(ICON_FA_VIRUSES "\tFlowers", &voxelComponent.hasFlowers);
                if (voxelComponent.hasFlowers)
                {
                    shouldUpdate |= ImGui::DragFloat("Flower probability", &voxelComponent.flowerProbability, 0.001f, 0.001f, 9);
                }

                ImGui::TreePop();
            }
            ImGui::PopItemWidth();

            if (voxelComponent.hotReload || ImGui::Button("Regenerate voxels"))
            {
                if (shouldUpdate)
                {
                    voxelManager->startingPosition = voxelComponent.startPosition;
                    voxelManager->maxSize = voxelComponent.maxSize;
                    voxelManager->loadedSize = voxelComponent.loadSize;
                    voxelManager->chunkSize = voxelComponent.chunkSize;
                    voxelManager->terrainType = voxelComponent.terrainType;
                    voxelManager->scale2D = voxelComponent.scale2D;
                    voxelManager->scale3D = voxelComponent.scale3D;
                    voxelManager->iterations2D = voxelComponent.iterations2D;
                    voxelManager->iterations3D = voxelComponent.iterations3D;
                    voxelManager->persistence2D = voxelComponent.persistence2D;
                    voxelManager->persistence2D = voxelComponent.persistence3D;
                    voxelManager->hasWater = voxelComponent.hasWater;
                    voxelManager->waterHeight = voxelComponent.waterHeight;
                    voxelManager->hasTrees = voxelComponent.hasTrees;
                    voxelManager->treeProbability = voxelComponent.treeProbability;
                    voxelManager->minTreeHeight = voxelComponent.minTreeHeight;
                    voxelManager->maxTreeHeight = voxelComponent.maxTreeHeight;
                    voxelManager->minTreeWidth = voxelComponent.minTreeWidth;
                    voxelManager->maxTreeWidth = voxelComponent.maxTreeWidth;
                    voxelManager->hasFlowers = voxelComponent.hasFlowers;
                    voxelManager->flowerProbability = voxelComponent.flowerProbability;
                    shouldUpdate = false;
                    voxelManager->reset();
                    voxelManager->update(voxelComponent.playerPosition);
                    // for (auto& s : resourceManager->models[voxelManager->meshHash].shapes)
                    // {
                    //     s.vertices = std::vector<dag::Vertex>{};
                    //     s.indices = std::vector<unsigned int>{};
                    // }
                    resourceManager->models[voxelManager->meshHash] = dag::Model{};
                    resourceManager->models[voxelManager->meshHash] = voxelManager->model;

                    // renderingSystem->vaos[voxelManager->model.shapes[0].VAOIndex].destroy();

                    // std::cout << renderingSystem->vaos.size() << std::endl;
                    // for (auto& i : renderingSystem->vaos)
                    // {
                    //     std::cout << "|_" << sizeof(i) << std::endl;
                    //     std::cout << "  |_ib: " << i.indexBuffer.elements.size() << std::endl;
                    //     std::cout << "    |_cap: " << i.indexBuffer.elements.capacity() << std::endl;
                    //     std::cout << "  |_vb: " << i.vertexBuffer.elements.size() << std::endl;
                    //     std::cout << "    |_cap: " << i.vertexBuffer.elements.capacity() << std::endl;
                    // }

                    // for (auto& shape : voxelManager->model.shapes)
                    // {
                    //     renderingSystem->vaos[shape.VAOIndex].destroy();
                    //     renderingSystem->vaos[shape.VAOIndex] = dag::VertexArrayObject<float, uint32_t>{};
                    //     renderingSystem->vaos[shape.VAOIndex] = (resourceManager->generateVAO(shape, resourceManager->atts));
                    //     renderingSystem->vaos[shape.VAOIndex].indexBuffer.elements.shrink_to_fit();
                    //     renderingSystem->vaos[shape.VAOIndex].vertexBuffer.elements.shrink_to_fit();
                    // }
                }
            }
            ImGui::Separator();
        }
        if (!isOpened)
        {
            dag::scene->removeComponent<VoxelComponent>(entity);
            voxelManager->currVoxelEntity = UINT32_MAX;
            voxelManager->currVoxelMainEntity = UINT32_MAX;
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_CUBES "\tVoxel component");
    }

    if (coordinator->hasComponent<PlayerController>(entity))
    {
        bool isOpened = true;
        auto& playerController = coordinator->getComponent<PlayerController>(entity);
        if (ImGui::CollapsingHeader(ICON_FA_USER "\tPlayer controller", &isOpened))
        {
            if (ImGui::Checkbox("Active", &playerController.isActive))
            {
                coordinator->getSystem<dag::PlayerControllerSystem>()->setPossessed(entity, playerController.isActive);
            }

            ImGui::Checkbox("FPS Style", &playerController.fps_style);

            ImGui::DragFloat("Walking speed", &playerController.walkSpeed, 0.1f, 0.0f, 10.0f);
            ImGui::DragFloat("Running multiplier", &playerController.runMultiplier, 0.1f, 1.0f, 5.0f);
            ImGui::Separator();
        }
        if (!isOpened)
        {
            coordinator->getSystem<dag::PlayerControllerSystem>()->setPossessed(entity, false);
            dag::scene->removeComponent<PlayerController>(entity);
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_USER "\tPlayer controller");
    }

    if (coordinator->hasComponent<Volume>(entity))
    {
        bool isOpened = true;
        auto& volume = coordinator->getComponent<Volume>(entity);

        if (ImGui::CollapsingHeader(ICON_FA_MAGIC "\tVolume", &isOpened))
        {
            static std::string types[2] = {"Global", "Local"};

            if (ImGui::BeginCombo("Type", types[volume.type].c_str()))
            {
                for (size_t i = 0; i < 2; i++)
                {
                    if (ImGui::Selectable(types[i].c_str(), volume.type == i))
                    {
                        volume.type = static_cast<Volume::Type>(i);
                    }
                }
                ImGui::EndCombo();
            }

            if (volume.type == 1)
            {
                // TODO Check if entity has collider component
                ImGui::TextColored(ImVec4(0.8f, 0.2f, 0.2f, 1.0f), "Local bounds require a collider component in this entity.");
            }

            static const int count = 8;

            static std::array<std::string, count> postProcessNames = {"Fog", "Sharpen", "Box Blur", "Tone Mapping", "Chromatic Aberration", "Pixelize", "Dilation", "Bloom"};

            using RSPP = RenderingSystem::PostProcessFuncID;

            static std::array<RenderingSystem::PostProcessFuncID, count> postProcessFuncID = {
                RSPP::Fog,
                RSPP::Sharpen,
                RSPP::Blur,
                RSPP::ToneMapping,
                RSPP::ChromaticAberration,
                RSPP::Pixelize,
                RSPP::Dilation,
                RSPP::Bloom};

            static std::array<bool, count> postProcessEnabled = {false};

            // Fog settings
            {
                size_t id = static_cast<size_t>(RSPP::Fog);
                ImGui::PushID(id);
                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }

                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str(), postProcessEnabled[id]))
                {
                    ImGui::Indent(27.0f);

                    auto& buf = renderingSystem->uniformHandler.ubPPFogData.elements[0];
                    ImGui::DragFloat("Near", &volume.fogData.fogNear);
                    ImGui::DragFloat("Far", &volume.fogData.fogFar);
                    ImGui::DragFloat("Density", &volume.fogData.density);
                    ImGui::ColorEdit4("Color", glm::value_ptr(volume.fogData.fogColor));

                    buf.fogNear = volume.fogData.fogNear;
                    buf.fogFar = volume.fogData.fogFar;
                    buf.density = volume.fogData.density;
                    buf.fogColor = volume.fogData.fogColor;

                    ImGui::Unindent(27.0f);
                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Sharpen Settings

                size_t id = static_cast<size_t>(RSPP::Sharpen);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }

                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPSharpenData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragFloat("Amount", &volume.sharpenData.amount);
                    buf.amount = volume.sharpenData.amount;

                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Blur Settings
                size_t id = static_cast<size_t>(RSPP::Blur);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }
                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPBlurData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragInt("Kernel Size", &volume.blurData.kernelSize, 0.1f, 0.0f, 100.0f);
                    buf.kernelSize = volume.blurData.kernelSize;
                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Tone Map Settings
                size_t id = static_cast<size_t>(RSPP::ToneMapping);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }
                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    ImGui::Indent(27.0f);

                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // ChromaticAberration Settings
                size_t id = static_cast<size_t>(RSPP::ChromaticAberration);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }

                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPChromaticAberrationData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragFloat("R - Offset", &volume.chromaticAberrationData.rOffset, 0.001f, -5.0f, 5.0f);
                    ImGui::DragFloat("G - Offset", &volume.chromaticAberrationData.gOffset, 0.001f, -5.0f, 5.0f);
                    ImGui::DragFloat("B - Offset", &volume.chromaticAberrationData.bOffset, 0.001f, -5.0f, 5.0f);

                    buf.rOffset = volume.chromaticAberrationData.rOffset;
                    buf.gOffset = volume.chromaticAberrationData.gOffset;
                    buf.bOffset = volume.chromaticAberrationData.bOffset;

                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Pixelize Settings
                size_t id = static_cast<size_t>(RSPP::Pixelize);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }
                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPPixelizeData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragInt("Pixel Size", &volume.pixelizeData.pixelSize, 0.1f, -5.0f, 5.0f);

                    buf.pixelSize = volume.pixelizeData.pixelSize;

                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Dilation Settings
                size_t id = static_cast<size_t>(RSPP::Dilation);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }
                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPDilationData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragInt("Kernel Size", &volume.dilationData.kernelSize, 0.1f, 0, 10.0f);
                    ImGui::DragFloat("Separation", &volume.dilationData.separation, 0.1f, 0.0f, 10.0f);
                    ImGui::DragFloat("Min. Threshold", &volume.dilationData.minThreshold, 0.01f, 0.0f, buf.maxThreshold);
                    ImGui::DragFloat("Max. Threshold", &volume.dilationData.maxThreshold, 0.01f, buf.minThreshold, 10.0f);
                    ImGui::Unindent(27.0f);

                    buf.kernelSize = volume.dilationData.kernelSize;
                    buf.separation = volume.dilationData.separation;
                    buf.minThreshold = volume.dilationData.minThreshold;
                    buf.maxThreshold = volume.dilationData.maxThreshold;

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            { // Bloom Settings

                size_t id = static_cast<size_t>(RSPP::Bloom);
                ImGui::PushID(id);

                bool used = ImGui::Checkbox(("##" + postProcessNames[id]).c_str(), &postProcessEnabled[id]);

                if (used)
                {
                    if (postProcessEnabled[id])
                    {
                        renderingSystem->enqueuePostProcessEffect(postProcessFuncID[id]);
                    }
                    else
                    {
                        renderingSystem->dequeuePostProcessEffect(postProcessFuncID[id]);
                    }
                }

                ImGui::SameLine();

                if (!postProcessEnabled[id])
                {
                    pushInactive();
                }

                if (ImGui::CollapsingHeader(postProcessNames[id].c_str()))
                {
                    auto& buf = renderingSystem->uniformHandler.ubPPBloomData.elements[0];
                    ImGui::Indent(27.0f);

                    ImGui::DragFloat("Threshold", &volume.bloomData.threshold, 0.01f, 0.0f, 1.0f);
                    ImGui::DragFloat("Offset", &volume.bloomData.offset, 0.01f, 0.0f, 5.0f);

                    buf.threshold = volume.bloomData.threshold;
                    buf.offset = volume.bloomData.offset;

                    ImGui::Unindent(27.0f);

                    ImGui::Separator();
                }

                if (!postProcessEnabled[id])
                {
                    popInactive();
                }

                ImGui::PopID();
            }

            ImGui::Separator();
        }
        if (!isOpened)
        {
            scene->removeComponent<Volume>(entity);
            renderingSystem->postProcessStack.clear();
            renderingSystem->postProcessStack.shrink_to_fit();
        }
    }
    else
    {
        availableComponents.push_back(ICON_FA_MAGIC "\tVolume");
    }

    static ImGuiComboFlags flags = 0;
    flags &= ~ImGuiComboFlags_NoPreview;

    static int currentComponent = 0;

    if (ImGui::Button("Add component", ImVec2(ImGui::GetContentRegionAvail().x, 1.5f * ImGui::GetFontSize())))
    {
        ImGui::OpenPopup("componentsPopup");
    }

    if (ImGui::BeginPopup("componentsPopup"))
    {
        if (availableComponents.size() == 0)
        {
            ImGui::Text("No available components");
        }
        for (int i = 0; i < availableComponents.size(); ++i)
        {
            if (ImGui::Selectable(availableComponents[i].c_str()))
            {
                if (availableComponents[i] == ICON_FA_COMPRESS_ARROWS_ALT "\tRigid body")
                {
                    dag::scene->addComponent(entity, dag::RigidBody{.mass = 1.0f});
                    dag::scene->addComponent(entity, dag::CollisionSphere{.radius = 0.1f, .center = glm::vec3(0.0)});
                }
                else if (availableComponents[i] == ICON_FA_GLOBE "\tCollision Sphere")
                {
                    if (coordinator->hasComponent<CollisionAABB>(entity))
                    {
                        coordinator->removeComponent<CollisionAABB>(entity);
                    }
                    dag::scene->addComponent(entity, dag::CollisionSphere{.radius = 0.1f, .center = glm::vec3(0.0)});
                }
                else if (availableComponents[i] == ICON_FA_CUBE "\tCollision AABB")
                {
                    if (coordinator->hasComponent<CollisionSphere>(entity))
                    {
                        coordinator->removeComponent<CollisionSphere>(entity);
                    }
                    dag::scene->addComponent(entity, dag::CollisionAABB{.width = 0.1f, .height = 0.1f, .depth = 0.1f, .center = glm::vec3(0.0)});
                }
                else if (availableComponents[i] == ICON_FA_SHAPES "\tMesh renderer")
                {
                    dag::scene->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Cube")]);
                    dag::scene->addComponent(entity, MaterialComponent{});
                }
                else if (availableComponents[i] == ICON_FA_VIDEO "\tCamera component")
                {
                    dag::scene->addComponent(entity, dag::CameraComponent{});
                }
                else if (availableComponents[i] == ICON_FA_LIGHTBULB "\tLight component")
                {
                    dag::scene->addComponent(entity, dag::LightComponent{});
                    coordinator->getSystem<LightSystem>()->updateLookupVectors();
                }
                else if (availableComponents[i] == ICON_FA_FILE_CODE "\tScript component")
                {
                    dag::scene->addComponent(entity, dag::ScriptComponent{});
                }
                else if (availableComponents[i] == ICON_FA_CUBES "\tVoxel component")
                {
                    dag::scene->addComponent(entity, dag::VoxelComponent{});
                    voxelManager->currVoxelMainEntity = entity;
                }
                else if (availableComponents[i] == ICON_FA_USER "\tPlayer controller")
                {
                    dag::scene->addComponent(entity, dag::PlayerController{});

                    coordinator->getSystem<dag::PlayerControllerSystem>()->setPossessed(entity, true);
                }
                else if (availableComponents[i] == ICON_FA_MAGIC "\tVolume")
                {
                    dag::scene->addComponent(entity, dag::Volume{});
                }
            }
        }
        ImGui::EndPopup();
    }

    ImGui::End();
}

void dag::EditorUILayer::drawTransformButtons()
{
    ImGui::PushID(3);
    gizmoType == ImGuizmo::TRANSLATE ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_ARROWS_ALT, ImVec2(ImGui::GetFrameHeight(), ImGui::GetFrameHeight())))
    {
        gizmoType = ImGuizmo::TRANSLATE;
    }
    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Translate selected");
    }
    popButtonColor();
    ImGui::PopID();
    ImGui::SameLine();

    ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 6);
    ImGui::PushID(4);
    gizmoType == ImGuizmo::ROTATE ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_REDO_ALT, ImVec2(ImGui::GetFrameHeight(), ImGui::GetFrameHeight())))
    {
        gizmoType = ImGuizmo::ROTATE;
    }
    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Rotate selected");
    }
    popButtonColor();
    ImGui::PopID();

    ImGui::SameLine();
    ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 6);

    ImGui::PushID(5);
    gizmoType == ImGuizmo::SCALE ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_EXPAND_ALT, ImVec2(ImGui::GetFrameHeight(), ImGui::GetFrameHeight())))
    {
        gizmoType = ImGuizmo::SCALE;
    }
    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Scale selected");
    }
    popButtonColor();
    ImGui::PopID();
}

void dag::EditorUILayer::drawSnappingTools()
{
    drawGridSnappingTool();
    ImGui::SameLine();

    drawRotationSnappingTool();
    ImGui::SameLine();

    drawScaleSnappingTool();
}

void dag::EditorUILayer::drawGridSnappingTool()
{
    ImGui::PushID(6);

    shouldSnapGrid ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_BORDER_ALL, ImVec2(20, ImGui::GetFrameHeight())))
    {
        shouldSnapGrid = !shouldSnapGrid;
    }

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Enables or disables grid snapping");
    }

    popButtonColor();
    ImGui::PopID();
    ImGui::SameLine();

    if (!shouldSnapGrid)
    {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
    }

    ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 8);

    ImGui::PushID("snapGridPopup");

    std::string name(16, '\0');
    std::snprintf(&name[0], name.size(), "%.3f", snapGridSizes[currentSnapGridSizeID]);

    bool button = ImGui::Button(name.c_str());

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Current grid snap value");
    }

    if (button)
    {
        ImGui::OpenPopup("snapGridPopup");
    }

    if (ImGui::BeginPopup("snapGridPopup"))
    {
        for (int i = 0; i < 10; i++)
        {
            std::snprintf(&name[0], name.size(), "%.3f", snapGridSizes[i]);

            if (ImGui::Checkbox(name.c_str(), &snapGridSizesCheckboxes[i]))
            {
                snapGridSizesCheckboxes[currentSnapGridSizeID] = false;
                currentSnapGridSizeID = i;
            }
        }

        ImGui::EndPopup();
    }
    ImGui::PopID();

    if (!shouldSnapGrid)
    {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
    }
}

void dag::EditorUILayer::drawRotationSnappingTool()
{
    ImGui::PushID(7);

    shouldSnapRotation ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_REDO_ALT, ImVec2(20, ImGui::GetFrameHeight())))
    {
        shouldSnapRotation = !shouldSnapRotation;
    }
    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Enables or disables rotation snapping");
    }
    popButtonColor();
    ImGui::PopID();
    ImGui::SameLine();

    if (!shouldSnapRotation)
    {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
    }

    ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 8);
    ImGui::PushID("snapRotationPopup");

    std::string name(16, '\0');
    std::snprintf(&name[0], name.size(), "%.3f deg", snapRotationSizes[currentSnapRotationSizeID]);

    bool button = ImGui::Button(name.c_str());

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Current rotation snap value");
    }

    if (button)
    {
        ImGui::OpenPopup("snapRotationPopup");
    }

    if (ImGui::BeginPopup("snapRotationPopup"))
    {
        for (int i = 0; i < 7; i++)
        {
            std::snprintf(&name[0], name.size(), "%.3f deg", snapRotationSizes[i]);

            if (ImGui::Checkbox(name.c_str(), &snapRotationSizesCheckboxes[i]))
            {
                snapRotationSizesCheckboxes[currentSnapRotationSizeID] = false;
                currentSnapRotationSizeID = i;
            }
        }
        ImGui::EndPopup();
    }

    ImGui::PopID();

    if (!shouldSnapRotation)
    {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
    }
}

void dag::EditorUILayer::drawScaleSnappingTool()
{
    ImGui::PushID(8);

    shouldSnapScale ? pushActiveColor() : pushInactiveColor();
    if (ImGui::Button(ICON_FA_ARROWS_ALT_V, ImVec2(20, ImGui::GetFrameHeight())))
    {
        shouldSnapScale = !shouldSnapScale;
    }

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Enables or disables scale snapping");
    }

    popButtonColor();
    ImGui::PopID();
    ImGui::SameLine();

    if (!shouldSnapScale)
    {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
    }

    ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 8);

    ImGui::PushID("snapScalePopup");

    std::string name(16, '\0');
    std::snprintf(&name[0], name.size(), "%.3f", snapScaleSizes[currentSnapScaleSizeID]);

    bool button = ImGui::Button(name.c_str());

    if (ImGui::IsItemHovered())
    {
        ImGui::SetTooltip("Current scale snap value");
    }

    if (button)
    {
        ImGui::OpenPopup("snapScalePopup");
    }

    if (ImGui::BeginPopup("snapScalePopup"))
    {
        for (int i = 0; i < 7; i++)
        {
            std::snprintf(&name[0], name.size(), "%.3f", snapScaleSizes[i]);
            if (ImGui::Checkbox(name.c_str(), &snapScaleSizesCheckboxes[i]))
            {
                snapScaleSizesCheckboxes[currentSnapScaleSizeID] = false;
                currentSnapScaleSizeID = i;
            }
        }
        ImGui::EndPopup();
    }
    ImGui::PopID();

    if (!shouldSnapScale)
    {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
    }
}

void dag::EditorUILayer::drawWindowOverlay()
{
    if (shouldRenderFPS)
    {
        float offset = 5.0f;
        ImGui::SetCursorPos(ImVec2(5, 5.0));
        ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f FPS", ImGui::GetIO().Framerate);

        ImGui::SetCursorPos(ImVec2(5, 5.0 + ImGui::GetTextLineHeight()));
        ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f ms", 1000 / ImGui::GetIO().Framerate);

        ImGui::SetCursorPos(ImVec2(5, 5.0 + 2 * ImGui::GetTextLineHeight()));
#ifdef _WIN32
        float denom = dag::profiler->getTotalPhysicalMemory() * 0.000001;
#else
        float denom = dag::profiler->getTotalPhysicalMemory() * 0.001;
#endif
        if (denom != 0.0)
        {
            ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f MEM %%", (dag::profiler->getCurrentPhysicalMemoryUsed() * 0.000001) / denom);
        }
        else
        {
            ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f MEM %%", 0.0);
        }

        ImGui::SetCursorPos(ImVec2(5, 5.0 + 3 * ImGui::GetTextLineHeight()));
        ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f MEM MB", dag::profiler->getCurrentPhysicalMemoryUsed() * 0.000001);

        ImGui::SetCursorPos(ImVec2(5, 5.0 + 4 * ImGui::GetTextLineHeight()));
        auto currCPU = dag::profiler->getCurrentCPUUsed();
        if (currCPU < 10)
        {
            ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), " %.2f CPU %%", currCPU);
        }
        else
        {
            ImGui::TextColored(ImVec4(0.1f, 0.9f, 0.1f, 1.0f), "%.2f CPU %%", currCPU);
        }
    }

    // Drawing the icons for transform
    if (gameState != GameState::Play)
    {

        ImGui::SetCursorPos(ImVec2(ImGui::GetWindowWidth() - 400, 5));

        drawTransformButtons();

        ImGui::SameLine(ImGui::GetCursorPosX(), 20);

        drawSnappingTools();

        ImGui::SameLine(ImGui::GetCursorPosX(), 20);

        drawCameraController();

        ImGui::SetCursorPos(ImVec2(0, 0));
    }
}

void dag::EditorUILayer::drawGameStateButtons()
{
    if (ImGui::BeginMenuBar())
    {
        auto size = ImGui::GetWindowSize();

        float paddingX = size.x / 2;
        auto cursorPos = ImGui::GetCursorPosX();

        ImGui::SetCursorPosX(cursorPos + paddingX - 50);

        ImVec4 col = ImVec4(128.0 / 255.0, 23.0 / 255.0, 15.0 / 255.0, 1);

        ImGui::PushID(0);
        if (gameState == GameState::Play)
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
        }
        else
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(2 / 7.0f, 0.6f, 0.6f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(2 / 7.0f, 0.7f, 0.7f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(2 / 7.0f, 0.8f, 0.8f));
        }
        if (ImGui::Button(ICON_FA_PLAY))
        {
            if (gameState != GameState::Play)
            {
                if (gameState == GameState::Stop)
                {
                    dag::scene->serialize(prePlayState);
                }

                gameState = GameState::Play;
                coordinator->getSystem<dag::PhysicsSystem>()->gameState = GameState::Play;

                ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouse;

                prevDelta = ImVec2(0, 0);

                ImGui::FocusWindow(gameRenderWindow);
                renderWindowFocused = true;
                windowManager->disableCursor();
                renderingSystem->dequeueLateOverlays(RenderingSystem::OverlaysFuncID::Grid);
                renderingSystem->dequeueLateOverlaysEntity();
            }
        }
        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Play");
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::PushID(1);
        if (gameState == GameState::Play)
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(1 / 7.0f, 0.6f, 0.6f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(1 / 7.0f, 0.7f, 0.7f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(1 / 7.0f, 0.8f, 0.8f));
        }
        else
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
        }

        if (ImGui::Button(ICON_FA_PAUSE))
        {
            if (gameState == GameState::Play)
            {
                gameState = GameState::Pause;
                coordinator->getSystem<dag::PhysicsSystem>()->gameState = GameState::Pause;
                ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;

                ImGui::FocusWindow(nullptr);
                renderWindowFocused = false;
                windowManager->enableCursor();

                renderingSystem->dequeueLateOverlaysEntity();
            }
        }

        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Pause");
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::PushID(2);
        if (gameState == GameState::Play || gameState == GameState::Pause)
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0 / 7.0f, 0.6f, 0.6f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0 / 7.0f, 0.7f, 0.7f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0 / 7.0f, 0.8f, 0.8f));
        }
        else
        {
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.2f, 0.2f, 0.2f));
        }
        if (ImGui::Button(ICON_FA_STOP))
        {
            if (gameState != GameState::Stop)
            {
                gameState = GameState::Stop;
                coordinator->getSystem<dag::PhysicsSystem>()->gameState = GameState::Stop;
                ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;

                dag::scene->deserialize(prePlayState, std::filesystem::path("dummy_path"), false);

                for (auto& e : dag::scene->entities)
                {
                    if (coordinator->hasComponent<RigidBody>(e))
                    {
                        auto& rb = coordinator->getComponent<RigidBody>(e);
                        rb.acceleration = glm::vec3(0.0);
                        rb.externalForce = glm::vec3(0.0);
                        rb.netForce = glm::vec3(0.0);
                        rb.velocity = glm::vec3(0.0);
                    }
                }

                ImGui::FocusWindow(nullptr);
                renderWindowFocused = false;
                windowManager->enableCursor();
            }

            renderingSystem->dequeueLateOverlaysEntity();

            if (shouldRenderGrid)
            {
                renderingSystem->enqueueLateOverlays(RenderingSystem::OverlaysFuncID::Grid);
            }
        }

        if (ImGui::IsItemHovered())
        {
            ImGui::SetTooltip("Stop");
        }

        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::EndMenuBar();
    }
}

void dag::EditorUILayer::refreshCachedDirs()
{
    cached_left_dirs.clear();
    cached_right_dirs.clear();

    for (auto& p : fs::directory_iterator(currentFileManagerPath))
    {
        if (p.is_directory())
        {
            cached_left_dirs.emplace_back(p);
        }
        else if (p.is_regular_file())
        {
            cached_right_dirs.emplace_back(p);
        }
    }
}

void dag::EditorUILayer::pushActiveColor()
{
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0, 0, 0.1f));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0, 0, 0.3f));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0, 0, 0.4f));
}

void dag::EditorUILayer::pushInactiveColor()
{
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0, 0.f, 0.5f));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0, 0.f, 0.6f));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0, 0.f, 0.7f));
}

void dag::EditorUILayer::popButtonColor()
{
    ImGui::PopStyleColor(3);
}

void dag::EditorUILayer::pushInactive()
{
    ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
    ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
}

void dag::EditorUILayer::popInactive()
{
    ImGui::PopItemFlag();
    ImGui::PopStyleVar();
}

/**
 * @brief Draws the scene graph
 *
 */
void dag::EditorUILayer::drawSceneGraph()
{
    ImGui::Begin("Scene Graph");

    // TODO: Set the button padding right.
    ImGui::BeginChild("DragDropContext", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y));

    ImGui::Text("Scene");
    ImGui::SameLine(ImGui::GetContentRegionAvail().x - 6.0f * ImGui::GetFontSize());
    ImGui::SetNextItemWidth(5.0f * ImGui::GetFontSize());

    if (ImGui::Button("Add entity"))
    {
        ImGui::OpenPopup("entitiesPopup");
    }

    if (ImGui::BeginPopup("entitiesPopup"))
    {
        for (int i = 0; i < scene->defaultEntities.size(); ++i)
        {
            if (ImGui::Selectable(scene->defaultEntities[i].c_str()))
            {
                scene->createEntity(scene->defaultEntities[i]);
            }
        }
        ImGui::EndPopup();
    }

    for (int i = 0; i < dag::scene->sceneGraph.size(); ++i)
    {
        const bool is_selected = (dag::scene->currentEntity == i);

        std::string title = " " + dag::scene->sceneGraph[i];

        ImGui::PushID((title + std::to_string(i)).c_str());
        if (ImGui::Selectable(title.c_str(), is_selected, ImGuiSelectableFlags_SpanAllColumns, ImVec2(ImGui::GetContentRegionAvail().x - 30.0f, ImGui::GetTextLineHeightWithSpacing())))
        {
            renderingSystem->dequeueLateOverlaysEntity();
            scene->currentEntity = i;
            scene->currentRenderableEntity = renderingSystem->getEntityOffset(scene->entities[i]);
            renderingSystem->enqueueLateOverlaysEntity();
        }

        if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0))
        {
            Log::debug("Double clicking entity ", i);

            Entity selectedEntity = dag::scene->entities[dag::scene->currentEntity];
            {
                auto& camera = coordinator->getComponent<CameraComponent>(coordinator->getSystem<CameraSystem>()->activeCamera);
                auto& transform = coordinator->getComponent<Transform>(coordinator->getSystem<CameraSystem>()->activeCamera);

                auto cameraView = camera.getView(transform.position, transform.rotation);

                auto& entityTransformComponent = coordinator->getComponent<dag::Transform>(selectedEntity);
                auto entityPosition = entityTransformComponent.position;

                auto dir = glm::normalize(entityPosition - transform.position);
                auto preferredView = glm::lookAt(transform.position, transform.position + dir, glm::vec3(0, 1, 0));

                glm::vec3 translation;
                glm::vec3 rotation;
                glm::vec3 scale;
                ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(glm::inverse(preferredView)), glm::value_ptr(translation), glm::value_ptr(rotation), glm::value_ptr(scale));

                // TODO Implement
                // transform.position = translation;
                // transform.rotation = rotation;
            }
        }

        ImGui::SameLine();

        if (ImGui::Button(ICON_FA_TIMES, ImVec2(20.0f, ImGui::GetTextLineHeightWithSpacing())))
        {
            // std::cout << i << std::endl;
            if (coordinator->hasComponent<CameraComponent>(dag::scene->entities[i]))
            {
                auto& cameraComponent = coordinator->getComponent<CameraComponent>(dag::scene->entities[i]);
                if (cameraComponent.isPossessed)
                {
                    cameraComponent.isPossessed = false;
                    renderingSystem->cameraSystem->setPossessed(dag::scene->entities[i], cameraComponent.isPossessed);
                }
            }
            if (coordinator->hasComponent<PlayerController>(dag::scene->entities[i]))
            {
                auto& playerController = coordinator->getComponent<PlayerController>(dag::scene->entities[i]);
                if (playerController.isActive)
                {
                    coordinator->getSystem<PlayerControllerSystem>()->setPossessed(dag::scene->entities[i], false);
                }
            }
            if (coordinator->hasComponent<LightComponent>(scene->entities[i]))
            {
                scene->removeComponent<LightComponent>(scene->entities[i]);
                coordinator->getSystem<LightSystem>()->updateLookupVectors();
            }

            if (voxelManager->currVoxelEntity == dag::scene->entities[i])
            {
                voxelManager->currVoxelEntity = UINT32_MAX;
            }
            if (voxelManager->currVoxelMainEntity == dag::scene->entities[i])
            {
                voxelManager->currVoxelMainEntity = UINT32_MAX;
            }

            scene->destroyEntity(dag::scene->entities[i]);

            ImGui::PopID();
            ImGui::EndChild();
            ImGui::End();
            return;
        }

        ImGui::PopID();
        // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
        if (is_selected)
        {
            ImGui::SetItemDefaultFocus();
        }
    }

    // Needed for dragNdrop
    ImGui::EndChild();

    // Drop Target from File Manager
    if (ImGui::BeginDragDropTarget())
    {
        if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DRAG_OBJECTS"))
        {
            std::string path_string = dragPayload.generic_string();

            std::cout << path_string << std::endl;

            std::string name = std::filesystem::relative(dragPayload, dag::filesystem::getAssetsPath()).string();

            // int index = name.find_last_of("assets");
            // if(index != std::string::npos){
            //     name = name.substr(index + 1);
            // }
            // index = name.find_last_of("/");
            // if(index != std::string::npos){
            //     name = name.substr(index + 1);
            // }

            dag::scene->createEntity(name);

            auto lastEntity = dag::scene->entities.back();

            auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();

            size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(dragPayload, dag::filesystem::getAssetsPath()).string());

            if (!resourceManager->models.contains(inputHash))
            {

                size_t meshHash = dag::resourceManager->parseOBJ(dragPayload);

                std::cout << name << std::endl;

                for (auto& shape : dag::resourceManager->models[meshHash].shapes)
                {
                    std::cout << "shape: " << std::endl;
                    std::cout << shape.indices.size() << std::endl;
                    std::cout << shape.vertices.size() << std::endl;
                    std::cout << shape.material.name << std::endl;
                    rs->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
                    shape.VAOIndex = rs->vaos.size() - 1;
                }

                MeshRenderer meshRenderer;
                meshRenderer.name = name;
                meshRenderer.vaoID = rs->vaos.size() - 1;
                meshRenderer.hashIndex = meshHash;

                resourceManager->meshRenderers.insert({meshHash, meshRenderer});
            }
            coordinator->addComponent(lastEntity, resourceManager->meshRenderers[inputHash]);
            coordinator->addComponent(lastEntity, MaterialComponent{});
        }

        ImGui::EndDragDropTarget();
    }

    ImGui::End();
}

/**
 * @brief Draws the controls for the physics system
 *
 */
void dag::EditorUILayer::drawPhysicsSystem()
{
    ImGui::Begin("Physics System");

    ImGuiColorEditFlags flags = ImGuiColorEditFlags_DisplayRGB;

    ImGui::SliderFloat("Time Multiplier", &(coordinator->getSystem<PhysicsSystem>()->timeMultiplier), 0.1f, 10.0f);

    ImGui::SliderFloat3("Gravity", &(coordinator->getSystem<PhysicsSystem>()->gravity[0]), -10.0f, 10.0f);

    ImGui::SliderFloat("Air Resistance", &(coordinator->getSystem<PhysicsSystem>()->airResistance), 0.0f, 1.0f);
    ImGui::SliderFloat("Epsilon", &(coordinator->getSystem<PhysicsSystem>()->epsilon), 0.0f, 1.0f);
    ImGui::SliderFloat("Dispersion Coefficient", &(coordinator->getSystem<PhysicsSystem>()->dispersionCoefficient), 0.0f, 1.0f);

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    if (ImGui::SliderInt("Number of Threads", &physicsThreadCount, 1, std::thread::hardware_concurrency()) || (physicsThreadCount != coordinator->getSystem<PhysicsSystem>()->numThreads))
    {
        if (!coordinator->getSystem<PhysicsSystem>()->adjustingThreads)
        {
            std::unique_lock lock(coordinator->getSystem<PhysicsSystem>()->physicsMutex);
            coordinator->getSystem<PhysicsSystem>()->createThreads(physicsThreadCount);
        }
    }

    ImGui::End();
}

void dag::EditorUILayer::renderUI()
{
    // Backbuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, windowManager->window.info.width, windowManager->window.info.height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // new frame from helper functions
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
    ImGuizmo::BeginFrame();

    ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());

    fileDialog.Display();

    drawTopBar();
    drawOpenGLRenderWindow();
    drawPhysicsSystem();
    drawSettingsMenu();
    drawFileManager();
    drawSceneGraph();
    drawInspector();

    ImGui::Begin("Profiler Window");
    profiler->imguiPlotMem();
    ImGui::End();

    dag::Console::get().draw("Console", NULL);

    //ImGui::ShowDemoWindow();

    ImGui::Render();

    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    ImGuiIO& io = ImGui::GetIO();

    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        GLFWwindow* backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_current_context);
    }
}

void dag::EditorUILayer::drawSettingsMenu()
{
    ImGui::Begin("Rendering Settings");

    ImGuiColorEditFlags flags = ImGuiColorEditFlags_DisplayRGB;

    static std::vector<std::string> renderingModes = {"Forward Rendering", "Deferred Rendering"};
    std::string currentItem = renderingModes[static_cast<size_t>(renderingSystem->currentRenderMode)];

    if (ImGui::BeginCombo("##Rendering Modes", currentItem.c_str()))
    {
        for (size_t i = 0; i < renderingModes.size(); i++)
        {
            bool isSelected = (currentItem == renderingModes[i]);

            if (ImGui::Selectable(renderingModes[i].c_str(), isSelected))
            {
                currentItem = renderingModes[i];

                if (i == 0)
                {
                    renderingSystem->setRenderingMode(RenderingSystem::RenderMode::ForwardRendering, false, true);
                }
                else
                {
                    renderingSystem->setRenderingMode(RenderingSystem::RenderMode::DeferredRendering, false, true);
                }
            }
        }

        ImGui::EndCombo();
    }

    ImGui::Separator();

    if (ImGui::CollapsingHeader("Render Graph"))
    {
        for (auto& renderPasse : renderingSystem->renderPasses)
        {
            ImGui::BulletText((renderPasse.name).c_str());
        }
    }

    ImGui::End();
}
