#pragma once
#include "dagmar/Layer.h"
#include "dagmar/WindowResizeEvent.h"
#include <dagmar/RenderPass.h>
#include <dagmar/RenderingSystem.h>
#include <memory>

namespace dag
{
    /**
     * @brief Layer class for the editor UI
     *
     * Mainly initializes the library used to show the UI
     */
    class RendererLayer : public Layer
    {
      public:
        GameState gameState;

    	std::shared_ptr<Layer> editorUILayer;

      private:
        std::shared_ptr<RenderingSystem> renderingSystem;

        

      public:
        RendererLayer();
        virtual ~RendererLayer() = default;

        void onAttach() override;
        void onDetach() override;
        void onUpdate(float const& dt = 0.1f) override;
        void onEvent(Event& event) override;

      private:
        bool onWindowResize(WindowResizeEvent& event);


       
    };
} // namespace dag