#pragma once
#include "dagmar/KeyPressedEvent.h"
#include "dagmar/KeyReleasedEvent.h"
#include "dagmar/KeyTypedEvent.h"
#include "dagmar/Layer.h"
#include "dagmar/MouseButtonPressedEvent.h"
#include "dagmar/MouseButtonReleasedEvent.h"
#include "dagmar/MouseMovedEvent.h"
#include "dagmar/MouseScrolledEvent.h"
#include "dagmar/WindowResizeEvent.h"
#include <dagmar/FileSystem.h>
#include <dagmar/RenderingSystem.h>
#include <glm/glm.hpp>
#include <memory>

#include <ImGuizmo.h>
#include <fstream>
#include <future>
#include <imfilebrowser.h>
#include <imgui.h>
#include <imgui_internal.h>

#include <nlohmann/json.hpp>

namespace dag
{
    /**
     * @brief Layer class for the editor UI
     *
     * Mainly initializes the library used to show the UI
     */
    class EditorUILayer : public Layer
    {
      public:
        GameState gameState = GameState::Stop;

        std::shared_ptr<Layer> rendererLayer;


      private:
        nlohmann::json prePlayState;

        std::vector<std::filesystem::directory_entry> cached_left_dirs;
        std::vector<std::filesystem::directory_entry> cached_right_dirs;

        std::shared_ptr<RenderingSystem> renderingSystem;

        std::future<int> compilerReturnCode;

        ImVec2 prevDelta;

        float mouseDragSensitivity = 0.10f;

        bool shouldUpdate = false;

        int physicsThreadCount = 4;

        glm::vec3 updatedScale;
        glm::vec3 updatedRotation;
        glm::vec3 updatedTranslation;

        std::filesystem::path currentFileManagerPath;
        std::filesystem::path currentFileManagerSelectedPath;

        bool renderWindowFocused = false;
        bool renderWindowHovered = false;
        bool isOverGizmo = false;

        int gizmoType = ImGuizmo::TRANSLATE;
        bool shouldRenderGrid = true;
        bool shouldRenderFPS = true;
        bool shouldRenderColliderDebugs = false;

        bool shouldSnapGrid = true;
        size_t currentSnapGridSizeID = 0;
        float snapGridSizes[10] = {1, 5, 10, 15, 20, 25, 50, 100, 1000, 10000};
        bool snapGridSizesCheckboxes[10] = {};

        bool shouldSnapRotation = true;
        size_t currentSnapRotationSizeID = 0;
        float snapRotationSizes[7] = {1, 5, 10, 30, 60, 90, 120};
        bool snapRotationSizesCheckboxes[7] = {};

        bool shouldSnapScale = true;
        size_t currentSnapScaleSizeID = 0;
        float snapScaleSizes[7] = {0.125, 0.25, 0.5, 1, 2, 5, 10};
        bool snapScaleSizesCheckboxes[7] = {};

        std::vector<std::string> allScriptNames;
        char scriptFileName[20] = "";
        std::filesystem::path scriptPath;
        const std::filesystem::path stencilPath = dag::filesystem::getAssetsPath() / "internal/Stencil.cpp";

        std::filesystem::path dragPayload;

        ImGuiWindow* gameRenderWindow;

        std::filesystem::path currOpenedSceneFile = "";

        ImGui::FileBrowser fileDialog;

        enum class dialogState
        {
            INACTIVE,
            SAVING_SCENE,
            LOADING_SCENE,
            EXPORTING
        };

        dialogState currDialogState = dialogState::INACTIVE;

      public:
        // Temporary variable, don't change values or you get division by zero
        // if we maximize the application instantly
        ImVec2 viewportSize = ImVec2(1024, 1024);
        ImVec2 viewportPos = ImVec2(0, 0);

        glm::quat initialRotation;

      public:
        EditorUILayer();
        virtual ~EditorUILayer() = default;

        void onAttach() override;
        void onDetach() override;
        void onUpdate(float const& dt = 0.1f) override;
        void onEvent(Event& event) override;

      private:
        void setEditorColors();

        void renderUI();

        void drawSettingsMenu();

        // Shows the top bar
        void drawTopBar();

        // Top bar menus?
        void drawDebugMenu();

        // draws the opengl window
        void drawOpenGLRenderWindow();

        // draws a physics control window
        void drawPhysicsSystem();

        // draws file manager
        void drawFileManager();

        void drawCameraController();
        // draws the scene graph
        void drawSceneGraph();

        // draws the inspector
        void drawInspector();

        // Overlays
        void drawTransformButtons();
        void drawSnappingTools();

        void drawGridSnappingTool();
        void drawRotationSnappingTool();
        void drawScaleSnappingTool();

        void drawWindowOverlay();
        void drawGameStateButtons();

        void refreshCachedDirs();

        // Helpers
        void pushActiveColor();
        void pushInactiveColor();
        void popButtonColor();

    	void pushInactive();
        void popInactive();

        bool onKeyTyped(KeyTypedEvent& event);
        bool onKeyPressed(KeyPressedEvent& event);
        bool onKeyReleased(KeyReleasedEvent& event);
        bool onMouseScrolled(MouseScrolledEvent& event);
        bool onMouseMoved(MouseMovedEvent& event);
        bool onMouseButtonPressed(MouseButtonPressedEvent& event);
        bool onMouseButtonReleased(MouseButtonReleasedEvent& event);
        bool onWindowResize(WindowResizeEvent& event);
    };
} // namespace dag