add_library(resource_loader STATIC
    ResourceLoader.cpp
 )

target_include_directories(resource_loader PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(resource_loader PUBLIC
    Dagmar::Log
    Dagmar::Resources
    Dagmar::Voxel
)

install(TARGETS resource_loader)

add_library(Dagmar::Editor::ResourceLoader ALIAS resource_loader)