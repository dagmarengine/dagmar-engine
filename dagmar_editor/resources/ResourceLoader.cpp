#include "dagmar/ResourceLoader.h"

#include <string>
#include <vector>

#include "dagmar/Coordinator.h"
#include "dagmar/FileSystem.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/VoxelChunkManager.h"

/**
 * @brief Loads the screen quad
 */
void dag::ResourceLoader::loadScreenQuad()
{
    auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();
    {

        // Load default screen plane
        std::vector<float> vertices =
            {
                1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 1.0f};

        std::vector<uint32_t> indices =
            {
                0, 1, 3, 1, 2, 3};

        auto screenMesh = dag::Mesh(vertices, indices);

        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0),
            dag::VertexAttribute(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)))};

        rs->vaos.emplace_back(screenMesh.generateVAO(atts));
    }

    {
        // Load default screen plane
        std::vector<float> vertices =
            {
                -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, 1, -1, -1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1};

        std::vector<uint32_t> indices =
            {
                1, 5, 5, 6, 6, 2, 2, 1, 3, 7, 7, 4, 4, 0, 0, 3, 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4};

        auto frustumMesh = dag::Mesh(vertices, indices);

        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), static_cast<void*>(0))};

        rs->vaos.emplace_back(frustumMesh.generateVAO(atts));
    }

    {
        // DefaultSkycube

        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0)};

        std::vector<float> vertices =
            {
                -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, 1, -1, -1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1};

        std::vector<uint32_t> indices =
            {
                0, 1, 3, 3, 1, 2, 1, 5, 2, 2, 5, 6, 5, 4, 6, 6, 4, 7, 4, 0, 7, 7, 0, 3, 3, 2, 7, 7, 2, 6, 4, 5, 0, 0, 5, 1};

        auto skymesh = dag::Mesh(vertices, indices);

        rs->vaos.emplace_back(skymesh.generateVAO(atts));
    }

    {
        // DefaultSphereBounds

        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0)};

        std::vector<float> vertices =
            {
                // SPHERE 2
                1.000000,
                0.000000,
                -0.000000,
                0.973563,
                0.000000,
                -0.229134,
                0.898272,
                0.000000,
                -0.439557,
                0.780154,
                0.000000,
                -0.625240,
                0.625240,
                0.000000,
                -0.780154,
                0.439557,
                0.000000,
                -0.898272,
                0.229134,
                0.000000,
                -0.973563,
                -0.000000,
                0.000000,
                -1.000000,
                -0.229134,
                0.000000,
                -0.973563,
                -0.439557,
                0.000000,
                -0.898272,
                -0.625240,
                0.000000,
                -0.780154,
                -0.780154,
                0.000000,
                -0.625240,
                -0.898272,
                0.000000,
                -0.439557,
                -0.973563,
                0.000000,
                -0.229134,
                -1.000000,
                0.000000,
                0.000000,
                -0.973563,
                0.000000,
                0.229134,
                -0.898272,
                0.000000,
                0.439557,
                -0.780154,
                0.000000,
                0.625240,
                -0.625240,
                0.000000,
                0.780154,
                -0.439557,
                0.000000,
                0.898272,
                -0.229134,
                0.000000,
                0.973563,
                0.000000,
                0.000000,
                1.000000,
                0.229134,
                0.000000,
                0.973563,
                0.439557,
                0.000000,
                0.898272,
                0.625240,
                0.000000,
                0.780154,
                0.780154,
                0.000000,
                0.625240,
                0.898272,
                0.000000,
                0.439557,
                0.973563,
                0.000000,
                0.229134,

                // SPHERE 1
                -0.000000,
                1.000000,
                -0.000000,
                -0.000000,
                0.973563,
                -0.229134,
                -0.000000,
                0.898272,
                -0.439557,
                -0.000000,
                0.780154,
                -0.625240,
                -0.000000,
                0.625240,
                -0.780154,
                -0.000000,
                0.439557,
                -0.898272,
                -0.000000,
                0.229134,
                -0.973563,
                0.000000,
                -0.000000,
                -1.000000,
                0.000000,
                -0.229134,
                -0.973563,
                0.000000,
                -0.439557,
                -0.898272,
                0.000000,
                -0.625240,
                -0.780154,
                0.000000,
                -0.780154,
                -0.625240,
                0.000000,
                -0.898272,
                -0.439557,
                0.000000,
                -0.973563,
                -0.229134,
                0.000000,
                -1.000000,
                0.000000,
                0.000000,
                -0.973563,
                0.229134,
                0.000000,
                -0.898272,
                0.439557,
                0.000000,
                -0.780154,
                0.625240,
                0.000000,
                -0.625240,
                0.780154,
                0.000000,
                -0.439557,
                0.898272,
                0.000000,
                -0.229134,
                0.973563,
                -0.000000,
                0.000000,
                1.000000,
                -0.000000,
                0.229134,
                0.973563,
                -0.000000,
                0.439557,
                0.898272,
                -0.000000,
                0.625240,
                0.780154,
                -0.000000,
                0.780154,
                0.625240,
                -0.000000,
                0.898272,
                0.439557,
                -0.000000,
                0.973563,
                0.229134,

                // SPHERE 3
                1.000000,
                0.000000,
                -0.000000,
                0.973563,
                -0.229134,
                -0.000000,
                0.898272,
                -0.439557,
                -0.000000,
                0.780154,
                -0.625240,
                -0.000000,
                0.625240,
                -0.780154,
                -0.000000,
                0.439557,
                -0.898272,
                -0.000000,
                0.229134,
                -0.973563,
                -0.000000,
                -0.000000,
                -1.000000,
                -0.000000,
                -0.229134,
                -0.973563,
                -0.000000,
                -0.439557,
                -0.898272,
                -0.000000,
                -0.625240,
                -0.780154,
                -0.000000,
                -0.780154,
                -0.625240,
                -0.000000,
                -0.898272,
                -0.439557,
                -0.000000,
                -0.973563,
                -0.229134,
                -0.000000,
                -1.000000,
                0.000000,
                0.000000,
                -0.973563,
                0.229134,
                0.000000,
                -0.898272,
                0.439557,
                0.000000,
                -0.780154,
                0.625240,
                0.000000,
                -0.625240,
                0.780154,
                0.000000,
                -0.439557,
                0.898272,
                0.000000,
                -0.229134,
                0.973563,
                0.000000,
                0.000000,
                1.000000,
                0.000000,
                0.229134,
                0.973563,
                0.000000,
                0.439557,
                0.898272,
                0.000000,
                0.625240,
                0.780154,
                0.000000,
                0.780154,
                0.625240,
                0.000000,
                0.898272,
                0.439557,
                0.000000,
                0.973563,
                0.229134,
                0.000000};

        std::vector<uint32_t> indices =
            {0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 0, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33, 33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47, 47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 28, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75, 75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 56};

        auto sphereMesh = dag::Mesh(vertices, indices);

        rs->vaos.emplace_back(sphereMesh.generateVAO(atts));
    }
}

/**
 * @brief Loads the internal assets: textures, meshes, etc.
 */
void dag::ResourceLoader::loadInternalAssets()
{
    resourceManager->textures.insert({0, Texture2D{}});
    resourceManager->textures[0].create((dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "2x2_white.jpg").generic_string());

    auto internals = dag::filesystem::getAssetsPath() / "internal" / "default_meshes";

    std::vector<std::string> internalsDat = {
        "Cube",
        "Sphere",
        "Starry",
        "Torus Knot",
        "Cylinder",
        "Plane",
        "Teapot",
        "Tetrahedra",
        "Torus"};

    auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();
    // Load the internal assets
    for (auto& internalPath : internalsDat)
    {
        size_t meshHash = dag::resourceManager->parseOBJ(internals / (internalPath + ".obj"));

        for (auto& shape : dag::resourceManager->models[meshHash].shapes)
        {
            shape.material.textureAmbientHash = 0;
            shape.material.textureDiffuseHash = 0;
            shape.material.textureSpecularHash = 0;
            shape.material.textureAlphaHash = 0;
            shape.material.textureBumpHash = 0;
            rs->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
            shape.VAOIndex = rs->vaos.size() - 1;
        }
        std::string name = internalPath;

        resourceManager->meshRenderers.insert({meshHash, dag::MeshRenderer{.name = name, .vaoID = rs->vaos.size() - 1, .hashIndex = meshHash, .textureHash = 0}});

        // Log::warning(rs->meshRenderers.back().hashIndex);
    }

    // load test voxel mesh
    {
        // shape 0
        // vb: 319611680
        // ib: 40755776

        // shape 1
        // vb: 29029088
        // ib: 3628664

        // shape 0
        // vb: 536870912
        // ib: 53687091

        // shape 1
        // vb: 53687091
        // ib: 5368709

        std::vector<size_t> sizes = { static_cast<size_t>(536870912) * 4, 53687091 * 4, 53687091 * 4, 5368709 * 4};

        for (size_t i = 0; i < voxelManager->model.shapes.size(); ++i)
        {
            voxelManager->model.shapes[i].material.textureAmbientHash = 0;
            voxelManager->model.shapes[i].material.textureDiffuseHash = 0;
            voxelManager->model.shapes[i].material.textureSpecularHash = 0;
            voxelManager->model.shapes[i].material.textureAlphaHash = 0;
            voxelManager->model.shapes[i].material.textureBumpHash = 0;

            //dag::Mesh shapeMesh;
            //resourceManager->getMesh(voxelManager->model.shapes[i], shapeMesh);

            rs->vaos.emplace_back();

            glGenVertexArrays(1, &rs->vaos.back().id);
            glGenBuffers(1, &rs->vaos.back().vertexBuffer.id);
            glGenBuffers(1, &rs->vaos.back().indexBuffer.id);

            rs->vaos.back().bind();

            rs->vaos.back().vertexBuffer.bind();
            glBufferData(GL_ARRAY_BUFFER, sizes[i * 2], NULL, GL_STREAM_DRAW);

            for (auto& att : resourceManager->atts)
            {
                rs->vaos.back().vertexBuffer.addAttribute(att);
                att.activate();
            }

            rs->vaos.back().indexBuffer.bind();
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizes[(i * 2) + 1], NULL, GL_STREAM_DRAW);

            rs->vaos.back().unbind();

            rs->vaos.back().vertexBuffer.unbind();
            rs->vaos.back().indexBuffer.unbind();

            voxelManager->model.shapes[i].VAOIndex = rs->vaos.size() - 1;
        }

        voxelManager->meshHash = resourceManager->getFileHash(std::string("V", 400));
        dag::voxelManager->update(glm::vec3(0.0f, 0.0f, 0.0f));

        std::string name = "Voxel";

        resourceManager->meshRenderers.insert({voxelManager->meshHash, dag::MeshRenderer{.name = name, .vaoID = rs->vaos.size() - 1, .hashIndex = voxelManager->meshHash, .textureHash = 0}});

        resourceManager->models.insert({voxelManager->meshHash, voxelManager->model});
    }

    resourceManager->textures.insert({0, dag::Texture2D()});
    resourceManager->textures[0].create((dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "2x2_white.jpg").generic_string());

    resourceManager->textureCube.insert({0, dag::TextureCube()});
    resourceManager->textureCube[0].create({(dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "px.png").generic_string(),
                                            (dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "nx.png").generic_string(),
                                            (dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "py.png").generic_string(),
                                            (dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "ny.png").generic_string(),
                                            (dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "pz.png").generic_string(),
                                            (dag::filesystem::getAssetsPath() / "internal" / "default_textures" / "skybox" / "nz.png").generic_string()});
}