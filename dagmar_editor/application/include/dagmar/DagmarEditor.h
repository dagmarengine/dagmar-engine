#pragma once

#include <dagmar/Application.h>
#include "dagmar/EditorUILayer.h"
#include "dagmar/RendererLayer.h"
#include "dagmar/GameLayer.h"

namespace dag
{
    class DagmarEditor : public Application
    {
      public:
        DagmarEditor();

        std::shared_ptr<GameLayer> gameLayer;
        std::shared_ptr<EditorUILayer> editorLayer;
        std::shared_ptr<RendererLayer> rendererLayer;
        void run() override;
        void onEvent(Event& e) override;
        bool onWindowClose(WindowCloseEvent& e) override;
    };
} // namespace dag