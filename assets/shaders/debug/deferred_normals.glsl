#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER

layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER

layout (location = 0) in vec2 inTexCoord;

layout (binding = 0) uniform sampler2D gbufferPosition;
layout (binding = 1) uniform sampler2D gbufferNormal;
layout (binding = 2) uniform sampler2D gbufferUV;
layout (binding = 3) uniform sampler2D gbufferBaseColor;
layout (binding = 4) uniform sampler2D gbufferAmbient;
layout (binding = 5) uniform sampler2D gbufferDiffuse;
layout (binding = 6) uniform sampler2D gbufferSpecularSpecExponent;
layout (binding = 7) uniform sampler2D gbufferEmissive;

layout(location = 0) out vec4 FragColor;

vec4 coloring()
{
    vec4 fp = texture(gbufferPosition, inTexCoord);

    if(fp.a == 0)
    {
        return vec4(0.0, 0.0, 0.0, 1.0);
    }

    vec3 normal = texture(gbufferNormal, inTexCoord).rgb;
    vec4 diffuse = texture(gbufferDiffuse, inTexCoord);
    vec4 baseColorWithAlpha = texture(gbufferBaseColor, inTexCoord);
    vec4 specularWithExp = texture(gbufferSpecularSpecExponent, inTexCoord);
    float specularExp = specularWithExp.a * MAX_SPECULAR_POWER;
    vec4 ambient = texture(gbufferAmbient, inTexCoord);
    vec4 emissive = texture(gbufferEmissive, inTexCoord);
    vec3 fragPos = fp.xyz;
    vec4 fragColor = baseColorWithAlpha;
    
    vec4 color = ambient * fragColor + emissive;
    vec4 diffuseTexColor = diffuse;
    
    color.rgb = normal.rgb * 0.5 + vec3(0.5);
    color.a = 1.0;

    return color;
}


void main()
{
    FragColor = coloring();
}
#endif