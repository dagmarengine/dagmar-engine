#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
layout (location = 0) in vec3 inVertPosition;
layout(location = 0) out vec3 outFragTexCoords;

const float factor = 1000;

const mat4 scale = mat4(
    factor, 0.0, 0.0, 0.0,
    0.0, factor, 0.0, 0.0,
    0.0, 0.0, factor, 0.0,
    0.0, 0.0, 0.0, 1.0);

void main()
{
    mat4 cameraModel = camera_data.modelMatrices[scene_data.currentCamera];
    cameraModel[3][0] = 0;
    cameraModel[3][1] = 0;
    cameraModel[3][2] = 0;

    outFragTexCoords = inVertPosition;    
    vec4 pos = camera_data.projectionMatrices[scene_data.currentCamera] * cameraModel * scale * vec4(inVertPosition, 1.0);
    gl_Position = pos.xyww;
}
#endif


#ifdef FRAGMENT_SHADER

layout (location = 0) in vec3 inFragTexCoords;
layout (location = 0) out vec4 outFragColor;

void main() 
{
	outFragColor = texture(sampler_cube_map, inFragTexCoords);
}
#endif