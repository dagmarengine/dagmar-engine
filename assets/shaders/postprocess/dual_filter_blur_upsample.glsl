#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif


#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    float threshold;
    float offset;
} post_process_data;


vec4 upsample(vec2 halfPixel)
{
    vec4 sum = texture(screen, inTexCoord + vec2(-halfPixel.x * 2.0, 0.0));
    sum += texture(screen, inTexCoord + vec2(-halfPixel.x, halfPixel.y)) * 2.0;
    sum += texture(screen, inTexCoord + vec2(0.0, halfPixel.y * 2.0));
    sum += texture(screen, inTexCoord + vec2(halfPixel.x, halfPixel.y)) * 2.0;
    sum += texture(screen, inTexCoord + vec2(halfPixel.x * 2.0, 0.0));
    sum += texture(screen, inTexCoord + vec2(halfPixel.x, -halfPixel.y)) * 2.0;
    sum += texture(screen, inTexCoord + vec2(0.0, -halfPixel.y * 2.0));
    sum += texture(screen, inTexCoord + vec2(-halfPixel.x, -halfPixel.y)) * 2.0;
    return sum / 12.0;
}

void main()
{
    float offset = post_process_data.offset;
    // float offset = 0.5;

    ivec2 textureSize = textureSize(screen, 0);

    float tsizeX = 1.0 / float(textureSize.x);
    float tsizeY = 1.0 / float(textureSize.y);

    vec4 color = upsample(vec2(offset * tsizeX, offset * tsizeY));

    FragColor = color;
}
#endif