#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

layout(location = 0) uniform vec3 cameraPosition;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    float fogNear;
    float fogFar;
    float density;
    vec4 color;

} post_process_data;

// const float density = 1.0;
const float LOG2 = 1.442695;
// const float fogNear = 0.01;
// const float fogFar = 5.0;

float fexp2(float dist, float density)
{
    float d = density * dist;
    return 1.0 - clamp(exp2(d * d * -LOG2), 0.0, 1.0);
}

float linearizeDepth(float d, float zNear, float zFar)
{
    return (2.0 * zNear) / (zFar + zNear - d * (zFar - zNear));
}

void main()
{
    float fogNear = post_process_data.fogNear;
    float fogFar = post_process_data.fogFar;
    float density = post_process_data.density;
    vec4 color = post_process_data.color;

    vec4 fragColor = texture(screen, inTexCoord);
    float fogDist = linearizeDepth(texture(screenDepth, inTexCoord).r, fogNear, fogFar);

    // float fogAmount = fexp2(fogDist, density);

    float fogAmount = smoothstep(fogNear, fogFar, fogDist);
    vec4 fogColor = vec4(1.0);
    
    FragColor = mix(fragColor, fogColor, fogAmount);
}
#endif