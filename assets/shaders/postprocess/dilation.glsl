#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;


// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif




#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    int size;
    float separation;
    float minThreshold;
    float maxThreshold;
} post_process_data;


// half-kernel size
// const int size = 3;
// const float separation = 4;
// const float minThreshold = 0.1;
// const float maxThreshold = 0.3;

void main()
{

    int size = post_process_data.size;
    float separation = post_process_data.separation;
    float minThreshold = post_process_data.minThreshold;
    float maxThreshold = post_process_data.maxThreshold;

    vec2 texSize = textureSize(screen, 0).xy;

    vec4 fragColor = texture(screen, inTexCoord);

    float maxGray = 0.0;
    vec4 maxGrayColor = fragColor; 

    for (int i = -size; i <= size; i++)
    {
        for(int j = -size; j <= size; j++)
        {
            bool isOutside = !(distance(vec2(i, j), vec2(0, 0)) <= size);

            // circular shape
            // skip if outside the circle, simple radius check
            if(isOutside) 
            {
                continue;
            }

            vec4 tcolor = texture(screen, inTexCoord + (vec2(i, j) * separation) / texSize);
            float grayscale = dot(tcolor.rgb, vec3(0.21, 0.72, 0.07));

            if(grayscale > maxGray)
            {
                maxGray = grayscale;
                maxGrayColor = tcolor;
            }
        }
    }

    fragColor.rgb = mix(fragColor.rgb, maxGrayColor.rgb, smoothstep(minThreshold, maxThreshold, maxGray));

    FragColor = vec4(fragColor.rgb, 1.0);
}
#endif