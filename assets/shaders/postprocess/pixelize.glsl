#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    int pixelSize;
} post_process_data;

// const int pixelSize = 4;

void main()
{
    vec2 texSize = textureSize(screen, 0).xy;

    int pixelSize = post_process_data.pixelSize;

    // vec4 position = texture(positionTexture, texCoord);
    // if(position.w <= 0) avoid bkg

    float x = int(gl_FragCoord.x) % pixelSize;
    float y = int(gl_FragCoord.y) % pixelSize;

    x = floor(pixelSize / 2.0) - x;
    y = floor(pixelSize / 2.0) - y;

    x = gl_FragCoord.x + x;
    y = gl_FragCoord.y + y;

    vec2 uv = vec2(x, y) / texSize;

    vec4 fragColor = texture(screen, uv);

    FragColor = vec4(fragColor.xyz, 1.0);
}
#endif