float linearizeDepth(float d, float near, float far)
{
    return (((far - near) * d) + near + far) / 2.0;
}