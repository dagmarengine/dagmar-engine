#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inVertexPos;

void main()
{
    gl_Position = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[draw_data.entityID] * vec4(inVertexPos, 1.0f);
}
#endif

/////////////////////////////

#ifdef FRAGMENT_SHADER
layout (location = 0) out vec4 FragColor;

void main()
{
    FragColor = draw_data.encodedID;
}
#endif